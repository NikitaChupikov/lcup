package com.java.lcup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LcupApplication  {
    public static void main(String[] args) {
        SpringApplication.run(LcupApplication.class, args);
    }
}
