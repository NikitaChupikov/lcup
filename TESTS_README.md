# Test Automation for LCUP

For write tests, we used the [pytest test framework](https://docs.pytest.org/en/latest/).

You can also find the following modules:
- [Selenium](#https://pypi.org/project/selenium/) – for UI tests.([selenium+pytest documentation](https://pytest-selenium.readthedocs.io/en/latest/user_guide.html))
- [Requests](https://requests.readthedocs.io/en/master/) – for REST API tests.


## Requirements installing

```sh
pip install pytest
pip install prettytable
pip install selenium
```

## Tests running
The following is an example of how to run tests from the console. You can also use the IDE tools.Such as Pycharm, Visual Studio Code and e.t.c.

- [Pycharm tools documentation](https://www.jetbrains.com/help/pycharm/pytest.html)
- [Visual Studio Code tools documentation](https://code.visualstudio.com/docs/python/testing) 

First of all you need to [run backend and frontend servers](https://bitbucket.org/NikitaChupikov/lcup/src/develop/README.md).
```sh
(venv) user@user: /lcup/LCUP$           python manager runserver
(venv) user@user: /lcup/LCUP/frontend$  npm start
```

Before running the tests, go to the automation_tests repository and 
```sh
cd automation_tests/
```
If you want to run only API smoke tests:
```sh
pytest tests/api_smoke_tests/
```
If you want to run only UI smoke tests:
```sh
pytest tests/ui_smoke_tests/
```
If you want to run all tests:
```sh
pytest tests
```

As a result, you should see information about the results of the tests.
```diff
============================ test session starts ===================================
platform linux -- Python 3.7.3, pytest-5.3.5, py-1.8.1, pluggy-0.13.1
rootdir: /lcup/automation_tests
collected 10 items                                                                                                                                                                                                                                               

tests/api_smoke_tests/test_default_endpoint.py ...                           [ 30%]
tests/api_smoke_tests/test_default_ml_resp.py .                              [ 40%]
tests/api_smoke_tests/test_insta_api.py ..                                   [ 60%]
tests/api_smoke_tests/test_param_response.py ....                            [100%]

============================= 10 passed in 7.38s ===================================
```
## File's Structure

Our test framework has the following file structure:

```
automation_tests
├── drivers                    # selenium drivers
│   ├── __init__.py
│   ├── geckodriver
│   ├── chromedriver
│   ├── exectutable_path.py
│   ├── geckodriver_win.exe
│   └── chromedriver_win.exe
│ 
├── fixtures                   # pytest fixtures
│   ├── __init__.py
│   ├── api_fixtures.py
│   ├── selenium_fixtures.py
│   └── ...
│
├── helpers                     # wrappers 
│   ├── __init__.py
│   ├── report.py
│   ├── insta_helper.py
│   ├── request_wrapper.py
│   └── ...
│
└── tests                       # all tests
    ├── api_smoke_tests         # rest api smoke tests
    │   ├── config.yaml         # tests configs
    │   └── ...
    │  
    ├── ui_smoke_tests          # ui tests
    │   ├── config.yaml         # tests configs
    │   └── ...
    └── ...                     # other tests
```



