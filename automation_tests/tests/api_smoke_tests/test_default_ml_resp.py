from nested_lookup import nested_lookup

CITY_COUNT = 3


def test_city_count(default_params_response):
    cities = nested_lookup('city_name', default_params_response.json())
    assert len(cities) == CITY_COUNT
