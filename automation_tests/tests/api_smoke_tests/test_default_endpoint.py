import logging


def test_err_code(expected_default_response,
                  actual_default_response, url, form_msg):
    logging.info(f'Test "errorCode" in request for {url}')
    expected_code = expected_default_response.get('errorCode')
    actual_code = actual_default_response.get('errorCode')
    msg = form_msg(actual_code, expected_code)
    assert 400 == actual_code, msg


def test_message(expected_default_response, actual_default_response, url, form_msg):
    logging.info(f'Test "message" in request for {url}')
    expected_message = expected_default_response.get('message')
    actual_message = actual_default_response.get('message')
    msg = form_msg(actual_message, expected_message)
    assert expected_message == actual_message, msg


def test_status(expected_default_response, actual_default_response, url, form_msg):
    logging.info(f'Test "errorCode" in request for {url}')
    expected_status = expected_default_response.get('status')
    actual_status = actual_default_response.get('status')
    msg = form_msg(actual_status, expected_status)
    assert expected_status == actual_status, msg
