import logging


def test_message(expected_params_response, actual_params_info, form_msg):
    url = actual_params_info.url
    actual_params_info = actual_params_info.json()
    logging.info(f'Test "message" in request for {url}')
    expected_message = expected_params_response.get('message')
    actual_message = actual_params_info.get('message')
    msg = form_msg(actual_message, expected_message)
    assert expected_message == actual_message, msg


def test_error_code(expected_params_response, actual_params_info, form_msg):
    url = actual_params_info.url
    actual_params_info = actual_params_info.json()
    logging.info(f'Test "message" in request for {url}')
    expected_code = expected_params_response.get('errorCode')
    actual_code = actual_params_info.get('errorCode')
    msg = form_msg(actual_code, expected_code)
    assert 400 == actual_code, msg


def test_status(expected_params_response, actual_params_info, form_msg):
    url = actual_params_info.url
    actual_params_info = actual_params_info.json()
    logging.info(f'Test "message" in request for {url}')
    expected_status = expected_params_response.get('status')
    actual_status = actual_params_info.get('status')
    msg = form_msg(actual_status, expected_status)
    assert expected_status == actual_status, msg


def test_params(expected_params_response, actual_params_info, form_msg):
    url = actual_params_info.url
    actual_params_info = actual_params_info.json()
    logging.info(f'Test "message" in request for {url}')
    expected_params = expected_params_response.get('positional_arguments')
    actual_params = actual_params_info.get('positional_arguments')
    msg = form_msg(actual_params, expected_params)
    assert expected_params == actual_params, msg
