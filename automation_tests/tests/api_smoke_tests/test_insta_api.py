def test_full_name(insta_api, actual_insta_info):
    full_name = insta_api.get('full_name')
    actual_full_name = actual_insta_info.full_name
    assert full_name == actual_full_name


def test_biography(insta_api, actual_insta_info):
    biography = insta_api.get('biography')
    actual_biography = actual_insta_info.biography
    assert biography == actual_biography
