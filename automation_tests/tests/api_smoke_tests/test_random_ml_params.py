from nested_lookup import nested_lookup

CITY_COUNT = 3


def test_random_ml_city_count(random_ml_params_response):
    cities = nested_lookup('city_name', random_ml_params_response)
    assert len(cities) == CITY_COUNT
