import logging

from time import sleep
from selenium.webdriver.common.keys import Keys

SLEEP_TIME = 3


def test_sign_up(username, db_helper, browser):
    logging.info(f'SignUp new user: {username}')
    driver = browser
    driver.find_elements_by_class_name('MuiButton-label')[1].click()
    for name in ('firstName', 'lastName', 'email', 'password'):
        field = driver.find_element_by_name(name)
        field.send_keys(username)
    field.send_keys(Keys.ENTER)
    sleep(SLEEP_TIME)
    driver.close()

    logging.info('Select user from database')
    assert db_helper.select_user('auth_user', username)[0][0] == username

    logging.info('Delete user from database')
    db_helper.delete_user('auth_user', username)

    logging.info(f'Check that {username} no in database')
    assert db_helper.select_user('auth_user', username) is None
