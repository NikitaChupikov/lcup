import logging


def test_country_name(destination_resp):
    logging.info('Test destination country name')
    assert 'Germany' == destination_resp.get('country_name')


def test_city_name(destination_resp):
    logging.info('Test destination city name')
    assert 'Berlin' == destination_resp.get('city_name')


def test_flights_len(destination_resp):
    logging.info('Test destination flights resp len')
    limit = 1
    destination_limit = len(destination_resp.get('flights'))
    assert limit == destination_limit


def test_hotels_len(destination_resp):
    logging.info('Test destination hotels resp len')
    limit = 1
    destination_limit = len(destination_resp.get('hotels'))
    assert limit == destination_limit
