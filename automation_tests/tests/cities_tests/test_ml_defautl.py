import logging

DEFAULT_CITIES = {'Riga': 8.00, 'Moscow': 8.00, 'Vienna': 8.00}


def test_ml_default(random_ml_params_response, form_msg):
    actual_info = f'{random_ml_params_response} not equal to {DEFAULT_CITIES}'
    logging.info(f'Assert that {actual_info}')
    msg = form_msg(actual_info, DEFAULT_CITIES)
    assert str(random_ml_params_response) != str(DEFAULT_CITIES), msg
