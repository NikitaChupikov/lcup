import pytest
import logging

from .euro_capitals import EURO_CAPITALS
from automation_tests.helpers.DB_helper import PostgresHelper

sql = PostgresHelper()
CODES_TABLE = 'way_out_citycodes'


@pytest.mark.parametrize('city', EURO_CAPITALS)
def test_capitals_in_db(city):
    logging.info('Check IATA codes for euro capitals in database')
    city_info = sql.select_city(CODES_TABLE, city)
    assert city_info[0] == (city, EURO_CAPITALS.get(city))
