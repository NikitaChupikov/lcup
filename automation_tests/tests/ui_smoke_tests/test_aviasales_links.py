import logging

from time import sleep

SLEEP_TIME = 8


def test_aviasales_first_link(browser):
    logging.info('Test first aviasales link after click on LetsGo button')
    driver = browser
    driver.find_element_by_class_name('button-start').click()
    sleep(SLEEP_TIME)
    icons = browser.find_elements_by_class_name('rotate90')
    for icon in icons:
        icon.click()
        sleep(SLEEP_TIME)
    driver.find_element_by_class_name('flightBookBtn').click()
    sleep(SLEEP_TIME)
    endpoint = driver.current_url
    driver.close()
    assert 'aviasales' in endpoint
