import logging

from time import sleep
from selenium.webdriver.support.ui import Select

SLEEP_TIME = 8


def test_random_params(browser, random_ml_params):
    logging.info('Test selectors')
    driver = browser
    for key, value in random_ml_params.items():
        if key not in ('depart_date', 'return_date',
                       'origin', 'sort', 'limit'):
            sleep(SLEEP_TIME)
            select = Select(driver.find_element_by_name(key))
            sleep(SLEEP_TIME)
            select.select_by_visible_text(value)
    driver.find_element_by_class_name('button-start').click()
    sleep(SLEEP_TIME)
    icons = browser.find_elements_by_class_name('rotate90')
    for icon in icons:
        icon.click()
        sleep(SLEEP_TIME)
    driver.find_element_by_class_name('flightBookBtn').click()
    sleep(SLEEP_TIME)
    endpoint = driver.current_url
    driver.close()
    assert 'aviasales' in endpoint
