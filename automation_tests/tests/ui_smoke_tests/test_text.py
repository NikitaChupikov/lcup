import logging


def test_text_in_page(expected_texts, actual_text, teardown):
    for expected_text in expected_texts:
        text_count = expected_texts.get(expected_text)
        logging.info(f'Test that the "{expected_text}" '
                     f'appears on the page {text_count} times.')
        msg = teardown(f'{expected_text} appers {text_count}',
                       f'{actual_text.count(expected_text)} - text appears.')
        assert actual_text.count(expected_text) == text_count, msg
