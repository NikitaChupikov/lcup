import logging


def test_title(expected_title, actual_title, teardown):
    logging.info('Test title on page')
    msg = teardown(actual_title, expected_title)
    assert expected_title == actual_title, msg



