from prettytable import PrettyTable


class ReportTable:
    def __init__(self, actual_result, expected_result):
        self.actual_result = actual_result
        self.expected_result = expected_result
        self.table = PrettyTable(["Actual result",
                                  "Expected result"])

    @property
    def message(self):
        self.table.add_row([self.actual_result,
                            self.expected_result])
        return self.table