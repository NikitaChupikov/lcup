""" Module for moving aviasales IATA codes to database. """
import requests

from .DB_helper import PostgresHelper

URL = 'http://api.travelpayouts.com/data/ru/cities.json'

session = requests.session()
database = PostgresHelper()


class AviasalesCityCodes:
    @staticmethod
    def get_cities() -> dict:
        """ Get aviasales city dict.

        :return: .json with cities.
        """
        resp = session.get(URL)
        cities = resp.json()
        return cities

    @staticmethod
    def _get_cities_coordinates(city: dict) -> tuple:
        """ Get coordinates from city data.

        :param city: dict with city info.
        :return:     coordinates - tuple(lon, lat)
        """
        coordinates = city.get('coordinates')
        if coordinates:
            lon = str(coordinates.get('lon', 'None'))
            lat = str(coordinates.get('lat', 'None'))
        else:
            lon = 'None'
            lat = 'None'
        return lon, lat

    @staticmethod
    def _get_cities_translation(city: dict) -> str:
        """ Get english name from city info.

        :param city: dict with city info.
        :return:     city name.
        """
        name_translations = city.get('name_translations')
        if name_translations:
            name = name_translations.get('en', 'None')
        else:
            name = 'None'
        return name

    def add_to_table(self, table: str) -> None:
        """ Add rows with city codes in table.

        :param table: name of database table.
        """
        for city in self.get_cities():
            name = self._get_cities_translation(city)
            lon, lat = self._get_cities_coordinates(city)
            time_zone = city.get('time_zone', 'None')
            rus_name = city.get('name', 'None')
            code = city.get('code', 'None')
            country_code = city.get('country_code', 'None')
            values = (time_zone, name, rus_name, lon, lat, code, country_code)
            database.insert_codes(values, table)
        database.close_connection()
