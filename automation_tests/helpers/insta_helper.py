import re

from automation_tests.helpers.request_wrapper import RequestSession


class InstaHelper:
    def __init__(self, username: str):
        self.session = RequestSession()
        self.username = username
        self.url = self.get_url(username)
        self.full_name = self.get_full_name()
        self.biography = self.get_biography()

    @staticmethod
    def get_url(username: str) -> str:
        """ Get user instagram  URL by username.

        :param username: username for instagram.
        :return:         user URL.
        """
        return f'https://www.instagram.com/{username}/'

    def get_full_name(self) -> str:
        """ Get user full_name by usename.

        :return: user full_name.
        """
        resp = self.session.get(self.url)
        return re.search(r'"full_name":"(.*?)"', resp.text)[1]

    def get_biography(self) -> str:
        """ Get user biography by usename.

        :return: user biography.
        """
        resp = self.session.get(self.url)
        return re.search(r'"biography":"(.*?)"', resp.text)[1]
