import requests


class RequestSession:
    def __init__(self):
        self.session = requests.session()

    @property
    def cookies(self):
        return self.session.cookies

    @property
    def headers(self):
        return self.session.headers

    @property
    def params(self):
        return self.session.params

    @property
    def proxies(self):
        return self.session.proxies

    def authenticate(self, username: str, password: str) -> None:
        """ Session authentication.

        :param username: name for user.
        :param password: user password.
        """
        self.session.auth = (username, password)

    def get(self, url: str, **kwargs):
        """Sends a GET request.

        :param url: current URL.
        :return:    class:`Response` object.
        """
        resp = self.session.get(url, **kwargs)
        return resp

    def post(self, url: str, data=None, json=None, **kwargs):
        """Sends a POST request.

        :param url:    current URL.
        :param json:   (optional) json to send in the body of the :class:`Request`.
        :param kwargs: (optional) arguments that ``request`` takes.
        :param data:   (optional) Dictionary, list of tuples, bytes, or file-like
                        object to send in the body of the :class:`Request`.
        :return:       class:`Response` object.
        """
        resp = self.session.post(url, data=data, json=json, **kwargs)
        return resp

    def put(self, url: str, data=None, **kwargs):
        """Sends a PUT request.

        :param url:    current URL.
        :param data:   (optional) Dictionary, list of tuples, bytes, or file-like
                        object to send in the body of the :class:`Request`.
        :param kwargs: (optional) arguments that ``request`` takes.
        :return:       class:`Response` object.
        """
        resp = self.session.put(url, data, **kwargs)
        return resp

    def set_redirects(self, max_redirects: str):
        """Set maximum number of redirects allowed

        :param max_redirects: expected redirects count.
        :return:              number of redirects allowed.
        """
        self.session.max_redirects = max_redirects

    def response_choice(self, url: str = '', resp_type: str = 'GET',
                        data=None, json=None, **kwargs):
        """Choosing the response type.

        :param url:       current URL.
        :param resp_type: type of using response.
        :param json:      (optional) json to send in the body of the :class:`Request`.
        :param kwargs:    (optional) arguments that ``request`` takes.
        :param data:      (optional) Dictionary, list of tuples, bytes, or file-like
                           object to send in the body of the :class:`Request`.
        :return:
        """
        if resp_type == 'POST':
            return self.post(url, data=data, json=json, **kwargs)
        if resp_type == 'GET':
            return self.get(url, **kwargs)
        if resp_type == 'PUT':
            return self.put(url, data=data, **kwargs)

    @staticmethod
    def _raise_status(resp):
        """Raises "HTTPError", if one occurred.

        :param resp: class:`Response` object.
        """
        if resp.status_code != requests.codes.ok:
            resp.raise_for_status()

    @staticmethod
    def set_url(protocol: str, hostname: str,
                port: int, endpoint: str) -> str:
        """Create URL using params.

        :param protocol: protocol.
        :param hostname: name of the host.
        :param port:     port.
        :param endpoint: endpoint for URL.
        :return:         current URL.
        """
        url = f"{protocol}://{hostname}:{port}{endpoint}"
        return url
