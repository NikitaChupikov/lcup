""" Module to get city codes from Hotellook. """
import requests

from nested_lookup import nested_lookup

from lcup.automation_tests.helpers.DB_helper import PostgresHelper
from lcup.LCUP.configs.config import aviasales_token

URL = r'http://engine.hotellook.com/api/v2/static/locations.json'
database = PostgresHelper()


class HotellookCityCodes:
    def __init__(self):
        self.base_url = URL
        self.session = requests.session()
        self.table_name = 'way_out_hotellookcodes'
        self.values = ('city_id', 'country_id', 'lon',
                       'lat', 'name', 'rus_name')

    def get_cities(self) -> dict:
        """ Get cites info from hotellook.

        :return: dict with cities data.
        """
        params = {
            'token': aviasales_token
        }
        resp = self.session.get(self.base_url, params=params)
        return resp.json()

    @staticmethod
    def get_names(name_info: dict) -> tuple:
        """ Get data from city names.

        :param name_info: dict with city names.
        :return:          tuple(en_names, rus_names)
        """
        rus_names = nested_lookup('RU', name_info)
        en_names = nested_lookup('EN', name_info)
        if rus_names and en_names:
            return en_names[0], rus_names[0]
        else:
            return en_names, rus_names

    def add_city_info(self):
        """ Add city codes to database. """
        for city in self.get_cities():
            id = city.get('id', 'None')
            country_id = city.get('countryId', 'None')
            lon = city.get('longitude', 'None')
            lat = city.get('latitude', 'None')
            name_info = city.get('name', 'None')
            en_names, rus_names = self.get_names(name_info)
            if en_names and rus_names:
                for en_name, rus_name in zip(en_names, rus_names):
                    en_name = en_name.get('name', 'None')
                    rus_name = rus_name.get('name', 'None')
                    row = (id, country_id, lon, lat, en_name.replace("'", '`'),
                           rus_name.replace("'", '`'))
                    database.hotellook_codes(self.table_name, row)
        database.close_connection()
