""" Module for database connection. """
import psycopg2

from LCUP.LCUP.settings import DATABASES

DB = DATABASES.get('default')


class PostgresHelper:
    def __init__(self):
        user = DB.get('USER')
        pwd = DB.get('PASSWORD')
        host = DB.get('HOST')
        port = DB.get('PORT')
        database = DB.get('NAME')
        self.connection = psycopg2.connect(user=user,
                                           password=pwd,
                                           host=host,
                                           port=port,
                                           database=database)
        self.cursor = self.connection.cursor()

    def select_user(self, table: str, username: str) -> list:
        """ Select username from table in database.

        :param table:    name of the table in database.
        :param username: username in database.
        :return:         result of select.
        """
        sql_query = f"select username from {table} where username='{username}'"
        self.cursor.execute(sql_query)
        return self.cursor.fetchall()

    def select_city(self, table: str, city: str) -> list:
        """ Select city from table in database.

        :param table: name of the table in database.
        :param city:  name of the city.
        :return:      list((city, code), (city, code), ...).
        """
        sql_query = f"select name, code from {table} where name='{city}'"
        self.cursor.execute(sql_query)
        return self.cursor.fetchall()

    def delete_user(self, table: str, username: str) -> None:
        """ Delete table query with current username.

        :param table:    name of the table in database.
        :param username: username in database.
        """
        sql_query = f"delete from {table} where username='{username}'"
        self.cursor.execute(sql_query)

    def hotellook_codes(self, table: str, row: tuple) -> str:
        """ Insert hotellook codes into table;

        :param table: name of the table in database.
        :param row:   hotellook city info.
        """
        sql_query = f'INSERT INTO {table} ' \
            f'(city_id, country_id, lon, lat, name, rus_name) ' \
            f'VALUES{row};'
        self.cursor.execute(sql_query)

    def insert_codes(self, values: tuple, table: str) -> None:
        """ Insert codes into table.

        :param values: city params dict(time_zone, name ... )
        :param table:  name of the table in database.
        """
        sql_query = f"INSERT INTO {table} " \
                        f"(time_zone, " \
                        f"name, " \
                        f"rus_name, " \
                        f"lon, " \
                        f"lat, " \
                        f"code, " \
                        f"country_code) " \
                        f"VALUES(%s, %s, %s, %s, %s, %s, %s);",
        self.cursor.execute(sql_query, (values))

    def close_connection(self) -> None:
        """ Close database connection. """
        self.connection.commit()
        self.cursor.close()
        self.connection.close()
