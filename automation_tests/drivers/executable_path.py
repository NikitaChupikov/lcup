import os
import platform


def path():
    path = os.path.abspath(__file__)
    os_type = platform.system()
    if os_type == 'Windows':
        chromedriver = path.replace('executable_path.py', 'chromedriver_win')
        geckodriver = path.replace('executable_path.py', 'geckodriver_win')
        return chromedriver, geckodriver
    elif os_type == 'Darwin':
        chromedriver = path.replace('executable_path.py', 'chromedriver_mac')
        geckodriver = path.replace('executable_path.py', 'geckodriver_mac')
        return chromedriver, geckodriver
    else:
        chromedriver = path.replace('executable_path.py', 'chromedriver')
        geckodriver = path.replace('executable_path.py', 'geckodriver')
        return chromedriver, geckodriver
