import json
import os
import pytest
import random

from datetime import timedelta, datetime

from .selenium_fixtures import config, url
from automation_tests.helpers.request_wrapper import RequestSession
from automation_tests.helpers.report import ReportTable
from automation_tests.helpers.insta_helper import InstaHelper

session = RequestSession()

API_URL = r'http://127.0.0.1:8000/api/'


@pytest.fixture(scope='module')
def expected_default_response(config) -> dict:
    return config.get('default_response')


@pytest.fixture(scope='module')
def expected_params_response(config) -> dict:
    return config.get('params_info_response')


@pytest.fixture(scope='module')
def actual_default_response(url) -> dict:
    resp = session.get(url)
    return resp.json()


@pytest.fixture(scope='module')
def actual_params_info(url):
    invalid_params = {
        'invalid_param': 'error'
    }
    resp = session.get(url, params=invalid_params)
    return resp


@pytest.fixture(scope='module')
def default_params_response(url, dates):
    invalid_params = {
        'depart_date': dates[0],
        'return_date': dates[1],
        'origin': 'Minsk',
        'sort': 'True',
    }
    resp = session.get(url, params=invalid_params)
    return resp


@pytest.fixture(scope='module')
def dates() -> tuple:
    today = datetime.now()
    next_month = timedelta(30)
    interval = timedelta(10)
    depart_date = today + next_month
    return_date = depart_date + interval
    return str(depart_date.date()), str(return_date.date())


@pytest.fixture(scope='module')
def tst_insta_user(config) -> str:
    return config.get('test_insta_user')


@pytest.fixture(scope='module')
def insta_api(url, tst_insta_user):
    insta_url = f'{url}/insta'
    param = {
        'insta_user': tst_insta_user
    }
    resp = session.get(insta_url, params=param)
    return resp.json()


@pytest.fixture(scope='module')
def actual_insta_info(tst_insta_user):
    return InstaHelper(tst_insta_user)


@pytest.fixture(scope="function")
def form_msg() -> str:
    def _report(actual_result, expected_result):
        """Make report in tables.
        :return: report table.
        """
        table = ReportTable(actual_result, expected_result)
        table = f'\n{table.message}'
        return table

    return _report


@pytest.fixture(scope="module")
def ml_questions() -> dict:
    path = os.path.abspath(__file__)
    base_dir = path.split('lcup')[0]
    filename = f'{base_dir}lcup/LCUP/configs/ml_questions.json'
    with open(filename, 'r', encoding="utf8") as file:
        config = json.loads(file.read())
        return config


@pytest.fixture(scope="module")
def random_ml_params(ml_questions, dates) -> dict:
    params = {
        'depart_date': dates[0],
        'return_date': dates[1],
        'origin': 'Minsk',
        'sort': 'True',
        'limit': 1
    }
    for question in ml_questions.get('questions'):
        key = question.get('request_name')
        value = random.choice(question.get('answers'))
        params[key] = value
    return params


@pytest.fixture(scope='module')
def random_ml_params_response(url, random_ml_params):
    resp = session.get(url, params=random_ml_params)
    return resp.json()


@pytest.fixture(scope='module')
def destination_resp(dates):
    params = {
        'depart_date': dates[0],
        'return_date': dates[1],
        'origin': 'Minsk',
        'destination': 'Berlin',
        'sort': 'True',
        'limit': 1
    }
    resp = session.get(API_URL, params=params)
    return resp.json()[0]
