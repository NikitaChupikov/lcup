import pytest

from automation_tests.helpers.DB_helper import PostgresHelper


@pytest.fixture(scope='module')
def username(config: dict) -> str:
    return config.get('test_username')


@pytest.fixture(scope='module')
def db_helper(config: dict) -> str:
    db_helper = PostgresHelper()
    return db_helper
