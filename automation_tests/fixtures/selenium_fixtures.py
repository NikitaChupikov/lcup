import logging
import os.path
import pytest
import yaml

from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from automation_tests.drivers.executable_path import path
from automation_tests.helpers.report import ReportTable

options = Options()
options.headless = True
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')

CONFIG_FILE = 'config.yaml'


@pytest.fixture(scope='module')
def config(request) -> dict:
    """Give data from config .yaml file.
    :param request: pytest request.
    :return:        dict from config file.
    """
    filename = f"{str(request.fspath.dirname)}/{CONFIG_FILE}"
    if not os.path.exists(filename):
        logging.info(f'No such file or directory:{filename}')
    else:
        with open(filename, 'r', encoding='utf-8') as file:
            config = yaml.safe_load(file)
        return config


@pytest.fixture(params=['chrome', 'firefox'], scope="module")
def browser(request, url):
    """Create webdriver instance.
    :param request: pytest request.
    :param url:     url of testing page.
    :return:        webdriver instance.
    """
    if r'chrome' in request.param:
        browser = webdriver.Chrome(executable_path=path()[0],
                                   options=chrome_options)
    if r'firefox' in request.param:
        browser = webdriver.Firefox(executable_path=path()[1], options=options)
    browser.get(url)
    return browser


@pytest.fixture(scope="function")
def teardown(browser):
    """Closes the current window,
    quits the drivers and closes every associated window
    and make report in tables.
    :param browser: webdriver instance.
    :return:        report.
    """

    def _report(actual_result, expected_result):
        """Make report in tables.
        :return: report table.
        """
        table = ReportTable(actual_result, expected_result)
        table = f'\n{table.message}'
        return table

    browser.close()
    browser.quit()
    return _report


@pytest.fixture(scope='module')
def url(config: dict) -> str:
    """Get URL from config file.
    :param config: dict from config file.
    :return:       URL from config file.
    """
    return config.get('url')
