import pytest


@pytest.fixture(scope='module')
def expected_title(config: dict) -> str:
    return config.get('title')


@pytest.fixture(scope='module')
def actual_title(browser) -> str:
    return browser.title


@pytest.fixture(scope='module')
def expected_texts(config) -> dict:
    return config.get('text')


@pytest.fixture(scope='module')
def actual_text(browser) -> str:
    return browser.page_source
