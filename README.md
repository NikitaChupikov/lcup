# LCUP APP

The best solution for your trip.

## Installation

Clone this repository

```sh
git clone https://NikitaChupikov@bitbucket.org/NikitaChupikov/lcup.git`
```

Open repository
```sh
cd LCUP
```

Run Django server (after python requirements and virtual Environments installation)

```sh
python manage.py runserver
```


## Setup the pip package manager

WARING: Use python version -- 3.7

Check to see if your Python installation has pip. Enter the following in your terminal:

```sh
pip -h
```

If you see the help text for pip then you have pip installed, otherwise [download and install pip](https://pip.pypa.io/en/latest/installing.html)


## Install the virtualenv package

The virtualenv package is required to create virtual environments. You can install it with pip:

**Warning:** On windows sometimes you need to add the prefix **' py -m pip install ...  '** or **' python -m pip install ...  '**
```sh
pip install virtualenv
```

## Create the virtual environment

To create a virtual environment, you must specify a path. For example to create one in the local directory called &#39;mypython&#39;, type the following:

```sh
virtualenv venv
```


## Activate the virtual environment

You can activate the python environment by running the following command:

**MacOS/Linux**

```sh
source venv/bin/activate
```

**Windows**
```sh
venv\Scripts\activate
```

You should see the name of your virtual environment in brackets on your terminal line e.g. (mypython).

Any python commands you use will now work with your virtual environment

**More info** : https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/

## Requirements Installation

Using pip

```sh
pip install –r requirements.txt
```

or
```sh
pip install asyncio
pip install aiohttp
…
```

or

You can also use the IDE tools.Such as [Pycharm](https://www.jetbrains.com/pycharm/), [Visual Studio Code](https://code.visualstudio.com/) and e.t.c.


- [PyCharm Install, uninstall, and upgrade packages﻿](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)

    PyCharm provides methods for installing, uninstalling, and upgrading Python packages for a particular Python interpreter. By default, PyCharm uses pip to manage project packages. For Conda environments you can use the conda package manager.
    ![alt text](https://resources.jetbrains.com/help/img/idea/2019.3/py_install_packages.png)
    
- [Visual Studio Code tools](https://code.visualstudio.com/docs/editor/extension-gallery)
    The Python extension provides many features for editing Python source code.
    ![alt text](https://code.visualstudio.com/assets/docs/python/editing/python-editing.gif)


## Running frontend server

First of all you need to install NPM
- [npm for Ubuntu/Debian](https://www.devroom.io/2011/10/24/installing-node-js-and-npm-on-ubuntu-debian/)
- [npm for Centos](https://linuxize.com/post/how-to-install-node-js-on-centos-7/)
- [npm for macOS](https://blog.teamtreehouse.com/install-node-js-npm-mac)

Open frontend repository
```sh
cd LCUP/frontend
```

Install all packages
```sh
npm install
```

Run react app
```sh
npm start
```

## Branches structure example

https://nvie.com/posts/a-successful-git-branching-model/
![alt text](https://nvie.com/img/git-model@2x.png)
