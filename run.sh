#!/bin/sh

echo 'RUNNIG REACT & SPRING APP'

# Create react build
rm -rf LCUP/frontend/build/*
cd LCUP/frontend/
npm run-script build
cd ../../

# Add react build to proxy static
rm -rf Getway/src/main/resources/static/*
cp -r LCUP/frontend/build/* Getway/src/main/resources/static

# Compile and run project
cd Getway/
mvn clean install
mvn compile
mvn exec:java -Dexec.mainClass=com.java.lcup.LcupApplication
