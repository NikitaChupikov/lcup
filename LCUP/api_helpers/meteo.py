""" Module for openweathermap parsing. """
import os
import requests

from base_parser import AiohttpParser

API_KEY = os.environ.get('METEO_KEY')
BASE_URL = f'http://api.openweathermap.org/data/2.5/'

DEFAULT_CITIES = ['London', 'New York', 'Moscow',
                  'Japan', 'Sydney', 'Oslo']

parser = AiohttpParser()


class Meteo:
    def __init__(self, city_name: str):
        self.city_name = city_name
        self.session = requests.Session()
        self.params = {'appid': API_KEY, 'q': city_name}

    @property
    def base_url(self) -> str:
        return f'http://api.openweathermap.org/data/2.5/'

    @property
    def forecast_url(self) -> str:
        return f'{self.base_url}/forecast?'

    @property
    def weather_url(self) -> str:
        return f'{self.base_url}weather?'

    @property
    def history_url(self) -> str:
        return self.base_url.replace('api', 'samples') + 'history/city?'

    def __fetch(self, url: str, params: dict) -> dict:
        """ Sends GET request.

        :param url:    url for GET request.
        :param params: params for GET requests.
        :return:       .json response.
        """
        response = self.session.get(url, params=params)
        return response.json()

    def get_five_days(self) -> dict:
        """ Get weather for 5 days.

        :return: dict with 5 days weather.
        """
        response = self.__fetch(self.forecast_url, self.params)
        return {'city': response.get('city'),
                'list': response.get('list')}

    def current_weather(self) -> dict:
        """ Current weather for self.city_name. """
        return self.__fetch(self.weather_url, self.params)

    def history(self, start=None, end=None, cnt=None) -> dict:
        """ Get weather history.

        :param start: start date (unix time, UTC time zone),e.g.start=1369728000
        :param end:   end date (unix time, UTC time zone), e.g. end=1369789200
        :param cnt:   amount of returned data
                      (one per hour, can be used instead of 'end')
        :return:      weather history response.
        """
        params = self.params
        if start:
            params['start'] = start
        if end:
            params['end'] = end
        if cnt:
            params['cnt'] = cnt
        return self.__fetch(self.history_url, params)

    async def world_meteo(self) -> list:
        """ Get list of weather data for default cities"""
        cities_params = []
        url = self.weather_url
        for city in DEFAULT_CITIES:
            params = {'appid': API_KEY, 'q': city, }
            cities_params.append(params)
        return await parser.get_api_data(url, cities_params)
