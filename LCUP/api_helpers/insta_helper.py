import instagram_explore as ie
import instagram_explore

from nested_lookup import nested_lookup


class InstaAnalyzer:
    def __init__(self, username: str, max_id=None):
        self.ie = instagram_explore
        self.user = ie.user(username, max_id)
        self.user_data = self.user.data

    def get_locations(self, location_id, max_id=None) -> dict:
        return self.ie.location(location_id, max_id).data

    @property
    def biography(self):
        return self.user_data.get('biography', '')

    @property
    def edge_followed_by(self):
        return self.user_data.get('edge_followed_by').get('count')

    @property
    def edge_follow(self):
        return self.user_data.get('edge_follow').get('count')

    @property
    def full_name(self):
        return self.user_data.get('full_name')

    @property
    def id(self):
        return self.user_data.get('id')

    @property
    def profile_pic_url_hd(self):
        return self.user_data.get('profile_pic_url_hd')

    @property
    def pics(self):
        return nested_lookup('display_url', self.user_data)

    @property
    def locations(self):
        location_result = nested_lookup('location', self.user_data)
        locations = [location for location in location_result if location]
        return locations
