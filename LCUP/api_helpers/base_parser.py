# !/usr/bin/python
# -*- coding: utf-8 -*-
import asyncio
import nest_asyncio
import requests_async as requests

from aiohttp import ClientSession
from bs4 import BeautifulSoup

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
nest_asyncio.apply()


class AiohttpParser:
    @staticmethod
    async def fetch(session, url: str, params: dict = None) -> dict:
        """ Make async GET requests.

        :param session: client session.
        :param url:     url for request.
        :param params:  params for get request.
        :return:        dict with request data.
        """
        async with session.get(url, params=params) as response:
            return await response.json(content_type=None)

    async def run(self, referer: str, params: list) -> list:
        """ Create tasks with requests.

        :param referer: base url.
        :param params:  params for get request.
        :return:        result of requests.
        """
        tasks = []
        async with ClientSession(headers={"Referer": referer}) as session:
            for param in params:
                task = asyncio.ensure_future(
                    self.fetch(session, referer, param))
                tasks.append(task)
            responses = await asyncio.gather(*tasks)
            return responses

    async def get_api_data(self, referer: str, params: list) -> list:
        """ Get API responses using base url.

        :param referer: base url.
        :param params:  params for get request.
        :return:        result of requests.
        """
        return await asyncio.ensure_future(self.run(referer, params))


class RequestsAsyncParser:
    def __init__(self, username: str = '', password: str = ''):
        self.session = requests.Session()
        self.session.auth = (username, password)

    async def get(self, url: str, **kwargs):
        """ Sends a GET request.

        :param url: current URL.
        :return:    class:`Response` object.
        """
        resp = await self.session.get(url, **kwargs)
        self._raise_status(resp)
        return resp

    async def post(self, url: str, data=None, json=None, **kwargs):
        """ Sends a POST request.

        :param url:    current URL.
        :param json:   (optional) json to send in the body of the :class:`Request`.
        :param kwargs: (optional) arguments that ``request`` takes.
        :param data:   (optional) Dictionary, list of tuples, bytes, or file-like
                        object to send in the body of the :class:`Request`.
        :return:       class:`Response` object.
        """
        resp = await self.session.post(url, data=data, json=json, **kwargs)
        return resp

    async def put(self, url: str, data=None, **kwargs):
        """ Sends a PUT request.

        :param url:    current URL.
        :param data:   (optional) Dictionary, list of tuples, bytes, or file-like
                        object to send in the body of the :class:`Request`.
        :param kwargs: (optional) arguments that ``request`` takes.
        :return:       class:`Response` object.
        """
        resp = await self.session.put(url, data, **kwargs)
        return resp

    @staticmethod
    def _raise_status(resp):
        """ Raises "HTTPError", if one occurred.

        :param resp: class:`Response` object.
        """
        if resp.status_code != requests.codes.ok:
            resp.raise_for_status()

    @staticmethod
    def get_soup(html_doc: str, **kwargs):
        """ Get BeautifulSoup object using html text.

        :param html_doc: html text.
        :return:         BeautifulSoup object.
        """
        return BeautifulSoup(html_doc, 'html.parser', **kwargs)

    @property
    def cookies(self):
        return self.session.cookies

    @property
    def headers(self):
        return self.session.headers

    @property
    def params(self):
        return self.session.params

    @property
    def proxies(self):
        return self.session.proxies
