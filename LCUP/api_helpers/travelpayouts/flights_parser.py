""" Module that provide flights API. """
from configs.config import aviasales_token
from api_helpers.base_parser import RequestsAsyncParser, AiohttpParser

parser = AiohttpParser()


class AviasalesParser(RequestsAsyncParser):
    """ Aviasales API instance  """

    @property
    def base_url(self) -> str:
        return r'http://api.travelpayouts.com/v2/prices/latest?'

    @property
    def country_url(self):
        return r'http://api.travelpayouts.com/data/countries.json'

    @property
    def city_url(self):
        return r'http://api.travelpayouts.com/data/ru/cities.json'

    @property
    def aviasales_headers(self) -> dict:
        return {'x-access-token': aviasales_token}

    async def get_flights(self, params: dict) -> dict:
        """ Get all flights by params.

        :param params: dict with data
                       Example: {'origin': 'MOW',
                                 'destination': 'LED',
                                 'depart_date': '2019-11',
                                 'return_date': '2019-12',
                                 ...
                                 }
        :return:       list of flights.
        """
        resp = await self.get(url=self.base_url,
                              headers=self.aviasales_headers,
                              params=params)
        return resp.json().get('data', list)

    async def get_countries(self) -> dict:
        """ Get info about countries.

        :param params: dict with data
        :return:       dict with countries data.
        """
        resp = await self.get(url=self.country_url)
        return resp.json()

    async def get_cities(self) -> dict:
        """ Get info about cities.

        :param params: dict with data
        :return:       dict with cities data.
        """
        resp = await self.get(url=self.city_url)
        return resp.json()

    async def get_cities_flights(self, origin: str, depart_date: str,
                                 return_date: str, cities: list, limit=10) -> list:
        """ Get flights by cities data.

        :param origin:      start city.
        :param depart_date: flights depart date.
        :param return_date: flights return date.
        :param cities:      list of cities IATA codes.
        :param limit:       number of flights.
        :return:            list of flights.
        """
        city_data = []
        for city in cities:
            data = {'origin': origin,
                    'destination': city,
                    'depart_date': depart_date,
                    'return_date': return_date,
                    'currency': 'usd',
                    'limit': limit,
                    'token': aviasales_token,
                    'beginning_of_period': f'{depart_date[0:-2]}01',
                    'period_type': 'month'
                    }
            city_data.append(data)
        resp = await parser.get_api_data(self.base_url, city_data)
        return resp
