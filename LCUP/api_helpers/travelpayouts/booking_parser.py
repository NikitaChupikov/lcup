""" Module that provide booking API. """
from api_helpers.base_parser import RequestsAsyncParser, AiohttpParser

parser = AiohttpParser()


class Hotellook(RequestsAsyncParser):
    """ Hotellook API instance. """

    @property
    def base_url(self):
        return 'http://engine.hotellook.com/api/v2/cache.json?'

    async def get_hotel(self, params: dict = None) -> dict:
        """ Get hotel info.

        :param params: params for request.
        :return:       dict with hotels.
        """
        resp = await self.get(url=self.base_url,
                              params=params)
        return resp.json()

    async def hotel_by_city(self, cities: list, checkIn: str,
                            checkOut: str, limit=10) -> list:
        """ Get hotels for list of cities.

        :param cities:   list of cities.
        :param checkIn:  checkIn date.
        :param checkOut: checkOut date.
        :param limit:    number of elem in response.
        :return:         list with responses.
        """
        city_data = []
        for city in cities:
            data = {'location': city,
                    'checkIn': checkIn,
                    'checkOut': checkOut,
                    'limit': limit,
                    'currency': 'usd'}
            city_data.append(data)
        resp = await parser.get_api_data(self.base_url, city_data)
        return resp


class TravelataParser(RequestsAsyncParser):
    """ Travelata API instance. """

    base_url = 'http://api-gateway.travelata.ru/'

    @property
    def cheapest_tours_url(self) -> str:
        return f'{self.base_url}statistic/cheapestTours'

    @property
    def country_url(self) -> str:
        return f'{self.base_url}directory/countries'

    @property
    def hotel_url(self) -> str:
        return f'{self.base_url}directory/resortHotels'

    @property
    def hotel_categories_url(self) -> str:
        return f'{self.base_url}directory/hotelCategories'

    @property
    def resorts_url(self) -> str:
        return f'{self.base_url}directory/resorts'

    async def get_cheapest_tours(self, params: dict = None) -> dict:
        """ Get cheapest tours.

        :param params: params for request.
        :return:       dict with cheapest tours data.
        """
        resp = await self.get(url=self.cheapest_tours_url,
                              params=params)
        return resp.json()

    async def get_countries(self, params: dict = None) -> dict:
        """ Get data about countries.

        :param params: params for request.
        :return:       dict with countries data.
        """
        resp = await self.get(url=self.country_url,
                              params=params)
        return resp.json()

    async def get_hotels(self, params: dict = None) -> dict:
        """ Get data about hotels.

        :param params: params for request.
        :return:       dict with hotels data.
        """
        resp = await self.get(url=self.hotel_url,
                              params=params)
        return resp.json()

    async def get_hotel_categories(self, params: dict = None) -> dict:
        """ Get data with hotels categories.

        :param params: params for request.
        :return:       dict with hotels categories.
        """
        resp = await self.get(url=self.hotel_categories_url,
                              params=params)
        return resp.json()

    async def get_resorts(self, params: dict = None) -> dict:
        """ Get resorts info.

        :param params: params for request.
        :return:       dict with resorts.
        """
        resp = await self.get(url=self.resorts_url,
                              params=params)
        return resp.json()
