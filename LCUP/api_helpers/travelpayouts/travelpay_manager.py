""" Module with travelpay instance. """
import asyncio

from api_helpers.travelpayouts.flights_parser import AviasalesParser
from api_helpers.travelpayouts.excursion_parser import Tripster
from api_helpers.travelpayouts.booking_parser import Hotellook

flights_parser = AviasalesParser()
excursions_parser = Tripster()
booking_parser = Hotellook()


class TravelPayManager:
    """ Travel instance that includes all API's"""

    @staticmethod
    async def get_flights(origin: str, cities: list,
                          depart_date: str, return_date: str, limit=10) -> list:
        """ Get list of flights by cities.

        :param origin:      city start point.
        :param cities:      list of cities.
        :param depart_date: depart date.
        :param return_date: return date.
        :param limit:       number of flights for cities.
        :return:            list of flights.
        """
        return await flights_parser.get_cities_flights(cities=cities,
                                                       origin=origin,
                                                       depart_date=depart_date,
                                                       return_date=return_date,
                                                       limit=limit)

    @staticmethod
    async def get_excursions(cities: list, limit=10, citytag=None) -> list:
        """ Get list of excursions by cities.

        :param cities:  list of excursions.
        :param limit:   number of excursions for cities.
        :param citytag: (option) int rubric id of excursion.
        :return:        list of excursions.
        """
        return await excursions_parser.get_trip_by_cities(cities,
                                                          limit,
                                                          citytag)

    @staticmethod
    async def get_hotels(cities: list, checkIn: str,
                         checkOut: str, limit=10) -> list:
        """ Get list of hotels by cities.

        :param cities:   list of cities.
        :param checkIn:  date to check in.
        :param checkOut: date to check out.
        :param limit:    number of hotels for cities.
        :return:         list of hotels.
        """
        return await booking_parser.hotel_by_city(cities, checkIn,
                                                  checkOut, limit)

    async def get_trip(self, origin: str, cities: list, lata_cities: list,
                       depart_date: str, return_date: str, limit=10) -> tuple:
        """ Get tours by list of cities.

        :param origin:      city start point.
        :param cities:      list of cities.
        :param lata_cities: IATA codes of cities.
        :param depart_date: depart date.
        :param return_date: return date.
        :param limit:       number of tours for cities.
        :return:            tuple(flights, hotels, excursions)
        """
        tasks = [asyncio.ensure_future(self.get_flights(origin,
                                                        lata_cities,
                                                        depart_date,
                                                        return_date,
                                                        limit)),
                 asyncio.ensure_future(self.get_excursions(cities, limit)),
                 asyncio.ensure_future(self.get_hotels(cities, depart_date,
                                                       return_date, limit)), ]
        flights, excursions, hotels = await asyncio.gather(*tasks)
        return flights, hotels, excursions

    async def hotels_and_flights(self, origin: str, cities: list,
                                 lata_cities: list, depart_date: str,
                                 return_date: str, limit=10) -> tuple:
        """ Get hotels and flights by list of cities.

        :param origin:      city start point.
        :param cities:      list of cities.
        :param lata_cities: IATA codes of cities.
        :param depart_date: depart date.
        :param return_date: return date.
        :param limit:       number of tours for cities.
        :return:            tuple(flights, excursions, hotels)
        """
        tasks = [asyncio.ensure_future(self.get_flights(origin,
                                                        lata_cities,
                                                        depart_date,
                                                        return_date,
                                                        limit)),
                 asyncio.ensure_future(self.get_hotels(cities, depart_date,
                                                       return_date, limit)), ]
        flights, hotels = await asyncio.gather(*tasks)
        return flights, hotels

    @staticmethod
    async def city_excursions(city_name: str, page_size=10,
                              citytag=None, page=1) -> dict:
        """ Get excursions by city name and tag.

        :param city_name: name of the city.
        :param page_size: number of page.
        :param citytag:   (option) int rubric id of excursion.
        :param page_size: number of excursions on page.
        :param page:      page number.
        :return:          dict with city excursions.
        """
        return await excursions_parser.get_trip_by_city(city_name, page_size,
                                                        citytag, page)

    @staticmethod
    async def city_excursions_rubrics(city_name: str) -> dict:
        """ Get city excursion rubrics by name of the city.

        :param city_name: name of the city.
        :return:          dict with city rubrics.
        """
        return await excursions_parser.get_city_rubrics(city_name)

    @staticmethod
    async def cities_excursions_rubrics(cities: list) -> dict:
        """ Get city excursion rubrics by cities list.

        :param cities: list of cities.
        :return:       dict with cities rubrics.
        """
        return await excursions_parser.get_cities_rubrics(cities)

    @staticmethod
    async def city_excursions_and_rubrics(city_name: str, page_size=10,
                                          citytag=None) -> tuple:
        """ Get excursions with rubrics.

        :param city_name: name of the city.
        :param page_size: number of pages.
        :param citytag:   (option) int rubric id of excursion.
        :return:          tuple(rubrics, excursions)
        """
        tasks = [asyncio.ensure_future(excursions_parser.
                                       get_city_rubrics(city_name)),
                 asyncio.ensure_future(excursions_parser.
                                       get_trip_by_city(city_name,
                                                        page_size,
                                                        citytag))
                 ]
        rubrics, excursions = await asyncio.gather(*tasks)
        return rubrics, excursions

    @staticmethod
    async def cities_excursions_and_rubrics(cities: list, page_size=10,
                                            citytag=None) -> tuple:
        """ Get excursions with rubrics by list of the cities.

        :param cities:    list of cities.
        :param page_size: number of pages.
        :param citytag:   (option) int rubric id of excursion.
        :return:          tuple(rubrics, excursions)
        """
        tasks = [asyncio.ensure_future(excursions_parser.
                                       get_cities_rubrics(cities)),
                 asyncio.ensure_future(excursions_parser.
                                       get_trip_by_cities(cities,
                                                          page_size,
                                                          citytag))
                 ]
        rubrics, excursions = await asyncio.gather(*tasks)
        return rubrics, excursions
