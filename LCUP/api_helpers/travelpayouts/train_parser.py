""" Module for train parsing. """
from aiohttp import ClientSession
from nested_lookup import nested_delete


class TutuTrains:
    """ Tutu.ru train parser instance. """

    @property
    def search_url(self):
        return r'https://suggest.travelpayouts.com/search?'

    @property
    def point_url(self):
        return r'https://www.tutu.ru/suggest/railway_simple/'

    @staticmethod
    async def __form_url(tour: dict, date: str) -> str:
        """ Form tutu.ru order url.

        :param tour: dict with tour data.
        :param date: tour date.
        :return:
        """
        url = f'https://www.tutu.ru/poezda/order/?' \
              f'dep_st={tour.get("departureStation", "")}&' \
              f'arr_st={tour.get("arrivalStation", "")}&' \
              f'tn={tour.get("numberForUrl", "")}&' \
              f'date={date}'
        return url

    async def form_resp(self, tour: dict, date: str) -> dict:
        """ Form response (remove keys and add url)

        :param tour: train tour information.
        :param date: depart date.
        :return:     sorted dict with tour data.
        """
        tour['url'] = await self.__form_url(tour, date)
        nested_delete(tour, 'name', in_place=True)
        nested_delete(tour, 'firm', in_place=True)
        nested_delete(tour, 'departureStation', in_place=True)
        nested_delete(tour, 'arrivalStation', in_place=True)
        nested_delete(tour, 'runDepartureStation', in_place=True)
        nested_delete(tour, 'runArrivalStation', in_place=True)
        return tour

    async def get_endpoint_id(self, endpoint: str) -> dict:
        """ Get endpoint ID by name.

         :param endpoint: endpoint name (Example: Minsk).
         :return:         response in .json format.
         """
        param = {'name': endpoint}
        headers = {"Referer": self.point_url}
        async with ClientSession(headers=headers) as session:
            async with session.get(self.point_url, params=param) as response:
                return await response.json(content_type='text/html')

    async def get_tours_by_ids(self, depart_id: int, arrival_id: int) -> dict:
        """ Get train tours information by endpoints ID's.

        :param depart_id:  depart endpoint id.
        :param arrival_id: arrival endpoint id.
        :return:           response in .json format.
        """
        headers = {"Referer": self.search_url}
        param = {
            'service': 'tutu_trains',
            'term': depart_id,
            'term2': arrival_id,
        }
        async with ClientSession(headers=headers) as session:
            async with session.get(self.search_url, params=param) as response:
                return await response.json()
