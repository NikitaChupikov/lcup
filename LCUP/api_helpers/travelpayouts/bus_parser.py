""" Module for bus tours parses. """
from datetime import datetime
from urllib.parse import quote
from nested_lookup import nested_lookup as ns

from configs.config import marker
from api_helpers.base_parser import AiohttpParser

CURRENCY = 'USD'


class TutuBuses(AiohttpParser):
    """ Instance for provide information from tutu.ru """

    def __init__(self):
        self.stops_url = r'https://bus.tutu.ru/api/v1/geo/geopoint_incline/'
        self.search_url = r'https://bus.tutu.ru/api/v1/search/'

    @staticmethod
    def _form_url(req_param: dict, bus_tour: dict) -> str:
        """ Form url for tut.ru partner.

        :param req_param: params for tutu.ru request.
        :param bus_tour:  tutu.ru response.
        :return:          partner url.
        """
        url = f'https://bus.tutu.ru/seats/?' \
              f'search[from]={req_param.get("departureId")}' \
              f'&search[to]={req_param.get("arrivalId")}' \
              f'&search[on]={req_param.get("departureDate")}' \
              f'&search[passengers]=1' \
              f'&trip[start_time]=' \
              f'{bus_tour.get("departureDateTime").get("displayTime")}' \
              f'&trip[hash]={bus_tour.get("hash")}'
        return f'https://tp.media/r?marker={marker}&p=1294&u={quote(url)}'

    @staticmethod
    def _check_endpoints(endpoints) -> list:
        """ Check endpoints response.

        :param endpoints: response with endpoints.
        :return:          endpoints/error message.
        """
        for endpoint in endpoints:
            if endpoint.get('error'):
                return [endpoint]
        return endpoints

    @staticmethod
    def __form_endpoints(endpoints: list) -> list:
        """ Form param for tutu.ru endpoint requests.

        :param endpoints: endpoints names.
        :return:          [{'name': 'Minsk'}, ...]
        """
        return [{'name': endpoint} for endpoint in endpoints]

    @staticmethod
    def __get_endpoints_keys(stop: dict) -> tuple:
        """ Get keys from bus stop dictionary.

        :param stop: bus stop dictionary.
        :return:     tuple(id, ru_name, en_name).
        """
        return ns('id', stop)[0], ns('name', stop)[0], ns('name', stop)[1]

    @staticmethod
    def __get_bus_stops(resp_data: dict) -> list:
        """ Get bus stops name from endpoints response.

        :param resp_data: endpoints response.
        :return:          [{bus stop id: bus stop name}, ...]
        """
        bus_stops = ns('busStops', resp_data)
        if bus_stops:
            stops = bus_stops[0]
            return [{ns('id', stop)[0]: ns('name', stop)[0]} for stop in stops]

    @staticmethod
    def __form_params(depart_id: int, arrival_id: int, date: str,
                      seat_count: int = 1, currency: str = CURRENCY) -> dict:
        """ Form params for requests.

        :param depart_id:   depart endpoint id (bus stop).
        :param arrival_id:  arrival endpoint id (bus stop).
        :param date:        date for you tour.
        :param seat_count:  count of seats.
        :param currency:    cash value in response.
        :return:            formed params for request.
        """
        return {
            'departureId': depart_id,
            'arrivalId': arrival_id,
            'currency': currency,
            'seatCount': seat_count,
            'departureDate': date
        }

    def __endpoints_resp(self, bus_stops: list) -> list:
        """ Get id, ru_name and en_name from endpoints response.

        :param bus_stops: bus stops information.
        :return:          [(1297863, 'Минск', 'Minsk'), ...]
        """
        return [self.__get_endpoints_keys(stop) for stop in bus_stops]

    def __form_resp(self, resp_data, bus_stops: list, param: dict) -> list:
        """ Form API response data with bus tours.

        :param resp_data: tutu.ru response data.
        :param bus_stops: bus stops names.
        :param param:     params for tutu.ru request.
        :return:          [{bus tour}, ...]
        """
        resp = []
        for bus_tour in resp_data.get('offers'):
            tour = {
                'currency': CURRENCY,
                'price': bus_tour.get('price').get('value'),
                'priceOriginal': bus_tour.get('priceOriginal').get('value'),
                'departureDateTime': bus_tour.get('departureDateTime').get(
                    'displayTime'),
                'arrivalDateTime': bus_tour.get('arrivalDateTime').get(
                    'displayTime'),
                'departureDate': param.get('departureDate'),
                'url': self._form_url(param, bus_tour),
                'arrival_point': ns(bus_tour.get('arrivalPoint'), bus_stops)[0],
                'depart_point': ns(bus_tour.get('departurePoint'), bus_stops)[0]
            }
            resp.append(tour)
        return resp

    async def get_endpoints(self, endpoints: list) -> list:
        """ Get endpoints ID's by names.

        :param endpoints: list of names (Example: [Berlin, Minsk, Brest])
        :return:          list of endpoints [(1297863, 'Минск', 'Minsk'), ...]
        """
        endpoints = self.__form_endpoints(endpoints)
        stops = await self.get_api_data(self.stops_url, endpoints)
        stops = self._check_endpoints(stops)
        if not stops[0].get('data'):
            return stops[0]
        else:
            return self.__endpoints_resp(stops)

    async def get_bus_tours(self, depart_id: int, arrival_id: int,
                            date: str) -> list:
        """ Get bus tours by cities ID's (Example: 1297863 -> 1297478)

        :param depart_id:  depart city ID (Example: 1297863 - Minsk)
        :param arrival_id: arrival city ID (Example: 1297478 - Molodechno)
        :param date:       date in format 'YYYY-MMMM-DDDD'.
        :return:           list with bus tours or error message.
        """
        param = self.__form_params(depart_id, arrival_id, date)
        tours = await self.get_api_data(self.search_url, [param])
        if tours[0].get('data'):
            bus_stops = self.__get_bus_stops(tours)
            return self.__form_resp(tours[0].get('data'), bus_stops, param)
        else:
            return tours[0]

    async def bus_tours(self, depart: str, arrival: str,
                        date: str = '') -> list:
        """ Get bus tours by cities names (Example: Minsk -> Berlin)

        :param depart:  depart endpoint(Example: Minsk).
        :param arrival: arrival endpoint(Example: Berlin).
        :param date:    date in format 'YYYY-MMMM-DDDD'.
        :return:        list with bus tours or error message.
        """
        if not date:
            today = datetime.now()
            date = str(today.date())
        endpoints = await self.get_endpoints([depart, arrival])
        if isinstance(endpoints, list):
            depart_id, arrival_id = endpoints
            return await self.get_bus_tours(depart_id[0], arrival_id[0], date)
        else:
            return endpoints
