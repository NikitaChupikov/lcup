""" Module that provide excutsions API. """
from api_helpers.base_parser import RequestsAsyncParser
from api_helpers.base_parser import AiohttpParser

parser = AiohttpParser()


class Tripster(RequestsAsyncParser):
    """ Tripster API instance. """

    base_url = 'https://experience.tripster.ru/api/experiences/'
    rubric_url = r'https://experience.tripster.ru/api/citytags/'

    async def get_trip_by_city(self, city_name: str, page_size=10,
                               citytag=None, page=1) -> dict:
        """ Get excursions by city name.

        :param city_name: name of the city.
        :param page_size: number of excursions on page.
        :param page:      page number.
        :param citytag:   (option) int rubric id of excursion.
        :return:          dict with response.
        """
        data = {'city__name_en': city_name,
                'format': 'json',
                'page': page,
                'page_size': page_size}
        if citytag:
            data['citytag'] = citytag
        resp = await self.get(url=self.base_url,
                              params=data)
        return resp.json()

    async def get_trip_by_cities(self, cities: list,
                                 page_size=10, citytag=None):
        """ Get excursions for list of cities.

        :param cities:    list of cities.
        :param page_size: number of elem on page.
        :param citytag:   (option) int rubric id of excursion.
        :return:          list with cities excursions.
        """
        city_data = []
        for city in cities:
            data = {'city__name_en': city,
                    'format': 'json',
                    'page_size': page_size,
                    'currency': 'USD'}
            if citytag:
                data['citytag'] = citytag
            city_data.append(data)
        resp = await parser.get_api_data(self.base_url, city_data)
        return resp

    async def get_city_rubrics(self, city_name: str) -> dict:
        """ Get all excursion rubrics by city name.

        :param city_name: name of the city.
        :return:          dict with response.
        """
        data = {'city__name_en': city_name,
                'format': 'json'}
        resp = await self.get(url=self.rubric_url, params=data)
        return resp.json()

    async def get_cities_rubrics(self, cities) -> dict:
        """ Get all excursion rubrics by cities name.

        :param cities: list of cities.
        :return:       dict with response.
        """
        city_data = []
        for city in cities:
            data = {'city__name_en': city,
                    'format': 'json'}
            city_data.append(data)
        resp = await parser.get_api_data(self.rubric_url, city_data)
        return resp
