"""
 Epidemic (COVID-19) data provider module from the following pages:
 ref: https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic_by_country_and_territory
 ref: https://coronavirus-tracker-api.herokuapp.com
 ref: https://www.trip.com/travel-restrictions-covid-19/

 More info:
 ref: https://pypi.org/project/COVID19Py/
 ref: https://habr.com/ru/post/496144/
"""
import os
import json
import requests
import pandas as pd

from bs4 import BeautifulSoup
from COVID19Py import COVID19

trip_covid = r'https://www.trip.com/travel-restrictions-covid-19/'
wiki_covid = r'https://en.wikipedia.org/wiki/' \
             r'COVID-19_pandemic_by_country_and_territory'
wiki_restr = r'https://en.wikipedia.org/wiki/' \
             r'Travel_restrictions_related_to_the_COVID-19_pandemic'

session = requests.Session()


class CovidParser(COVID19):
    """ Instance that parse covid-19 data from different sources."""

    @property
    def html_code(self):
        """ Html text of trip.com epidemic web page"""
        return session.get(trip_covid).text

    @property
    def soup(self):
        """ BeautifulSoup instance. """
        return BeautifulSoup(self.html_code, 'html.parser')

    @property
    def countries_contents(self) -> list:
        """ Find all country html block contents from trip.com

        :return: list with html contents.
        """
        class_name = {"class": "country-content"}
        return self.soup.findAll("div", class_name)

    @property
    def update_date(self) -> str:
        """ Get date of updating data from trip.com

        :return: date of updating.
        """
        class_name = {"class": "r5mvo9-9 huLDuD"}
        date = self.soup.findAll("div", class_name)[0]
        return date.text

    @staticmethod
    def form_country_resp(name, status, content) -> dict:
        """ Form .json response for API.

        :param name:     name of the country.
        :param status:   covid-19 status of country.
        :param content:  country content.
        :return:
        """
        return {
            'name': name,
            'status': status,
            'content': content,
        }

    @staticmethod
    def write_json_file(filename: str, data: dict) -> None:
        """ Write .json data to file.

        :param filename: name of .json file.
        :param data:     .json data
        """

        with open(filename, 'w') as file:
            json.dump(data, file)
            file.close()

    @staticmethod
    def get_wiki_report():
        """ Get epidemic statistic by wiki

        :return: pandas data frame with data.
        """
        response = session.get(wiki_covid)
        df = pd.read_html(response.content)[1]
        df = pd.DataFrame(
            df.values[:, 1:5],
            columns=['country', 'confirmed', 'deaths', 'recovered']
        )
        return df

    def get_countries_info(self) -> list:
        """ Get list of countries with entry info.

        :return: list of countries with covid info.
        """
        countries = []
        for country in self.countries_contents:
            name = country.contents[0].contents[0].text
            status = country.contents[0].contents[1].text
            content = country.contents[1].text
            data = self.form_country_resp(name, status, content)
            countries.append(data)
        return countries

    def sort_status_countries(self, status: str) -> list:
        """ Get countries with normal COVID-19 status.

        :param status: COVID-19 status of country.
        :return:
        """
        if status in ('Normal', 'Entry prohibited', 'Partially restricted'):
            normal_countries = []
            for country in self.get_countries_info():
                if country.get('status') == status:
                    normal_countries.append(country)
            return normal_countries

    def get_entry_info(self) -> dict:
        """ Form dict for trip.com response.

        :return: dict(update,resources_url, counties)
        """
        return {
            'update': self.update_date,
            'resources_url': trip_covid,
            'countries': self.get_countries_info(),
            'normal': self.sort_status_countries('Normal'),
            'restricted': self.sort_status_countries('Partially restricted'),
            'prohibited': self.sort_status_countries('Entry prohibited'),
        }

    def _update_web_info(self) -> None:
        """ Update information in static files. """
        dataset = (self.getLocations(),
                   self.get_entry_info(),)
        filenames = ('counties_info.json',
                     'entry_info.json')
        for data, filename in zip(dataset, filenames):
            full_path = os.path.abspath(filename)
            media = full_path.replace('api_helpers', 'media')
            covid.write_json_file(media, data)


if __name__ == '__main__':
    covid = CovidParser()
    covid._update_web_info()
