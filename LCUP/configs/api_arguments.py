positional_arguments = [
    'depart_date',
    'return_date',
    'origin'
]

NO_PARAMS_ERROR = {
    "errorCode": 400,
    "message": "query: Parameter is missing",
    "status": "OK"
}

INVALID_PARAMS = {
    "errorCode": 400,
    "message": "query: Missing required positional arguments ",
    "positional_arguments": positional_arguments,
    "status": "error"
}

INSTAGRAM_PARAMS = {
    "status": "error",
    "errorCode": 400,
    "message": "query: Missing required positional arguments 'insta_user'"
}

AUTH_ERROR = {
    "status": "OK",
    "errorCode": 4,
    "message": "User is not authenticated"
}

SAVE_ERROR = {
    "status": "error",
    "errorCode": 5,
    "message": "Failed to save user answers."
}
INSTA_USER_ALREADY_EXISTS = {
    "errorCode": 8,
    "message": "Instagram user already exists in DB",
    "status": "error"
}

USER_ID_DOESNT_EXIST = {
    "errorCode": 9,
    "message": "User id doesn't exist, please Sign up first",
    "status": "error"
}


SAVE_USER = {
    "status": "OK",
    "message": "User answers have been saved."
}

IATA_CODES = {
    'Addis Ababa': 'ADD',
    'Akureyri': 'AEY',
    'Anchorage': 'ANC',
    'Ankara': 'ANK',
    'Antalya': 'AYT',
    'Bangkok': 'BKK',
    'Barrow': 'BRW',
    'Beijing': 'BJS',
    'Amsterdam': 'AMS',
    'Athens': 'ATH',
    'Berlin': 'BER',
    'Berne': 'BRN',
    'Bolzano': 'BZO',
    'Brussels': 'BRU',
    'Budapest': 'BUD',
    'Calgary': 'YYC',
    'Cuzco': 'CUZ',
    'Delhi': 'DEL',
    'Dublin': 'DUB',
    'Desroches': 'DES',
    'Freiburg': 'QFB',
    'Hakodate': 'HKD',
    'Hanoi': 'HAN',
    'Harbour Island': 'HBI',
    'Helsinki': 'HEL',
    'Hurghada': 'HRG',
    'Ilulissat': 'JAV',
    'Innsbruck': 'INN',
    'Ivano-Frankovsk': 'IFO',
    'Kingston': 'KIN',
    'Kiruna': 'KRN',
    'Kochi': 'KCZ',
    'Kumamoto': 'KMJ',
    'Lima': 'LIM',
    'Lisbon': 'LIS',
    'London': 'LON',
    'Lviv': 'LWO',
    'Madrid': 'MAD',
    'Mahe Island': 'SEZ',
    'Mexico City': 'MEX',
    'Murmansk': 'MMK',
    'Nassau': 'NAS',
    'Neerlerit Inaat': 'CNP',
    'Nuuk': 'GOH',
    'Osaka': 'OSA',
    'Oslo': 'OSL',
    'Oulu': 'OUL',
    'Petropavlovsk-Kamchats': 'PKC',
    'Phnom Penh': 'PNH',
    'Reykjavik': 'REK',
    'Rome': 'ROM',
    'Shanghai': 'SHA',
    'Tasiilaq': 'XEQ',
    'Tel Aviv-Yafo': 'TLV',
    'Tokio': 'TYO',
    'Tromso': 'TOS',
    'Ulaanbaatar': 'ULN',
    'Valencia': 'VLC',
    'Varadero': 'VRA',
    'Varna': 'VAR',
    'Vienna': 'VIE',
    'Yakutsk': 'YKS',
    'Kiev': 'IEV',
    'Prague': 'PRG',
    'Barcelona': 'BCN',
    'Minsk': 'MSQ'
}
