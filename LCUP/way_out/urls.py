from django.urls import path
from django.conf.urls import url

from .views import WayOutView, InstagramData, MachineLearn, \
    current_user, UserList, UserInfoSave, Excursions, \
    Transport, TokenAuth

urlpatterns = [
    path(r'', WayOutView.way_out),
    path(r'hotels', WayOutView.hotels),
    path(r'current_user/', current_user),
    path(r'users/', UserList.as_view()),
    path('token-auth/', TokenAuth.as_view()),
    url(r'insta', InstagramData.get_open_inst),
    url(r'machine_learn', MachineLearn.ml_results),
    url(r'ml_params', MachineLearn.get_ml_params),
    url(r'save_user_ans', UserInfoSave.save_answers),
    url(r'tripster', Excursions.excursions_and_rubrics),
    url(r'rubrics', Excursions.rubrics),
    url(r'excursions', Excursions.excursions),
    url(r'news', Excursions.news),
    url(r'bus', Transport.bus),
    url(r'train', Transport.train),
    url(r'plane', Transport.plane),
    url(r's_location', InstagramData.social_ml),
]
