from django.apps import AppConfig


class WayOutConfig(AppConfig):
    name = 'way_out'
