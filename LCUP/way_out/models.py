from django.db import models
from django.contrib.auth.models import User


class File(models.Model):
    title = models.TextField()
    cover = models.FileField()

    def __str__(self):
        return self.title


class Post(models.Model):
    ru_title = models.CharField(max_length=200, default='', blank=True,
                                null=True)
    en_title = models.CharField(max_length=200, default='', blank=True,
                                null=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    image_url = models.CharField(max_length=4000, default='', blank=True,
                                 null=True)
    text = models.TextField(max_length=4000, blank=True, default='')
    rus_text = models.TextField(max_length=4000, blank=True, default='')

    def __str__(self):
        return f'{self.ru_title} - {self.en_title}'


class CityCodes(models.Model):
    time_zone = models.CharField(max_length=50, default='', blank=True,
                                 null=True)
    name = models.CharField(max_length=50, default='', blank=True, null=True)
    rus_name = models.CharField(max_length=50, default='', blank=True,
                                null=True)
    lon = models.CharField(max_length=20, default='', blank=True, null=True)
    lat = models.CharField(max_length=20, default='', blank=True, null=True)
    code = models.CharField(max_length=3, default='', blank=True, null=True)
    country_code = models.CharField(max_length=3, default='', blank=True,
                                    null=True)

    def __str__(self):
        return f'{self.name} - {self.code}'


class HotellookCodes(models.Model):
    city_id = models.CharField(max_length=50, default='', blank=True, null=True)
    country_id = models.CharField(max_length=50, default='', blank=True,
                                  null=True)
    lon = models.CharField(max_length=20, default='', blank=True, null=True)
    lat = models.CharField(max_length=20, default='', blank=True, null=True)
    name = models.CharField(max_length=120, default='', blank=True, null=True)
    rus_name = models.CharField(max_length=120, default='', blank=True,
                                null=True)

    def __str__(self):
        return f'{self.name} - {self.city_id}'


class AccountSocial(models.Model):
    social_id = models.CharField(max_length=20, null=False, primary_key=True)
    username = models.CharField(max_length=30, null=True)
    provider = models.CharField(max_length=10, null=True)
    token = models.CharField(max_length=150, null=True)
    expires_at = models.DateTimeField(null=True)
    insert_ts = models.DateTimeField(null=True)

    def __str__(self):
        return f'{self.social_id} - {self.username} - ' \
               f'{self.token} - {self.insert_ts}'

    def get_username(self):
        return self.username

    def get_token(self):
        return self.token

    def get_expire_date(self):
        return self.expires_at


class UserAnswer(models.Model):
    username = models.CharField(max_length=20)
    data = models.TextField()

    def __str__(self):
        return self.username


class InstagramLocation(models.Model):
    country = models.CharField(max_length=80, default='', blank=True, null=True)
    location = models.CharField(max_length=80, default='', blank=True, null=True)
    location_id = models.CharField(max_length=80, default='', blank=True, null=True)

    def __str__(self):
        return self.location
