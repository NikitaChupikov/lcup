from django.contrib import admin
from .models import File, CityCodes, \
    HotellookCodes, AccountSocial, \
    UserAnswer, Post, InstagramLocation

PER_PAGE = 10


class FileAdmin(admin.ModelAdmin):
    search_fields = ['id', 'title']
    list_display = ('id', 'title', 'cover')
    list_per_page = PER_PAGE


class CityCodesAdmin(admin.ModelAdmin):
    search_fields = ['name', 'rus_name', 'lon', 'lat', 'code', 'country_code', 'time_zone']
    list_display = ('name', 'rus_name', 'code', 'country_code')
    list_per_page = PER_PAGE


class HotellookCodesAdmin(admin.ModelAdmin):
    search_fields = ['city_id', 'country_id', 'name', 'rus_name', 'lon', 'lat']
    list_display = ('city_id', 'country_id', 'name', 'rus_name')
    list_per_page = PER_PAGE


class AccountSocialAdmin(admin.ModelAdmin):
    search_fields = ['social_id', 'username', 'provider', 'token', 'expires_at', 'insert_ts']
    list_display = ('social_id', 'username', 'provider', 'insert_ts')
    list_per_page = PER_PAGE


class UserAnserAdmin(admin.ModelAdmin):
    search_fields = ['username', 'data']
    list_display = ('username', 'data')
    list_per_page = PER_PAGE


class PostAdmin(admin.ModelAdmin):
    search_fields = ['ru_title', 'en_title', 'owner', 'created']
    list_display = ('ru_title', 'en_title', 'owner', 'created')
    list_per_page = PER_PAGE


class InstagramLocationAdmin(admin.ModelAdmin):
    search_fields = ['country', 'location', 'location_id']
    list_display = ('country', 'location', 'location_id')
    list_per_page = PER_PAGE


admin.site.register(File, FileAdmin)
admin.site.register(CityCodes, CityCodesAdmin)
admin.site.register(HotellookCodes, HotellookCodesAdmin)
admin.site.register(AccountSocial, AccountSocialAdmin)
admin.site.register(UserAnswer, UserAnserAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(InstagramLocation, InstagramLocationAdmin)
