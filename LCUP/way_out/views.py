from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.throttling import UserRateThrottle
from rest_framework import permissions, status
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import JSONParser

from datetime import datetime
from configs.logger import logger

from .serializers import UserSerializer, UserSerializerWithToken
from .models import CityCodes, Post, AccountSocial, UserAnswer

from tour_managers.tour_manager import TourManager
from tour_managers.ml_arguments import MachineLearnQuestions
from tour_managers.insta_manager import InstaManager
from tour_managers.user_manager import UserManager
from tour_managers.excursion_manager import ExcursionManager
from tour_managers.tutu_manager import TutuTransport
from tour_managers.insta_auth import InstaAuth
from configs.api_arguments import positional_arguments
from rest_framework_jwt.views import ObtainJSONWebToken
from LCUP.utils import my_jwt_response_handler as jwt_response_payload_handler
from rest_framework_jwt.views import api_settings

tour_manager = TourManager()
insta_manager = InstaManager()
ml_params = MachineLearnQuestions()
user_manager = UserManager()
excursion_manager = ExcursionManager()
tutu_transport = TutuTransport()
insta_auth = InstaAuth()


class WayOutView(APIView):
    throttle_classes = [UserRateThrottle]

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def api_root(request):
        """ Примеры всех возможных ссылок.

        <b>Параметры запроса:</b>
        <ul>
            <li>lang — язык описания API, принимает значения en и ru. (ru as default)</li>
        <ul>
        """
        if request.GET.get('lang') == 'en':
            return Response(tour_manager.api_root(lang='en'))
        else:
            return Response(tour_manager.api_root(), status=status.HTTP_200_OK)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def hotels(request):
        """
        <b>Параметры запроса:</b>
        <ul>
            <li><b>location</b> — город в котором ищем отель (Примеры: Minsk )</li>
            <li><b>checkIn</b> — дата начала проживания (YYYY-MM-DD)</li>
            <li><b>checkOut</b> — дата окончания проживания (YYYY-MM-DD)</li>
            <li><b>limit (optional)</b> — кол-во отелей в ответе. </li>
        <ul>
        """
        if request.GET:
            response = tour_manager.hotels(request.GET)
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response(tour_manager.no_param_error,
                            status.HTTP_400_BAD_REQUEST)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def way_out(request):
        """
        <b>Пример запроса: </b>
        <ul>
            <code>
                /api/?origin=Minsk&depart_date=2020-05-15&return_date=2020-05-18&mode=show_q&destination=&sort=True&limit=1
           </code>
        </ul>
        <b>Параметры запроса:</b>
        <ul>
            <li><b>depart_data</b> —дата отправление (Пример: depart_date=2020-03-22)</li>
            <li><b>return_date</b> —дата возвращения (Пример: return_date=2020-03-30)</li>
            <li><b>destination (optional)</b> — направление тура (Пример: destination=Berlin)
            <li><b>limit (optional)</b> —количество отелей/билетов/экскурсий для каждого города от ML (По умолчанию стоит 5)
            <li><b>sort (optional)</b> — сортировать ли данные для вывода во фронт (Принимает значния True или False)
            <li><b>activity_type (optional)</b> —тип активностей для туриста (По умолчанию стоит 'активный')
            <b>Варианты запроса:</b>
            1.	ленивый
            2.	активный
            3.	не уверен, удивите меня
            <li><b>climate (optional)</b>—комфортный климат (По умолчанию 'нет предпочтений')
            <b>Варианты запроса:</b>
            1.	буйство стихии
            2.	настоящая зима и много снега
            3.	нейтральные комфортные дни
            4.	нет предпочтений
            <li><b>interest (optional)</b>—активностей, которыми турист увлекатеся (По умолчанию 'достопримечательности')
            <b>Варианты запроса:</b>
            1.	всего понемногу
            2.	шоппинг
            3.	природа
            4.	достопримечательности
            5.	еда
            <li><b>travel_time (optional)</b>—время в пути (По умолчанию 'несколько ( до 6) часов')
            <b>Варианты запроса:</b>
            1.	не имеет значения
            2.	несколько ( до 6) часов
            3.	как можно меньше
            4.	не имеет значения
        <ul>
        """
        if request.GET:
            resp_keys = set(positional_arguments)
            if resp_keys.issubset(request.GET.keys()):
                return Response(
                    tour_manager.manage_tour(request.GET, CityCodes),
                    status=status.HTTP_200_OK)
            else:
                return Response(tour_manager.invalid_params,
                                status.HTTP_400_BAD_REQUEST)
        else:
            return Response(tour_manager.no_param_error,
                            status.HTTP_400_BAD_REQUEST)


class MachineLearn(APIView):
    throttle_classes = [UserRateThrottle]

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def ml_results(request):
        """ Запрос для обращения к ML для получения направлений поездки.

        Параметры запроса можно посмотреть по endpoint-у
        <ul>
            <code>
                /api/ml_params
           </code>
        </ul>
        """
        if request.GET:
            return Response(tour_manager.get_ml_ans(request.GET),
                            status=status.HTTP_200_OK)
        else:
            return Response(tour_manager.get_ml_ans({}),
                            status=status.HTTP_200_OK)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def get_ml_params(request):
        """ Параметры запроса для ML .
        Если пользователь авторизован, то ему выдается его сохраненный
        и полный список вопросов .

        <b>Параметры запроса:</b>
        <ul>
            <li><b>search (optional)</b> — поиск по ключу
            (Например search=question выдает только заголовки вопросов.)</li>
        <ul>
        """
        if request.user.is_authenticated:
            username = request.user.get_username()
            user_answers = user_manager.get_user_answers(username)
            return Response(user_answers, status=status.HTTP_200_OK)
        else:
            return Response(ml_params.get_questions(request.GET, auth=False))


class InstagramData(APIView):
    throttle_classes = [UserRateThrottle]

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def get_open_inst(request):
        """ Поиск открытой информации с instagram аккаунта.

        <b>Параметры запроса:</b>
        <ul>
            <li><b>insta_user</b> — ваш username в instagram 
            (аккаунт должен быть открытй)</li>
        <ul>
        """
        if request.GET:
            return Response(insta_manager.parse(request.GET),
                            status=status.HTTP_200_OK)
        else:
            return Response(insta_manager.invalid_params,
                            status.HTTP_400_BAD_REQUEST)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def social_ml(request):
        """ Поиск локаций по вашему instagram аккаунту.

        <b>Параметры запроса:</b>
        <ul>
            <li><b>username</b> — ваш username в instagram </li>
        <ul>
        """
        if 'username' in request.GET:
            username = request.GET.get('username', '')
            resp = insta_manager.user_recommend(username, 3)
            return Response(resp)
        else:
            return Response({'status': 'error', 'code': '400',
                             'message': 'Bad request: user param "username" '
                                        'in your requests'},
                            status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def current_user(request):
    serializer = UserSerializer(request.user)
    username = serializer.data.get('username', '')
    answers = UserAnswer.objects.filter(username=username)
    if answers:
        user_answers = eval(answers[0].data)
    else:
        user_answers = {}
    resp = {'user': serializer.data, 'answers': user_answers}
    return Response(resp)


class TokenAuth(ObtainJSONWebToken):
    """ Override JWT. """

    def get_instauser_data(self, insta_data: dict, insta_user: str):
        serializer = self.get_serializer(data=insta_data)
        if serializer.is_valid():
            user = insta_user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, 'insta')
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            return response
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        if 'social_flag' in request.data:
            if request.data['social_flag'] == 'insta':
                code = request.data.get('code', '')
                if code.endswith('#_'):
                    code = code[:-2]
                insta_resp = insta_auth.authenticate_user(code)
                if 'errorCode' in insta_resp:
                    logger.error(insta_resp)
                    return Response({'status': insta_resp.get('errorCode', ''),
                                     'message': insta_resp.get('message', '')})
                else:
                    insta_user = insta_resp
                insta_auth.update_token(insta_user)
                insta_data = {'email': insta_user, 'password': insta_user,
                              'username': insta_user}
                return self.get_instauser_data(insta_data, insta_user)
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                user = serializer.object.get('user') or request.user
                token = serializer.object.get('token')
                response_data = jwt_response_payload_handler(token, user, '',
                                                             request)
                response = Response(response_data)
                if api_settings.JWT_AUTH_COOKIE:
                    expiration = (datetime.utcnow() +
                                  api_settings.JWT_EXPIRATION_DELTA)
                    response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                        token,
                                        expires=expiration,
                                        httponly=True)
                return response
            else:
                return Response(serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)


class UserList(APIView):
    """ Registration of the User."""

    permission_classes = (permissions.AllowAny,)

    @staticmethod
    def post(request):
        """ Register user according to body params. """
        social_flag = request.data.get('social_flag', '')
        code = request.data.get('code', '')
        if social_flag:
            if code.endswith('#_'):
                code = code[:-2]
            insta_responce = insta_auth.register_user(code)
            if 'error_code' in insta_responce:
                return Response(insta_responce.get('error_message', ''),
                                insta_responce.get('error_code', ''))
            else:
                account_check = AccountSocial.objects.filter(
                    username=insta_responce.get('username', '')).exists()
                if account_check:
                    return Response(
                        {'status': insta_auth.user_already_exists.get(
                            'errorCode', ''),
                            'message': insta_auth.user_already_exists.get(
                                'message', '')})
                else:
                    username_insta = insta_responce.get('username', '')

                    user_data_insta = ml_params.get_questions(request.GET,
                                                              auth=True)
                    data = {'email': f'{username_insta}@mail.com',
                            'first_name': username_insta,
                            'last_name': username_insta,
                            'password': username_insta,
                            'username': username_insta}
                    serializer = UserSerializerWithToken(data=data)
                    if serializer.is_valid():
                        serializer.save()
                        insta_auth.user_to_db(insta_responce.get('id', ''),
                                              insta_responce.get('username',
                                                                 ''))
                        user_manager.add_new_user_ans(username_insta,
                                                      user_data_insta)
                        return Response(serializer.data,
                                        status=status.HTTP_201_CREATED)
                    else:
                        return Response({'message': 'Error with Serializer',
                                         'code': '403', 'status': 'error'},
                                        status=status.HTTP_403_FORBIDDEN)

        else:
            serializer = UserSerializerWithToken(data=request.data)
            if serializer.is_valid():
                serializer.save()
                username = request.data.get('username', '')
                user_data = ml_params.get_questions(request.GET, auth=True)
                user_manager.add_new_user_ans(username, user_data)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class UserInfoSave:

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def save_answers(request):
        """ Сохранение ответов пользователя.

        Параметры запроса можно посмотреть по endpoint-у
        <ul>
            <code>
                /api/ml_params
           </code>
        </ul>
        """
        if request.GET:
            serializer = UserSerializer(request.user)
            username = serializer.data.get('username', '')
            if username:
                user_data = user_manager.get_user_answers(username)
                user_ans = tour_manager.get_user_answer(request.GET)
                resp = user_manager.save_user_ans(user_ans, user_data, username)
                return Response(resp, status=status.HTTP_200_OK)
            else:
                return Response(user_manager.auth_error,
                                status.HTTP_400_BAD_REQUEST)
        return Response(tour_manager.no_param_error,
                        status.HTTP_400_BAD_REQUEST)


class Excursions:

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def news(request):
        """ Вывод всех новостей из БД. """
        resp = []
        for post in Post.objects.all():
            data = tour_manager.news(post)
            resp.append(data)
        return Response(resp)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def excursions_and_rubrics(request):
        """ Поиск всех возможных рубрик и экскурсий.

        <b>Параметры запроса:</b>
        <ul>
            <li><b>city</b> — город, где нужно заказать экскурсию (Пример: city=Berlin)</li>
        <ul>
        """
        if request.GET:
            city_name = request.GET.get('city', '')
            page_size = request.GET.get('page_size', 20)
            page = request.GET.get('page', 1)
            tag = request.GET.get('tag')
            return Response(excursion_manager.get_excursions(city_name, 1, tag,
                                                             page_size, page),
                            status=status.HTTP_200_OK)
        else:
            return Response(excursion_manager.no_params_error,
                            status.HTTP_400_BAD_REQUEST)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def rubrics(request):
        """ Поиск готовых рубрик по городу.

        <b>Параметры запроса:</b>
        <ul>
            <li><b>city</b> — город, где нужно заказать экскурсию (Пример: city=Berlin)</li>
        <ul>
        """
        if request.GET:
            city_name = request.GET.get('city', '')
            rubrics = excursion_manager.get_excursions(city_name, 2)
            key = 'experience_count'
            response = [rubric for rubric in rubrics if rubric.get(key) > 0]
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response(excursion_manager.no_params_error,
                            status.HTTP_400_BAD_REQUEST)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def excursions(request):
        """ Поиск готовых экскурсий по городу и рубрике.

        <b>Параметры запроса:</b>
        <ul>
            <li><b>city</b> — город, где нужно заказать экскурсию (Пример: city=Berlin)</li>
            <li><b>tag</b> — ·	ID экскурсии в конкретном городе
            (Пример: tag=139 - это рубрика «Бары и ночная жизнь»).
            Тэги можно узнать по запросу по запросу rubrics</li>
        <ul>
        """
        if request.GET:
            city_name = request.GET.get('city', '')
            page_size = request.GET.get('page_size', 20)
            page = request.GET.get('page', 1)
            tag = request.GET.get('tag')
            return Response(excursion_manager.get_excursions(city_name, 3, tag,
                                                             page_size, page),
                            status=status.HTTP_200_OK)
        else:
            return Response(excursion_manager.no_params_error,
                            status.HTTP_400_BAD_REQUEST)


class Transport:
    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def plane(request):
        """ Поиск авиабилетов.

        <b>Пример запроса: </b>
        <ul>
            <code>
                /api/plane?origin=Minsk&depart_date=2020-12-15
                &return_date=2020-12-18&destination=Berlin&limit=3
           </code>
        </ul>
        <b>Параметры запроса:</b>
        <ul>
            <li><b>origin</b> — город отправления (Примеры: Minsk )</li>
            <li><b>destination</b> — станция прибытия (Примеры: Dublin )</li>
            <li><b>depart_date</b> — дата отправления (DD.MM.YYYY)</li>
            <li><b>return_date</b> — дата возвращения (DD.MM.YYYY)</li>
            <li><b>limit (optional)</b> — кол-во билетов в ответе.</li>
        <ul>
        """
        response = tour_manager.flights(request.GET, CityCodes)
        return Response(response)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def bus(request):
        """ Поиск билетов на автобусы.

        <b>Параметры запроса:</b>
        <ul>
            <li><b>arrival</b> — станция отправления (Примеры: Минск)</li>
            <li><b>depart</b> — станция прибытия (Примеры: Брест)</li>
            <li><b>date</b> — дата отправления (DD.MM.YYYY)</li>
        <ul>
        """
        if request.GET:
            if tutu_transport.params.issubset(request.GET.keys()):
                depart = request.GET.get('depart', '')
                arrival = request.GET.get('arrival', '')
                date = request.GET.get('date', '')
                return Response(tutu_transport.get_buses(depart, arrival, date))
            else:
                return Response(tutu_transport.error_param_msg,
                                status.HTTP_400_BAD_REQUEST)
        else:
            return Response(tutu_transport.error_param_msg,
                            status.HTTP_400_BAD_REQUEST)

    @staticmethod
    @api_view(['GET'])
    @parser_classes([JSONParser])
    def train(request):
        """ Поиск ЖД билетов.

        <b>Пример запроса: </b>
        <ul>
            <code>
                /api/train/?date=30.05.2020&arrival=Минск&depart=Брест
           </code>
        </ul>
        <b>Параметры запроса:</b>
        <ul>
            <li><b>arrival</b> — станция отправления (Примеры: Минск)</li>
            <li><b>depart</b> — станция прибытия (Примеры: Брест)</li>
            <li><b>date</b> — дата отправления (DD.MM.YYYY)</li>

        <ul>
        """
        if request.GET:
            if tutu_transport.params.issubset(request.GET.keys()):
                depart = request.GET.get('depart', '')
                arrival = request.GET.get('arrival', '')
                date = request.GET.get('date', '')
                return Response(tutu_transport.get_trains(depart, arrival, date),
                                status=status.HTTP_200_OK)
            else:
                return Response(tutu_transport.error_param_msg,
                                status.HTTP_400_BAD_REQUEST)
        else:
            return Response(tutu_transport.error_param_msg,
                            status.HTTP_400_BAD_REQUEST)
