import React from 'react';

import Questions from '../components/Questions/questions';
import '../index.scss';
import RouteList from "../components/RouteList/routeList";
import {useStateValue} from "../store";

const Home = () => {
    const [store, dispatch] = useStateValue()
    return (
        <div className="body-container">
            {store.defaultMainState.openedPage.isOpenQuestions
                ? <Questions/>
                : store.defaultMainState.openedPage.isOpenedRouteList?<div className="routeList">
                    <RouteList/>
                </div>
                    : null
            }
        </div>
    );
}
export default Home;