import React, {Component, useEffect, useState} from "react";
import PrsnlExcursions from "../components/PrsnlExcursions/prsnlExcursions";
import PrsnlPreferences from "../components/PrsnlPreferences/prsnlPreferences";
import PrsnlSocialMedia from "../components/PrsnlSocialMedia/prsnlSocialMedia";
import PrsnlTours from "../components/PrsnlTours/prsnlTours";
import "../index.scss";
import {useStateValue} from "../store";

const duration_default = 4,
    first_day = 1;

const PrsnlRoom = (props) => {
    const [store, dispatch] = useStateValue()
    const [state, setState] = useState(
        {
            buttonPrefClicked: true,
            buttonExcClicked: false,
            buttonSocMClicked: false,
            buttonToursClicked: false,
            questions: null,
            origin: "Minsk",
            depart_date: "2020-03-20",
            return_date: "2020-03-23",
            mode: "show_q",
            inpValue: "",
            isLoading: true,
            error: null,
            count: 0,
            destination: "",
        }
    )
    const switchContent = () => {
        const {
            buttonExcClicked,
            buttonPrefClicked,
            buttonSocMClicked,
            buttonToursClicked,
        } = state;
        const excursions = state.excursions;
        if (buttonPrefClicked) {
            return <PrsnlPreferences params={state}/>;
        } else {
            if (buttonExcClicked) {
                return <PrsnlExcursions/>;
            } else {
                if (buttonSocMClicked) {
                    return <PrsnlSocialMedia params={state}/>;
                } else {
                    return <PrsnlTours/>;
                }
            }
        }
    };

    const handlePrefClick = () => {
        setState({
            buttonPrefClicked: true,
            buttonExcClicked: false,
            buttonSocMClicked: false,
            buttonToursClicked: false,
        });
    }

    const handleExcClick = () => {
        setState({
            buttonPrefClicked: false,
            buttonExcClicked: true,
            buttonSocMClicked: false,
            buttonToursClicked: false,
        });
    }

    const handleSocMClick = () => {
        setState({
            buttonPrefClicked: false,
            buttonExcClicked: false,
            buttonSocMClicked: true,
            buttonToursClicked: false,
        });
    }

    const handleToursClick = () => {
        setState({
            buttonPrefClicked: false,
            buttonExcClicked: false,
            buttonSocMClicked: false,
            buttonToursClicked: true,
        });
    }

    const prefClicked = state.buttonPrefClicked;
    const excClicked = state.buttonExcClicked;
    const socMClicked = state.buttonSocMClicked;
    const toursClicked = state.buttonToursClicked;

    let cntnt;
    cntnt = switchContent();
    return (
        <div className="prsnlContent">
            <div className="sideMenu">
                <button className="sideMenuBtn" onClick={handlePrefClick}>
                    Preferences
                </button>
                <button className="sideMenuBtn" onClick={handleSocMClick}>
                    Social Media
                </button>
                <button className="sideMenuBtn" onClick={handleToursClick}>
                    Ready Tours
                </button>
                <button className="sideMenuBtn" onClick={handleExcClick}>
                    Excursions
                </button>
            </div>
            <div className="prsnlContainer">{cntnt}</div>
        </div>
    );
}

export default PrsnlRoom;
