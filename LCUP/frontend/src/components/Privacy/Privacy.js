import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import "./privacy.scss"
import Typography from "@material-ui/core/Typography";
import {Divider} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const styles = makeStyles((theme) => ({
    paper: {
        // left: `${left}%`,
        width: 600,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        color: "black",
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    button: {
        width: 120,
        height: 40,
        color: "white",
        "font-family": "Pacifico",
        "border-color": "white",
        "border-style": "double",
        "border-radius": "25px",
        background: "#64B5F6",
        margin: "20px 0",
    },
    logInButton: {
        color: "white",
        "font-family": "Nunito",
        backgroundColor: "#64B5F6",
        "&:hover": {backgroundColor: "#64b5f6"},
    },
}));

const Privacy = () => {
    const classes = styles();
    return (
        <div className="log-in-form">
            <Container component="main">
                <div className={classes.paper}>
                    <Grid container spacing={2} direction="column">
                        <Grid item xs={12}>
                            <Typography component="h1" variant="h5" align="center">
                                Privacy
                            </Typography>
                        </Grid>
                        <Divider light/>
                        <Grid item xs={12}>
                            <div className="privacy-text">
                                <h5 className="heading">Политика использования данных</h5>
                                <span>  В настоящей политике описывается информация, которую мы обрабатываем для поддержки <span
                                    className="bold">Facebook</span> и <span className="bold">Instagram</span> и других продуктов
                                    и функций, предлагаемых компанией <span className="bold">Facebook</span>.</span>
                                <h5 className="heading"> Как управлять информацией обо мне или удалить ее?</h5>
                                <span>  Мы предоставляем вам возможности доступа, исправления, переноса и удаления ваших данных.
                                        Мы храним данные, пока не перестанем нуждаться в них для предоставления нашего сервиса Вам
                                        или пока Вы не удалите свой аккаунт — в зависимости от того, какое событие наступит раньше.
                                        Срок хранения определяется в индивидуальном порядке в зависимости от таких факторов, как
                                        характер данных, цель их сбора и обработки. При удалении аккаунта по вашему запросу мы
                                        удаляем всю информацию, что была сохранена о вас, а именно: локации с ваших постов, базовый
                                        набор данных для авторизации и открытую информацию, предоставленную вами лично.Вы можете в
                                        любое время удалить свой аккаунт, для этого вам будет необходимо связаться с нами по адресу
                                        электронной почты, указанному ниже.</span>
                                <td/>
                                <h5 className="heading">Как мы обрабатываем данные о вас?</h5>
                                <span>  Информация о вас автоматически обрабатывается функционалом нашего приложения, все данные
                                        строго защищены от постороннего вмешательства и могут быть удалены по вашему запросу в любой
                                        момент. </span>
                                <h5 className="heading">Как мы будем уведомлять вас об изменениях этой политики?</h5>
                                <span>  Мы заранее уведомим вас о внесении изменений в эту политику и дадим вам возможность изучить
                                        пересмотренную версию данного документа, прежде чем принимать решение о дальнейшем
                                        использовании нашего продукта. </span>
                                <h5 className="heading">Как задать вопрос Let’sChange your place.</h5>
                                <span>  Вы можете узнать больше о принципах обеспечения конфиденциальности на Let’s Change your place.
                                        Если у вас возникнут вопросы по поводу настоящей политики, вы можете связаться с нами, как
                                        описано ниже.
                                </span>
                                <h5 className="heading"> Типы данных, которые мы используем:</h5>
                                <p> - <span>Локации</span> с ваших постов. Данная информация поможет наиболее
                                    качественно определить ваши предпочтения по туристическим направлениям.</p>
                                <p> - <span>Базовый набор данных для авторизации </span> для интеграции вашего аккаунта
                                    в наш продукт.</p>
                                <p> - <span>Открытая информаци о себе </span>. Информация, которую вы оставляете в
                                    открытом доступе для других пользователей социальных сетей.</p>
                                <span className="bold">Вы можете связаться с нами по почте: </span>
                                <a>letschangeyourplace@gmail.com</a>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </Container>
        </div>
    );
}
export default Privacy