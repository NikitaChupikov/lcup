import React, {useState} from "react";
import ReadMoreAndLess from 'react-read-more-less';
import ReactModal from '@material-ui/core/Modal';
import Backdrop from "@material-ui/core/Backdrop";

import "./newsFeed.scss";


const NewsFeeds = () => {

    const [state, setState] = useState({
        news_feeds: undefined,
        showModal: false,
    })

    const getInfo = async () => {
        const url = process.env.REACT_APP_API_ENDPOINT + 'news';
        const news = await fetch(url);
        const news_info = await news.json();
        console.log(news_info);
        setState({
            news_feeds: news_info,
            showModal: true,
        });
    };

    const handleOpenModal = () => {
        setState({...state, showModal: true});
    }
    const handleCloseModal = () => {
        setState({...state, showModal: false});
    }

    return (
        <div>
            <a className="news__modal_button" onClick={getInfo}>NEWS</a>
            <ReactModal
                open={state.showModal}
                onClose={handleCloseModal}
                className="modalContainer"
                closeAfterTransition
                container={document.getElementById('modal')}
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
            <div className="modal">
                {state.news_feeds && state.news_feeds.map(post=>{
                    return (
                        <div className="news">
                            <h3>{post.ru_title}</h3>
                            <b className="news__date">{post.date}</b>
                            <br/>
                            <img className="news__image" src={post.image}/>
                            <br/>
                            <br/>
                            <ReadMoreAndLess
                                className="read-more-content"
                                charLimit={250}
                                readMoreText="Read more"
                                readLessText="Read less">
                                {post.ru_text}
                            </ReadMoreAndLess>
                            <hr></hr>
                            <br/>
                        </div>
                    );
                })}
            </div>
            </ReactModal>
        </div>
    );
}
export default NewsFeeds;