import React, {useState} from 'react';

import "./prsnlSocialMedia.scss";

import facebookIcon from "../../static/icons/facebook.png";
import githubIcon from "../../static/icons/github.png";
import instagramIcon from "../../static/icons/instagram.png";
import {setCity, setExcursions} from "../../actioncreators";
import {useStateValue} from "../../store";
import searchIcon from "../../static/icons/iconmonstr-magnifier-1-240.png";

const PrsnlSocialMedia = (props) => {
    const [state, setState] = useState(
        {
            instUsername: "",
            cities: []
        }
    )

    const handleChangeInstInput = (event) => {
        setState({...state, instUsername: event.target.value})
    }
    const handleSelectInstUsername = (username) => {
        fetch(`${process.env.REACT_APP_API_ENDPOINT}s_location?username=${state.instUsername}`)
            .then((excursions) => excursions.json())
            .then((res) => {
                if (res.length > 0) {
                    setState({...state, cities: res.map(item => item[0])})
                }
            })
            .catch((e) => console.log(e));
    }
    return (
        <div className="prfrnc media">
            <div className="headerWrap">
                <h4>Search tours by instagram account:</h4>
            </div>
            <img className="big_inst" src={instagramIcon} alt="instagramIcon"/>
            <div className="socMediaWrap">
                <div className="socMediaContainer">
                    <div className="socMediaItem">
                        <img src={instagramIcon} alt="instagramIcon"/>
                        <div className="socMCaption">
                            <label className="socName">Instagram username</label>
                            <div className="soc-search-inst-info-container">
                                <input placeholder="Username" onChange={handleChangeInstInput}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="socSearchInstInfo">
                    <div className="recommended-cities">
                        {state.cities.map((item) => {
                            return (
                                <span>{item}</span>
                            )
                        })}
                    </div>
                </div>

            </div>
            <div className="confirmSocM">
                <input type="checkbox" name="confirmCheck" value="y"/>
                <div>
                    <label>I agree that my social media accounts will be used to collect data needed to provide me with
                        personalized traveling advice </label>
                </div>
            </div>
            <button className="button-search"
                    onClick={handleSelectInstUsername}>
                Search
            </button>
        </div>
    );
}

export default PrsnlSocialMedia;