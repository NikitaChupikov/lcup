import React from 'react';
import "../RoutePoint/routePoint.scss";

const FlightInfo = (props) => {
    return (
        <div className="flightInfoContainer">

            <div className="departureInfo">
                {props.flight.depart_date}
            </div>
            <div className="departureInfo">
                <div>
                    {props.flight.gate}
                </div>
                <div>
                    {props.flight.number_of_changes} stop(s)
                </div>
            </div>
            <div className="departureInfo">
                {props.flight.return_date}
            </div>
            <div className="bookSection">
                <div className="price">
                    {props.flight.value} USD
                </div>
                <div className="flightBookButton">
                    <a className="flightBookBtn" href={props.flight.url} role="button"> Book it now!</a>
                </div>
            </div>
        </div>
    )
}

export default FlightInfo;