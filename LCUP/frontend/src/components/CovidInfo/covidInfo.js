import React, {useState} from "react";
import ReactModal from '@material-ui/core/Modal';
import "./covidInfo.scss";
import Backdrop from "@material-ui/core/Backdrop";

const CovidInfo = () => {
    const [state, setState] = useState({
        showModal: false,
        update_date: undefined,
        resources_url: undefined,
        countries: undefined,
        normal: undefined,
        restricted: undefined,
        prohibited: undefined,
        view_info: undefined
    })
    const getInfo = async () => {
        const url = process.env.REACT_APP_API_ENDPOINT + "media/entry_info.json"
        const entry = await fetch(url);
        const entry_info = await entry.json();
        setState({
            ...state,
            update_info: entry_info.update,
            resources_url: entry_info.resources_url,
            countries: entry_info.countries,
            normal: entry_info.normal,
            restricted: entry_info.restricted,
            prohibited: entry_info.prohibited,
            view_info: entry_info.countries,
            showModal: true,
        });
    };

    const usefulLinks = () => {
        return (
            <div>
                <br/>
                <a className="modal__country_name">Useful links: </a>
                <br/>
                <a> - </a>
                <a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019">
                    World health organization
                </a>
                <br/>

                <a> - </a>
                <a href="https://www.iatatravelcentre.com/international-travel-document-news/1580226297.htm">
                    Travel news powered by IATA Timatic
                </a>
                <br/>

                <a> - </a>
                <a href="https://www.cdc.gov/coronavirus/2019-ncov/travelers/index.html">
                    Centers for Disease Control (CDC)
                </a>
                <br/>

                <a> - </a>
                <a href="https://www.airfarewatchdog.com/blog/50104455/covid-19-flight-waivers-and-refund-policies-by-airline/">
                    COVID-19 Flight Waivers and Refund Policies by Airline
                </a>
            </div>
        );
    }
    const statusInfo = (status) => {
        if (status === "Partially restricted") {
            return <a className="modal__restricted_status_text">⚠️ {status}</a>;
        }
        if (status === "Entry prohibited") {
            return <a className="modal__close_status_text">❌ {status}</a>;
        }
        if (status === "Normal") {
            return <a className="modal__open_status_text">✅ {status}</a>;
        }
    }

    const handleOpenModal = () => {
        setState({...state, showModal: true});
    }
    const handleCloseModal = () => {
        setState({...state, showModal: false});
    }
    const setAllViewInfo = () => {
        setState({...state, view_info: state.countries});
    }
    const setNormalViewInfo = () => {
        setState({...state, view_info: state.normal});
    }
    const setRestrictedViewInfo = () => {
        setState({...state, view_info: state.restricted});
    }
    const setProhibitedViewInfo = () => {
        setState({...state, view_info: state.prohibited});
    }
    const filterBy = () => {
        const filter_name = document.getElementById('search-text');
        if (filter_name.value !== '') {
            const result = (state.countries || []).filter(country => country.name === filter_name.value)
            setState({...state, view_info: result});
        }
    }
    return (
        <div>
            <a className="modal__read_more_button" onClick={getInfo}>
                COVID-19
            </a>
            <ReactModal
                open={state.showModal}
                onClose={handleCloseModal}
                className="modalContainer"
                closeAfterTransition
                container={document.getElementById('modal')}
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <div className="modal">
                    <h2>COVID-19 Situation Monitoring</h2>
                    <a>{state.update_info}</a>
                    <br/>
                    <a>Data was provided by trip.com resource: </a>
                    <br/>
                    <a href={state.resources_url}>{state.resources_url}</a>
                    <br/>
                    {usefulLinks()}

                    <div>
                        <br/>
                        <div>
                            <input type="text" autocomplete="off" placeholder="Country name: " id="search-text"/>
                            <button className='modal__select_button' onClick={filterBy}> Search</button>
                        </div>
                        <div>
                            <br/>
                            <button className='modal__select_button' onClick={setAllViewInfo}> All</button>
                            <button className='modal__select_button' onClick={setNormalViewInfo}> Normal entry</button>
                            <button className='modal__select_button' onClick={setRestrictedViewInfo}> ️Partially
                                restricted
                            </button>
                            <button className='modal__select_button' onClick={setProhibitedViewInfo}> Entry prohibited
                            </button>
                        </div>
                    </div>
                    <div>
                        {state.countries &&
                        state.view_info.map((country) => {
                            return (
                                <div key={country.name}>
                                    <br/>
                                    <a className="modal__country_name">{country.name} </a>
                                    <a> {statusInfo(country.status)}</a>
                                    <br/>
                                    <a>{country.content}</a>
                                    <br/>
                                    <hr></hr>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </ReactModal>
        </div>
    );
}
export default CovidInfo;
