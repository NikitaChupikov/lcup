import React, {useEffect} from 'react';
import moment from 'moment';
import "./questions.scss";
import {useStateValue} from "../../store";
import {changeAnswer, openRouteList} from "../../actioncreators";

const duration_default = 4, first_day = 1;

const Questions = (props) => {

    const [store, dispatch] = useStateValue();
    const state = store.defaultMainState.questions
    useEffect(() => {
            if (store.defaultMainState.login.isLogin) {
                fetch(`${process.env.REACT_APP_API_ENDPOINT}current_user/`, {
                    headers: {
                        Authorization: `JWT ${localStorage.getItem('token')}`
                    }
                })
                    .then(response => response.json())
                    .then(response => {
                        dispatch(changeAnswer("questions", response.answers.questions))
                        dispatch(changeAnswer("depart_date", getDate(first_day)))
                        dispatch(changeAnswer("return_date", getDate(duration_default)))
                    });
            } else {
                fetch(process.env.REACT_APP_API_ENDPOINT + 'ml_params')
                    .then(response => response.json())
                    .then(response => {
                        dispatch(changeAnswer("questions", response.questions))
                        dispatch(changeAnswer("depart_date", getDate(first_day)))
                        dispatch(changeAnswer("return_date", getDate(duration_default)))
                    });
            }
        }
        , [store.defaultMainState.login.isLogin])

    const handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        dispatch(changeAnswer(name, value))
    }

    const getDate = (x) => {
        let date = new Date();
        date = moment(date).add(x, 'day').format('YYYY-MM-DD');
        return date;
    }

    const handleButtonClick = () => {
        dispatch(openRouteList())

    }

    const ansChange = () => {
        let loginButton;
        let ans = document.getElementById("tour_search").value;
        if (ans === "Yes") {
            dispatch(changeAnswer("mode", "close_q"))
            dispatch(changeAnswer("destination", "Prague"))
        }
        if (ans === "No") {
            dispatch(changeAnswer("mode", "show_q"))
            dispatch(changeAnswer("destination", ""))
        }
    }

    const destinationInput = () => {
        let input_destination;
        if (state.mode === "close_q") {
            input_destination = <label>
                куда отправляемся?
                <br/>
                <input name="destination"
                       value={state.destination}
                       onChange={handleInputChange}>
                </input>
                <br/>
            </label>;
            return input_destination;
        }
    }

    return (
        <div className="questionsContainer">
            <form className="questionBoard">
                <label>
                    откуда отправляемся?
                    <br/>
                    <input
                        name="origin"
                        value={state.origin}
                        onChange={handleInputChange}/>
                </label>
                <br/>

                <label>
                    знаете куда хотите поехать?
                    <br/>
                    <select id="tour_search" onChange={ansChange}>
                        <option selected="selected" value="No">
                            нет, давайте подберем тур
                        </option>
                        <option value="Yes">
                            да, конкретный город
                        </option>
                    </select>
                    <br/>
                </label>
                <label>{destinationInput()}</label>

                {state.questions ? state.questions.map(question => {
                    if (state.mode === 'show_q') {
                        return (
                            <label>{question.question}
                                <br/>
                                <select name={question.request_name} onChange={handleInputChange}>
                                    {question.answers.map(answer => {
                                        return (<option value={answer}>{answer}</option>)
                                    })}
                                </select>
                                <br/>
                            </label>
                        );
                    }
                }) : null}
                <div className="dateFormParent">
                    <label className="dateForm">
                        from:
                        <br/>
                        <input
                            name="depart_date"
                            type="date"
                            value={state.depart_date}
                            onChange={handleInputChange}/>
                    </label>
                    <label>
                        to:
                        <br/>
                        <input
                            name="return_date"
                            type="date"
                            value={state.return_date}
                            onChange={handleInputChange}/>
                    </label>
                </div>
                <button className="button-start" onClick={handleButtonClick}>
                    Let's Go
                </button>
            </form>
        </div>
    );
}
export default Questions;