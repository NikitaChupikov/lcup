import "./routeCollapse.scss";
import React, {useState} from "react";
import cx from "classnames";
import Collapse from "@kunukn/react-collapse";
import HotelInfo from "../HotelInfo/hotelInfo";
import FlightInfo from "../FlightInfo/flightInfo";
import tranboardHeaderIcon from "../../static/icons/baseline_flight_white_48dp.png";
import pointHeaderIcon from "../../static/icons/baseline_location_city_white_18dp.png";
import RoutePoint from '../RoutePoint/routePoint';
import '../Tranboard/tranboard.scss'
import '../../index.scss';

const App = (props) => {
    const [state, setState] = useState({
        isOpen1: false,
        isOpen2: false,
    })
    const toggle = index => {
        let collapse = "isOpen" + index;
        setState(prevState => ({[collapse]: !prevState[collapse]}));
    };

    const hotels = props.hotels;
    const flights = props.flights;
    return (
        <div className="">
            <RoutePoint direction='start' hotels={false}/>
            <button
                className={cx("app__toggle", {
                    "app__toggle--active": state.isOpen1
                })}
                onClick={() => toggle(1)}
            >
                <div className="routeHeaderIcon">
                    <img src={tranboardHeaderIcon} alt="routeHeaderIcon"/>
                </div>
                <span className="app__toggle-text">
          <div className="tranboardHeaderType">
              Flights
          </div>
          </span>
                <div className="rotate90">
                    <svg
                        className={cx("icon", {"icon--expanded": state.isOpen1})}
                        viewBox="6 0 12 24"
                    >
                        <polygon points="8 0 6 1.8 14.4 12 6 22.2 8 24 18 12"/>
                    </svg>
                </div>
            </button>
            <Collapse
                isOpen={state.isOpen1}
                className={
                    "app__collapse" +
                    (state.isOpen1 ? "app__collapse--active" : "")
                }
            >
                <div className="app__content">
                    <div className="tranBoardContent">
                        {flights.map(flight => {
                            return (
                                <FlightInfo flight={flight}/>
                            );
                        })}
                    </div>
                </div>
            </Collapse>

            <button
                className={cx("app__toggle", {
                    "app__toggle--active": state.isOpen2
                })}
                onClick={() => toggle(2)}
            >
                <div className="routeHeaderIcon">
                    <img src={pointHeaderIcon} alt="routeHeaderIcon"/>
                </div>
                <span className="app__toggle-text">
            <b>Hotels</b>
          </span>
                <div className="rotate90">
                    <svg
                        className={cx("icon", {"icon--expanded": state.isOpen2})}
                        viewBox="6 0 12 24"
                    >
                        <polygon points="8 0 6 1.8 14.4 12 6 22.2 8 24 18 12"/>
                    </svg>
                </div>
            </button>
            <Collapse
                isOpen={state.isOpen2}
                className={
                    "app__collapse" +
                    (state.isOpen2 ? "app__collapse--active" : "")
                }
            >
                <div className="app__content">
                    <div className="">
                        {hotels.map(hotel => {
                            return (
                                <div className="routeBoard">
                                    <HotelInfo info={hotel} depart_date={props.depart_date}
                                               return_date={props.return_date}/>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </Collapse>
        </div>
    )
}

export default App;
