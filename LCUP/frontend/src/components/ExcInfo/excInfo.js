import React from 'react';

import "./excInfo.scss";
import starIcon from "../../static/icons/iconmonstr-star-3-240.png";
import clockIcon from "../../static/icons/iconmonstr-time-2-240.png";


const ExcInfo = (props) => {

    return (
        <div className="excItem">
            <div className="excImgDiv">
                <img src={props.excursion.photos[0].medium} className="excImg"/>
                <div className="excDur">
                    <img src={clockIcon} className="excIcon"/>
                    <p>{props.excursion.duration} ч</p>
                </div>
            </div>
            <div className="guideRate">
                <div className="guideInfo">
                    <img src={props.excursion.guide.avatar.small}/>
                    <div className="guideName">
                        <a href={props.excursion.guide.url}>{props.excursion.guide.first_name}</a>
                    </div>
                </div>
                <div className="rateInfo">
                    {props.excursion.rating}
                    <img src={starIcon} className="excIcon"/>
                    <a href={props.excursion.links.reviews}>{props.excursion.review_count} отзывов</a>
                </div>
            </div>
            <h5><a href={props.excursion.url}>{props.excursion.title}</a></h5>
            <div className="smallBlackDiv"/>
            <p>{props.excursion.tagline}</p>
            <div className="priceBlock">
                <p className="price">{props.excursion.price.value} {props.excursion.price.currency}</p> <p
                className="smDetail">{props.excursion.price.unit_string}</p>
            </div>
        </div>
    )
}

export default ExcInfo;