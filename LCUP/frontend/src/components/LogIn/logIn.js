import React, {useState} from 'react'
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {Divider} from "@material-ui/core";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import GitHubIcon from "@material-ui/icons/GitHub";
import "./logIn.css";
import {closeLogInModal, errorLoginUser, loginUser} from "../../actioncreators";
import {useStateValue} from "../../store";

const styles = makeStyles((theme) => ({
    paper: {
        // left: `${left}%`,
        width: 600,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        color: "black",
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    button: {
        width: 120,
        height: 40,
        color: "white",
        "font-family": "Pacifico",
        "border-color": "white",
        "border-style": "double",
        "border-radius": "25px",
        background: "#64B5F6",
        margin: "20px 0",
    },
    logInButton: {
        color: "white",
        "font-family": "Nunito",
        backgroundColor: "#64B5F6",
        "&:hover": {backgroundColor: "#64b5f6"},
    },
}));

const LogIn = () => {
    const [state, dispatch] = useStateValue();

    const [userInfo, setUserInfo] = useState({
        username: "",
        email: "",
        password: "",
    })
    const [instUserInfo, setInstUserInfo] = useState({
        app_id: 0,
        redirect_uri: "",
        scope: "",
        response_type: ""
    })
    const classes = styles();

    const appInstaId = 227733491632025;

    const redirectURI = window.location.href ;
    // const redirectURI = `https://127.0.0.1:3000/`;

    const handleChange = (name) => (event) => {
        setUserInfo({
            ...userInfo,
            [name]: event.target.value
        });
    };

    const handleLogIn = (event) => {
        event.preventDefault();
        userInfo.username = userInfo.email;
        fetch(`${process.env.REACT_APP_API_ENDPOINT}token-auth/`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userInfo),
        })
            .then((response) => response.json())
            .then((json) => {
                if (json.token) {
                    localStorage.setItem("token", json.token);
                    const value = {
                        token: json.token,
                        username: json.user.username,
                        firstName: json.user.first_name,
                        lastName: json.user.last_name,
                        email: json.user.email,
                    };
                    dispatch(loginUser(value))
                    dispatch(closeLogInModal())
                } else {
                    dispatch(errorLoginUser(json.non_field_errors))
                }
            });
    };

    const handleGithubLogIn = (event) => {
        console.log("TODO: handle sign up request via Github")
    };

    const handleInstLogIn = (event) => {
        event.preventDefault();
        userInfo.username = userInfo.email;
        localStorage.setItem("authFlag", "login")
        const Url = `https://api.instagram.com/oauth/authorize?client_id=${appInstaId}&redirect_uri=${redirectURI}&scope=user_profile,user_media&response_type=code`
        window.open(Url);
    };

    const handleFbLogIn = (event) => {
        console.log("TODO: handle sign up request via Facebook")
    };
    return (
        <div className="log-in-form">
            <Container component="main">
                <div className={classes.paper}>
                    <Grid container spacing={2} direction="column">
                        <Grid item xs={12}>
                            <Typography component="h1" variant="h5" align="center">
                                Log In
                            </Typography>
                        </Grid>
                        <Divider light/>
                        <Grid item xs={12}>
                            <form className={classes.form} onSubmit={handleLogIn} noValidate>
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <TextField
                                            id="email"
                                            name="email"
                                            label="Email"
                                            onChange={handleChange("email")}
                                            autoComplete="email"
                                            variant="outlined"
                                            required
                                            fullWidth
                                        />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <TextField
                                            id="password"
                                            name="password"
                                            label="Password"
                                            type="password"
                                            onChange={handleChange("password")}
                                            autoComplete="current-password"
                                            variant="outlined"
                                            required
                                            fullWidth
                                        />
                                    </Grid>

                                    <Grid  item  spacing={2}>
                                        <span >
                                            {state.defaultMainState.login.details}
                                        </span>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Button
                                            type="submit"
                                            fullWidth
                                            variant="contained"
                                            className={classes.logInButton}
                                        >
                                            Log IN
                                        </Button>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Grid container spacing={2} justify="space-evenly">
                                            <Grid container alignItems="center" xs={4} spacing={1}>
                                                <Grid item>
                                                    <GitHubIcon/>
                                                </Grid>
                                                <Grid item>
                                                    <Link
                                                        href="#"
                                                        onClick={handleGithubLogIn}
                                                        variant="caption"
                                                    >
                                                        Sign up with Github
                                                    </Link>
                                                </Grid>
                                            </Grid>

                                            <Grid container alignItems="center" xs={4} spacing={1}>
                                                <Grid item>
                                                    <InstagramIcon/>
                                                </Grid>
                                                <Grid item>
                                                    <Link
                                                        href="#"
                                                        onClick={handleInstLogIn}
                                                        variant="caption"
                                                    >
                                                        Log In with Instagram
                                                    </Link>
                                                </Grid>
                                            </Grid>

                                            <Grid container alignItems="center" xs={4} spacing={1}>
                                                <Grid item>
                                                    <FacebookIcon/>
                                                </Grid>
                                                <Grid item>
                                                    <Link
                                                        href="#"
                                                        onClick={handleFbLogIn}
                                                        variant="caption"
                                                    >
                                                        Log In with Facebook
                                                    </Link>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </form>
                        </Grid>
                        <Divider light/>
                        <Grid item xs={12} justifyContent="center">
                            <Link href="#" variant="body2">
                                Don't have an account? Sign Up
                            </Link>
                        </Grid>
                    </Grid>
                </div>
            </Container>
        </div>
    );
}
export default LogIn
