import React from 'react';
import pointHeaderIcon from "../../static/icons/baseline_location_city_white_18dp.png";
import '../../index.scss';

const PointHeader = (props) => {
    return (
        <div class="routeHeaderPanel">

            <div class="routeHeaderIcon">
                <img src={pointHeaderIcon} alt="routeHeaderIcon"/>
            </div>
            <div class="pointHeaderName">
                {props.direction}
            </div>
            <div class="pointHeaderHideShow">
            </div>
        </div>
    )
};
export default PointHeader;