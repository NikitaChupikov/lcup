import React from 'react';
import RoutePointHeader from "../RoutePoint/routePointHeader";
import HotelInfo from "../HotelInfo/hotelInfo";

const RoutePoint = (props) => {
    const hotels = props.hotels;
    if (!hotels) {
        return (
            <div className="routePoint">
                <div className="routePointHeaderContainer">
                    <RoutePointHeader direction="Start"/>
                </div>
            </div>
        )
    } else {
        return (
            <div className="routePoint">
                <div className="routePointHeaderContainer">
                    <RoutePointHeader direction="End"/>
                </div>
                <div className="">
                    {hotels.map(hotel => {
                        return (
                            <div className="routeBoard">
                                <HotelInfo info={hotel} depart_date={props.depart_date}
                                           return_date={props.return_date}/>
                            </div>
                        );
                    })}
                </div>
            </div>
        )
    }
}

export default RoutePoint;