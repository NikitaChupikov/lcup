import React, {useEffect} from 'react';
import TourInfo from "../TourInfo/tourInfo";
import "./prsnlTours.scss";
import {setTours} from "../../actioncreators";
import {useStateValue} from "../../store";

const PrsnlTours = () => {
    const [store, dispatch] = useStateValue()
    const tours = store.defaultPrsnlState.tours;
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_ENDPOINT}tripster?city=${store.defaultPrsnlState.city}`, {})
            .then(response => response.json())
            .then(response => {
                dispatch(setTours(response.rubrics))
            });
    }, [])

    return (
        <div className="prfrnc">
            <div className="toursHeader">
                <h1>Ready-To-Go Tours</h1>
            </div>
            <div className="toursGenInfo">
                <label>Если вас не интересуют индивидуальный подбор путешествий, вы можете воспользоваться нашими
                    готовыми вариантами туров на любой вкус. Наша команда регулярно обновляет базу туров, поэтому вы
                    наверняка найдете здесь что-то новое. Что бы вас не интересовало — популярные туристические
                    направления или нестандартные идеи — на этой странице вы наверняка найдете то, что вам по вкусу.
                    Поехали!</label>
            </div>
            <div className="toursContainer">
                {tours.map(tour => {
                    return (
                        <TourInfo tour={tour}/>
                    );
                })}
            </div>
        </div>
    );
}

export default PrsnlTours;