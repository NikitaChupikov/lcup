import React from "react";
import {useStateValue} from "../../store";

const ExcursionFilter = () => {
    const [store, dispatch] = useStateValue();
    return (
        <div className="excursions-filter" key="exFil">
            {store.defaultPrsnlState.rubrics.length > 0
                ?
                store.defaultPrsnlState.rubrics
                : null
            }
        </div>
    )
}
export default ExcursionFilter;