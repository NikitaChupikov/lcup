import React, {Component, useEffect, useState} from 'react';
import {Pagination} from 'antd';
import ExcInfo from "../ExcInfo/excInfo";
import 'antd/dist/antd.css';
import searchIcon from "../../static/icons/iconmonstr-magnifier-1-240.png";
import "./prsnlExcursions.scss"
import ExcursionFilter from "./ExcursionFilter";
import {useStateValue} from "../../store";
import {setCity, setExcursions, setPaginationInfo, setRubrics} from "../../actioncreators";

let destinat = '';

const PrsnlExcursions = (props) => {
    const [store, dispatch] = useStateValue()
    const [state, setState] = useState({
        url: process.env.REACT_APP_API_ENDPOINT + "excursions?city=" + store.defaultPrsnlState.city,
        page: 1,
        pageSize: 10,
        cover: "../../static/images/exc_headers/prague_header.png"
    })


    const fetchServerData = () => {
        setState({
            ...state,
            url: process.env.REACT_APP_API_ENDPOINT + "excursions?city=" + store.defaultPrsnlState.city
        })

        fetch(`${process.env.REACT_APP_API_ENDPOINT}tripster?city=${store.defaultPrsnlState.city}&page_size=${state.pageSize}`)
            .then((excursions) => excursions.json())
            .then((res) => {
                dispatch(setExcursions(res.excursions))
                dispatch(setRubrics(renderAllRubrics(res.rubrics.filter((e) => e.experience_count > 0))))
                dispatch(setPaginationInfo('total', res.rubrics.filter((e) => e.name === "Все")[0].experience_count))
                setState({...state, cover: res.excursions[0].city.image.cover})
            })
            .catch((e) => console.log(e));
    };

    useEffect(() => {
        fetchServerData()
    }, [store.defaultPrsnlState.city])
    const handleRubricsClick = (elem) => {

        fetch(`${process.env.REACT_APP_API_ENDPOINT}excursions?city=${store.defaultPrsnlState.city}&tag=${elem.id}&page=${state.page}&page_size=${state.pageSize}`, {})
            .then(response => response.json())
            .then(response => {
                dispatch(setExcursions(response))
                dispatch(setPaginationInfo("total", elem.experience_count))
            });
        setState({
            ...state,
            url: `${process.env.REACT_APP_API_ENDPOINT}excursions?city=${store.defaultPrsnlState.city}&tag=${elem.id}`
        })
    }
    const renderAllRubrics = (rubrics) => {
        return (
            rubrics.map((elem) => {
                return (
                    <div className="rubric-item" onClick={() => handleRubricsClick(elem)} key={`tour ${elem.id}`}>
                        <span>{`${elem.name} (${elem.experience_count})`}</span>
                    </div>
                )
            })
        )
    }

    const handleChange = (event) => {
        setState({...state, inpValue: event.target.value});
        destinat = event.target.value;
    }

    const handlePaginationChange = (page, pageSize) => {
        fetch(`${state.url}&page=${page}&page_size=${pageSize}`, {})
            .then(response => response.json())
            .then(response => {
                dispatch(setExcursions(response))
            });
        setState({...state, page: page, pageSize: pageSize})
    }

    return (
        <div className="excursions">
            <div className="excHeader" style={{backgroundImage: `url(${state.cover})`}}>
                <p>Куда отправляемся?</p>
                <div>
                    <input className="searchField" name="destination"
                           onChange={handleChange}
                    >
                    </input>
                    <button type="submit" className="searchBtn"
                            onClick={() => {
                                dispatch(setCity(state.inpValue))
                            }}>
                        <img className="excIcon" src={searchIcon} alt="searchIcon"/>
                    </button>

                </div>
                <h2>Экскурсии в ... {props.inpValue} </h2>
                <h5>Необычные экскурсии на русском языке. Цены на экскурсии от 9€</h5>
            </div>
            <Pagination total={store.defaultPrsnlState.pagination.total} current={state.page} showSizeChanger
                        pageSize={state.pageSize} onChange={handlePaginationChange}/>
            <div className='excWrapper'>
                <div className="excContainer">
                    {store.defaultPrsnlState.excursions.map(excursion => {
                        return (
                            <ExcInfo excursion={excursion}/>
                        );
                    })}
                </div>
                <ExcursionFilter/>
            </div>
        </div>
    );
}

export default PrsnlExcursions;