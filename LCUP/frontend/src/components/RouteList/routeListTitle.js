import React from 'react';
import './routeBoard.scss'

/*
*   The RouteBoardTitle is a component for displaying
*   a city and country as result by backend response.
*/
const RouteBoardTitle = (props) => {
    return (
        <div className="routeBoardTitle">
            <div className="routeBoardTitle-City">
                {props.cityName}
            </div>
            <hr/>
            <div className="routeBoardTitle-Country">
                {props.countryName}
            </div>
        </div>

    );
}
export default RouteBoardTitle;