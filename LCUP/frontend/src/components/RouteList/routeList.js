import React, {useEffect, useState} from 'react';
import RouteBoard from "../RouteBoard/routeBoard";
import RouteBoardTitle from "../RouteBoard/routeBoardTitle";
import "../RouteBoard/routeBoard.scss";
import {useStateValue} from "../../store";
import {openQuestions} from "../../actioncreators";

const RouteList = () => {
    const [state, setState] = useState(
        {
            routes: [],
            isLoading: true,
            error: null
        }
    )
    const [store, dispatch] = useStateValue()
    useEffect(() => {
        let requestString = process.env.REACT_APP_API_ENDPOINT + '?'
        for (let [req_name, value] of Object.entries(store.defaultMainState.questions)) {
            if (req_name !== 'questions' & req_name !== 'buttonClicked') {
                requestString += `${req_name}=${value}&`;
            }
        }
        fetch(requestString + '&sort=True')
            .then(response => response.json())
            .then(routes => {
                if (!routes.status && routes.status !== "error") {
                    setState({
                        ...state,
                        routes: routes,
                        isLoading: false,
                    })
                } else {
                    dispatch(openQuestions())
                    alert(routes.message)
                }
            })


    }, [])

    const isLoading = state.isLoading, routes = state.routes, error = state.error;
    return (
        <React.Fragment>
            {error ? <p>{error.message}</p> : null}
            {!isLoading ? (
                routes.map(route => {
                    return (
                        <div className="routeBoard">
                            <RouteBoardTitle cityName={route.city_name} countryName={route.country_name}
                                             countryCovidStatus={route.status}/>
                            <RouteBoard hotels={route.hotels} flights={route.flights}
                                        depart_date={store.defaultMainState.questions.depart_date}
                                        return_date={store.defaultMainState.questions.return_date}/>
                        </div>
                    );
                })
            ) : (
                <h3>Loading...</h3>
            )}
        </React.Fragment>)
}

export default RouteList;