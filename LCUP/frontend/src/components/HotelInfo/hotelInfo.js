import React from 'react';
import "./hotelInfo.scss";
import "../RoutePoint/routePoint.scss";
import hotelInfoImage from "../../static/icons/baseline_location_city_black_48dp.png";

const HotelInfo = (props) => {
    return (
        <div className="hotelInfoContainer">
            <img src={hotelInfoImage} alt="hotelInfoImage"/>
            <div className="hotelInfoContent">
                <div className="hotelInfo hotelName">
                    {props.info.hotelName}
                </div>
                <div className="hotelInfo">
                    {props.info.stars} stars
                </div>
                <div className="hotelInfo priceInfo">
                    Price {props.info.priceAvg}
                </div>
                <div className="hotelInfo hotelBookButton">
                    <a class="hotelBookBtn"
                       href={props.info.url + '&checkIn=' + props.depart_date + '&checkOut=' + props.return_date}
                       role="button"> Book it now!</a>
                </div>
            </div>
        </div>

    )
}

export default HotelInfo;