import React from "react";
import "./userPage.scss"
import {useStateValue} from "../../store";
import {openUserPage} from "../../actioncreators";

const UserCard = ()=>{
    const [store, dispatch] = useStateValue()
    const username = store.defaultMainState.login.username
    const handleOpenUserPage = () =>{
        dispatch(openUserPage())
    }
    return(
    <div className="user-card">
        <span>{`Welcome ${username}` }</span>
        <picture className="user-icon" onClick={handleOpenUserPage}/>
    </div>
    )
}
export default UserCard