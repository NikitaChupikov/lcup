import React, {useEffect, useState} from 'react';
import moment from 'moment';

import "./prsnlPreferences.scss";
import {changeAnswer, errorLoginUser, loginUser, logOut} from "../../actioncreators";
import {useStateValue} from "../../store";

const duration_default = 4, first_day = 1;

const PrsnlPreferences = () => {
    const [store, dispatch] = useStateValue();
    const [state, setState] = useState({
        buttonClicked: false,
        questions: [],
        origin: 'Minsk',
        depart_date: '2020-03-20',
        return_date: '2020-03-23',
        mode: 'show_q',
        destination: '',
        answers: {},
        details: ''
    })

    const handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        setState({
            ...state,
            answers: {
                ...state.answers,
                [name]: value
            }
        });
    }

    const getDate = (x) => {
        let date = new Date();
        date = moment(date).add(x, 'day').format('YYYY-MM-DD');
        return date;
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_ENDPOINT}current_user/`, {
            headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
            }
        })
            .then(response => response.json())
            .then(response => {
                if (response.user && response.user.username) {
                    setState({...state, questions: response.answers.questions})
                    dispatch(changeAnswer("questions", response.answers.questions))
                } else if (response.detail) {
                    const value = response.detail
                    dispatch(errorLoginUser(value))
                }
            });
        setState({...state, depart_date: getDate(first_day), return_date: getDate(duration_default)});
    }, [])

    const handleButtonClick = () => {
        let url = `${process.env.REACT_APP_API_ENDPOINT}save_user_ans?`
        Object.entries(state.answers).forEach(([prop, value]) => {
                url = url + `${prop}=${value}&`
            }
        )
        fetch(url, {
            method: 'GET',
            headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
            }
        })
            .then(response => response.json())
            .then(response => {
                setState({...state, details: response.message})

            });
    }

    return (
        <div className="prfrnc">
            <h4>Расскажите немного о своих предпочтениях:</h4>
            <div className="prefForm">
                {state.questions ? state.questions.map(question => {
                    if (state.mode === 'show_q') {
                        return (
                            <label>{question.question}
                                <br/>
                                <select name={question.request_name} onChange={handleInputChange}>
                                    {question.answers.map(answer => {
                                        return (<option value={answer}>{answer}</option>)
                                    })}
                                </select>
                                <br/>
                            </label>
                        );
                    }
                }) : null}
                <button key='pp-key' className="button-start" onClick={handleButtonClick}>
                    Save
                </button>
                <span>
                    {state.details}
                </span>
            </div>
            <div className="filler">

            </div>
        </div>
    );
}

export default PrsnlPreferences;