import React from 'react';

import "./tourInfo.scss";

const TourInfo = (props) => {
    return (
        <div className="tourItem">
            <h3><a href={props.tour.url}>{props.tour.name}</a></h3>
            <div className="smallBlackDiv"></div>
            <h5>{props.tour.header}</h5>
        </div>
    )
}

export default TourInfo;