import React, {useEffect, useState} from "react";
import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import Spinner from "react-mdl/lib/Spinner";
import "./loading.scss";

const Loading = () => {
    const code = new URLSearchParams(window.location.search).get("code");
    const [message, setMessage] = useState();
    useEffect(() => {
        if (localStorage.getItem('authFlag') === "signup") {
            console.log("signup")
            fetch(`${process.env.REACT_APP_API_ENDPOINT}users/`, {
                method: "POST",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                },
                body:
                    JSON.stringify({
                        code: code,
                        social_flag: 'insta'
                    })

            })
                .then((response) => response.json())
                .then((json) => {
                    if (json.token) {
                        localStorage.setItem("token", json.token)
                        window.close()
                    } else if (json.message) {
                        setMessage(json.message);
                    }
                })
        } else if (localStorage.getItem('authFlag') === "login") {
            console.log("login")
            fetch(`${process.env.REACT_APP_API_ENDPOINT}token-auth/`, {
                method: "POST",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                },
                body:
                    JSON.stringify({
                        code: code,
                        social_flag: 'insta'
                    })

            })
                .then((response) => response.json())
                .then((json) => {
                    if (json.token) {
                        localStorage.setItem("token", json.token)
                        window.close()
                    } else if (json.message) {
                        setMessage(json.message);
                    }
                })
        }
    }, [])
    return (
        <div className="loading-container">
            <div>
                <span>loading... </span>
                <Spinner/>
            </div>
            <a>{message}</a>
        </div>
    )
}
export default Loading