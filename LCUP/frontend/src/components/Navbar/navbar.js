import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ModalSignUp from './modalSignUp'
import ModalPrivacy from './ModalPrivacy'
import './navbar.scss'
import ModalLogIn from "./modalLogIn";
import {useStateValue} from "../../store";
import UserCard from "../UserPage/UserCard";
import {
    logOut,
    openQuestions,
    openLogInModal,
    closeLogInModal,
    openSignUpModal,
    closeSignUpModal,
    closePrivacyModal,
    openPrivacyModal
} from "../../actioncreators";
import CovidInfo from "../CovidInfo/covidInfo";

const NavigationBar = () => {
    const [store, dispatch] = useStateValue()
    const isLogin = store.defaultMainState.login.isLogin

    useEffect(() => {
        if (store.defaultMainState.login.details !== "") {
            dispatch(openLogInModal())
        }
    }, [store.defaultMainState.login.details])

    const handleLogInClick = () => {
        if (store.defaultMainState.modalPages.isOpenLogin) {
            dispatch(closeLogInModal())
        } else {
            dispatch(openLogInModal())
        }
    }

    const handleSignUpClick = () => {
        if (store.defaultMainState.modalPages.isOpenSignUp) {
            dispatch(closeSignUpModal())
        } else {
            dispatch(openSignUpModal())
        }
    }
    const handlePrivacyClick = () => {
        if (store.defaultMainState.modalPages.isOpenPrivacy) {
            dispatch(closePrivacyModal())
        } else {
            dispatch(openPrivacyModal())
        }
    }

    const handleLogOutClick = () => {
        localStorage.removeItem("token")
        dispatch(logOut())
    }
    const handleTitleClick = () => {
        dispatch(openQuestions())
    }

    return (
        <div className="header">
            <div className="modalContainerWrapper">
                {store.defaultMainState.modalPages.isOpenSignUp ?
                    ReactDOM.createPortal(
                        <ModalSignUp
                            open={store.defaultMainState.modalPages.isOpenSignUp}
                            handleClose={handleSignUpClick}
                        />,
                        document.getElementById('modal')
                    )
                    :
                    null
                }
            </div>
            <div className="modalContainerWrapper">
                {store.defaultMainState.modalPages.isOpenLogin ?
                    ReactDOM.createPortal(
                        <ModalLogIn
                            open={store.defaultMainState.modalPages.isOpenLogin}
                            handleClose={handleLogInClick}
                        />,
                        document.getElementById('modal')
                    )
                    :
                    null
                }
            </div>
            <div className="modalContainerWrapper">
                {store.defaultMainState.modalPages.isOpenPrivacy ?
                    ReactDOM.createPortal(
                        <ModalPrivacy
                            open={store.defaultMainState.modalPages.isOpenPrivacy}
                            handleClose={handlePrivacyClick}
                        />,
                        document.getElementById('modal')
                    )
                    :
                    null
                }
            </div>
            <Grid
                container
                spacing={0}
                direction="row"
                justify="space-between"
                alignItems="center"
            >
                <Grid item xs>
                    <Button className={"navSignupButton"}
                            onClick={handlePrivacyClick}
                    >
                        Privacy
                    </Button>
                </Grid>

                <Grid item xs={6} className={"navLogo"}>
                    <span onClick={handleTitleClick}>
                    Let's Change Your Place
                    </span>
                    <CovidInfo/>
                </Grid>
                {isLogin
                    ?
                    <Grid item xs className={"navButton login"}>
                        <UserCard/>
                        <Button className={"navLoginButton"}
                                onClick={handleLogOutClick}
                        >
                            Log Out
                        </Button>
                    </Grid>
                    :
                    <Grid item xs className={"navButton"}>
                        <Button className={"navLoginButton"}
                                onClick={handleLogInClick}
                        >
                            Log In
                        </Button>

                        <Button className={"navSignupButton"}
                                onClick={handleSignUpClick}
                        >
                            Sign Up
                        </Button>
                    </Grid>
                }
            </Grid>
        </div>
    );
}
export default NavigationBar;