import React from 'react';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import './navbar.scss'
import LogIn from "../LogIn";

const ModalLogIn = ({
                        open,
                        handleClose
                    }) => {
    return (
        <Modal
            open={open}
            onClose={handleClose}
            className="modalContainer"
            closeAfterTransition
            container={document.getElementById('modal')}
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <LogIn/>
        </Modal>

    )
}

export default ModalLogIn;