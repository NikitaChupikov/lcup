import React from "react";
import Backdrop from "@material-ui/core/Backdrop";
import Modal from "@material-ui/core/Modal";
import Privacy from "../Privacy/Privacy";

const ModalPrivacy = ({open, handleClose}) => {
    return (
        <Modal
            open={open}
            onClose={handleClose}
            className="modalContainer"
            closeAfterTransition
            container={document.getElementById('modal')}
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Privacy/>
        </Modal>
    )
}
export default ModalPrivacy