import React from 'react';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import SignUp from '../Signup/signup'
import './navbar.scss'

const ModalSignUp = ({
    open,
    handleClose,
    ...props
  }) => {
    return (
        <Modal
          open={open}
          onClose={handleClose}
          className="modalContainer"
          closeAfterTransition
          container={document.getElementById('modal')}
          BackdropComponent={Backdrop}
          BackdropProps={{
              timeout: 500,
          }}
        >   
            <SignUp />  
        </Modal>
    )
  }

  export default ModalSignUp;