import React from 'react';
import "./routeBoard.scss"
import RouteCollapse from "../RouteCollapse/routeCollapse";

const RouteBoard = (props) => {
    return (
        <div className="routeBoardListContainer">
            <RouteCollapse flights={props.flights} direction='end'
                           hotels={props.hotels} depart_date={props.depart_date}
                           return_date={props.return_date}/>
        </div>
    );
}
export default RouteBoard;