import React from 'react';
import FlightInfo from '../FlightInfo/flightInfo';
import TranBoardHeader from "./TranBoardHeader";

const TranBoardEntry = ()=>{
        return (
            <div className="tranboardEntry"> 
                  <div className="routePointHeaderContainer">
                        <TranBoardHeader direction="end"/>
                    </div>  
                    <div className="">
                                <div className="routeBoard">
                                    <TranBoardEntry info={FlightInfo}/>
                                </div>
                    </div> 
            </div>
        );
}
export default TranBoardEntry;