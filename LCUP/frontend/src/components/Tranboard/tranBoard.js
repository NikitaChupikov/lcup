import React from 'react';
import TranBoardHeader from "./TranBoardHeader";
import FlightInfo from "../FlightInfo/flightInfo";
import './tranboard.scss'

const TranBoard = (props) => {
    const flights = props.flights;
    console.log(flights);
    return (
        <div className="tranboard">
            <TranBoardHeader/>
            <div className="tranBoardContent">
                {flights.map(flight => {
                    return (
                        <FlightInfo flight={flight}/>
                    );
                })}
                { /*  <TranBoardFind /> List of transport entryes below */}
            </div>
        </div>
    );
}
export default TranBoard;