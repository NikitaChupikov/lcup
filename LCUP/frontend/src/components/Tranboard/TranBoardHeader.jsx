import React from 'react';
import tranboardHeaderIcon from "../../static/icons/baseline_flight_white_48dp.png";
import tranboardHeaderExpandBtn from "../../static/icons/baseline_expand_more_white_18dp.png";
import '../../index.scss'

const TranBoardHeader = () => {
    return (
        <div className="routeHeaderPanel">
            <div className="routeHeaderIcon">
                <img src={tranboardHeaderIcon} alt="routeHeaderIcon"/>
            </div>
            <div className="tranboardHeaderType">
                Flights
            </div>
            <div className="tranboardHeaderExpandBtn">
                <img src={tranboardHeaderExpandBtn} alt="tranboardHeaderExpandBtn"/>
            </div>

        </div>
    );
}
export default TranBoardHeader;