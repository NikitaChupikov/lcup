import React, {useState} from 'react';
import './tranboardFind.scss'

const TranBoardFind = (props) => {
    const [state, setState] = useState(
        {
            fromIATA: '',
            toIATA: '',
            dateFrom: '08.01.2020',
            dateTo: '09.01.2020'
        }
    )

    const handleInputChange = (event) => {

    }

    const handleButtonClick = () => {

    }

    return (
        <form class="tranboardFindForm">
            <label class="tranboardFindFormInputLabel">
                From:
                <br/>
                <input
                    name="fromIATA"
                    className='tranboardFindInput'
                    value={state.fromIATA}
                    onChange={handleInputChange}
                    placeholder='From'
                />
            </label>

            <label class="tranboardFindFormInputLabel">
                To:
                <br/>
                <input
                    name="toIATA"
                    className='tranboardFindInput'
                    value={state.toIATA}
                    onChange={handleInputChange}
                    placeholder='To'
                />
            </label>

            <label class="tranboardFindFormInputLabel">
                departure:
                <br/>
                <input
                    name="dateFrom"
                    className='tranboardFindInput'
                    type="date"
                    value={state.dateFrom}
                    onChange={handleInputChange}
                />
            </label>

            <label class="tranboardFindFormInputLabel">
                return:
                <br/>
                <input
                    name="dateTo"
                    F className='tranboardFindInput'
                    type="date"
                    value={state.dateTo}
                    onChange={handleInputChange}
                />
            </label>
            <br/>
            <button
                class="findTransportBtn"
                onClick={handleButtonClick}
            >
                Find
            </button>
        </form>
    );
}

export default TranBoardFind;


