import React from 'react';
import ReactDOM from 'react-dom';
import './static/fonts/Pacifico-Regular.ttf';
import './static/fonts/Oswald-Regular.ttf';
import {StateProvider} from './store'
import App from './App';
import * as serviceWorker from './serviceWorker';
import {defaultMainState} from "./reducer/main";
import {MainReducer} from "./reducer";
import Loading from "./components/Loading/Loading";
import {defaultPrsnlState} from "./reducer/prsnl";
import './index.scss';

const code = new URLSearchParams(window.location.search).has("code")
ReactDOM.render(
    <StateProvider initialState={{
        defaultMainState,
        defaultPrsnlState
    }} reducer={MainReducer}>
        {
            code
                ? <Loading/>
                : <App/>
        }

    </StateProvider>,
    document.getElementById('root'));

serviceWorker.unregister();
