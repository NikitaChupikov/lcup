import {
    SET_EXCURSIONS,
    SET_RUBRICS,
    SET_CITY, SET_TOURS, SET_PAGINATION_INFO
} from "../constants/actions";

export const defaultPrsnlState = {
    pagination: {
        total: 0,
        pageSize: 10,
        page: 1
    },
    excursions: [],
    rubrics: [],
    tours: [],
    city: 'Paris'
}

export const PrsnlState = (prsnlState = defaultPrsnlState, action) => {
    const {type, payload} = action

    switch (type) {
        case  SET_EXCURSIONS :
            return {
                ...prsnlState,
                excursions: [...payload.value]
            }
        case  SET_RUBRICS :
            return {
                ...prsnlState,
                rubrics: [...payload.rubrics]
            }
        case  SET_TOURS :
            return {
                ...prsnlState,
                tours: [...payload.tours]
            }
        case  SET_CITY :
            return {
                ...prsnlState,
                city: payload.value
            }
        case  SET_PAGINATION_INFO :
            return {
                ...prsnlState,
                pagination: {
                    ...prsnlState.pagination,
                    [payload.key]: payload.value
                }
            }
        default:
            return {...prsnlState}
    }
}