import {
    CHANGE_ANSWER, CLOSE_LOG_IN_MODAL, CLOSE_PRIVACY_MODAL, CLOSE_SIGN_UP_MODAL,
    ERROR_LOG_IN_INST_USER,
    ERROR_LOG_IN_USER,
    LOG_IN_INST_USER,
    LOG_IN_USER,
    LOG_IN_WITH_INST_TOKEN,
    LOG_IN_WITH_TOKEN,
    LOG_OUT_USER,
    LOG_UP_END, OPEN_LOG_IN_MODAL, OPEN_PRIVACY_MODAL,
    OPEN_QUESTIONS,
    OPEN_ROTE_LIST, OPEN_SIGN_UP_MODAL,
    OPEN_USER_PAGE,
} from "../constants/actions";
import moment from "moment";

export const defaultMainState = {
    login: {
        haveToken: !!localStorage.getItem('token') && localStorage.getItem('token') !== "" && localStorage.getItem('token') !== "undefined",
        isLogin: false,
        username: '',
        firstName: '',
        lastName: '',
        email: '',
        details: '',
        token: '',
    },
    modalPages: {
        isOpenSignUp: false,
        isOpenLogin: false,
        isOpenPrivacy: false
    },
    questions: {
        questions: null,
        origin: 'Minsk',
        depart_date: moment(new Date()).add(1, 'day').format('YYYY-MM-DD'),
        return_date: moment(new Date()).add(4, 'day').format('YYYY-MM-DD'),
        mode: 'show_q',
        destination: ''
    },
    openedPage: {
        isOpenQuestions: true,
        isOpenedRouteList: false,
        isOpenUserPage: false
    }
}

export const MainState = (mainState = defaultMainState, action) => {
    const {type, payload} = action

    switch (type) {

        case  LOG_UP_END :
            return {
                ...mainState,
                login: {
                    ...mainState.login,
                    token: payload.token,
                    isLogin: true,
                    username: payload.username,
                    firstName: payload.firstName,
                    lastName: payload.lastName,
                    email: payload.email,
                }

            }
        case   LOG_IN_USER:
            return {
                ...mainState,
                login: {
                    ...mainState.login,
                    token: payload.token,
                    isLogin: true,
                    username: payload.username,
                    firstName: payload.firstName,
                    lastName: payload.lastName,
                    email: payload.email,
                    details: '',
                }
            }
        case   LOG_IN_INST_USER:
            return {
                ...mainState,
                login: {
                    ...mainState.login,
                    // token: payload.token,
                    isLogin: true,
                    username: payload.username,
                    // firstName: payload.firstName,
                    // lastName: payload.lastName,
                    // email: payload.email,
                    // details: '',
                }
            }
        case   LOG_OUT_USER:
            return {
                ...mainState,
                login: {
                    ...mainState.login,
                    haveToken: false,
                    isLogin: false,
                    username: '',
                    firstName: '',
                    lastName: '',
                    email: '',
                    details: mainState.login.details,
                    token: '',
                },
                openedPage: {
                    isOpenQuestions: true,
                    isOpenedRouteList: false,
                    isOpenUserPage: false
                }
            }
        case   LOG_IN_WITH_TOKEN:
            return {
                ...mainState,
                login: {
                    ...mainState.login,
                    haveToken: true,
                }

            }
        case   LOG_IN_WITH_INST_TOKEN:
            return {
                ...mainState,
                login: {
                    ...mainState.login,
                }

            }
        case   ERROR_LOG_IN_USER:
            return {
                ...mainState,

                login: {
                    ...mainState.login,
                    details: payload,
                    token: '',
                    isLogin: false,
                    username: '',
                    firstName: '',
                    lastName: '',
                    email: '',
                },
                openedPage: {
                    isOpenQuestions: true,
                    isOpenedRouteList: false,
                    isOpenUserPage: false
                }
            }
        case   ERROR_LOG_IN_INST_USER:
            return {
                ...mainState,
                login: {
                    ...mainState.login,
                    details: payload,
                    token: '',
                    isLogin: false,
                    username: '',
                    firstName: '',
                    lastName: '',
                    email: '',
                }
            }
        case   CHANGE_ANSWER:
            return {
                ...mainState,
                questions: {
                    ...mainState.questions,
                    [payload.name]: payload.value
                }
            }
        case   OPEN_ROTE_LIST:
            return {
                ...mainState,
                openedPage: {
                    ...mainState.openedPage,
                    isOpenQuestions: false,
                    isOpenedRouteList: true,
                    isOpenUserPage: false
                }
            }
        case   OPEN_QUESTIONS:
            return {
                ...mainState,
                openedPage: {
                    ...mainState.openedPage,
                    isOpenQuestions: true,
                    isOpenedRouteList: false,
                    isOpenUserPage: false
                }
            }
        case   OPEN_USER_PAGE:
            return {
                ...mainState,
                openedPage: {
                    ...mainState.openedPage,
                    isOpenQuestions: false,
                    isOpenedRouteList: false,
                    isOpenUserPage: true
                }
            }

        case   OPEN_LOG_IN_MODAL:
            return {
                ...mainState,
                modalPages: {
                    ...mainState.modalPages,
                    isOpenLogin: true
                }
            }
        case   CLOSE_LOG_IN_MODAL:
            return {
                ...mainState,
                modalPages: {
                    ...mainState.modalPages,
                    isOpenLogin: false
                }
            }
        case   OPEN_SIGN_UP_MODAL:
            return {
                ...mainState,
                modalPages: {
                    ...mainState.modalPages,
                    isOpenSignUp: true
                }
            }
        case   CLOSE_SIGN_UP_MODAL:
            return {
                ...mainState,
                modalPages: {
                    ...mainState.modalPages,
                    isOpenSignUp: false
                }
            }
        case   OPEN_PRIVACY_MODAL:
            return {
                ...mainState,
                modalPages: {
                    ...mainState.modalPages,
                    isOpenPrivacy: true
                }
            }
        case   CLOSE_PRIVACY_MODAL:
            return {
                ...mainState,
                modalPages: {
                    ...mainState.modalPages,
                    isOpenPrivacy: false
                }
            }
        default:
            return {...mainState}
    }
}