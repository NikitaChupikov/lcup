import {defaultMainState, MainState} from "./main"
import {MAIN_STATE, PRSNL_STATE} from "../constants/prefixes";
import {defaultPrsnlState, PrsnlState} from "./prsnl";

export const MainReducer = (main = {defaultMainState, defaultPrsnlState}, action) => {
    switch (action.prefix) {
        case MAIN_STATE:
            return {
                ...main,
                defaultMainState: MainState(main.defaultMainState, action)
            };
        case PRSNL_STATE:
            return {
                ...main,
                defaultPrsnlState: PrsnlState(main.defaultPrsnlState, action)
            };

        default:
            return {...main};
    }
};