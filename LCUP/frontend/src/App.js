import React, {useEffect, useState} from 'react';
import Home from './pages/home';
import NavigationBar from "./components/Navbar/navbar";
import './App.scss';
import "./index.scss";
import {useStateValue} from "./store";
import {
    errorLoginInstUser,
    errorLoginUser,
    loginInstUser,
    loginUser,
    loginWithInstToken,
    loginWithToken,
    logOut
} from "./actioncreators";
import PrsnlRoom from "./pages/personalRoom";

function App() {
    const [store, dispatch] = useStateValue()

    const [state, setState] = useState(
        {
            loggedIn: !!localStorage.getItem('token') && localStorage.getItem('token') !== ""
        }
    )

    const storageEvent = (e) => {
        if (e.key === "token") {
            if (e.newValue === "") {
                dispatch(logOut())
            } else if (e.newValue !== "") {
                dispatch(loginWithToken())
            }
        }
        console.log(e)
    };

    useEffect(() => {
        window.addEventListener('storage', storageEvent);
    }, [])
    useEffect(() => {
            if (store.defaultMainState.login.haveToken) {
                fetch(`${process.env.REACT_APP_API_ENDPOINT}current_user/`, {
                    headers: {
                        Authorization: `JWT ${localStorage.getItem('token')}`
                    }
                })
                    .then(res => res.json())
                    .then(json => {
                        if (json.user && json.user.username) {
                            const value = {
                                username: json.user.username,
                            };
                            dispatch(loginUser(value))
                        } else if (json.detail) {
                            const value = json.detail
                            dispatch(errorLoginUser(value))
                        }
                    });
            }
        }, [store.defaultMainState.login.haveToken]
    )
    return (
        <div className="appWrap">
            <NavigationBar/>
            {
                store.defaultMainState.openedPage.isOpenUserPage
                    ? <PrsnlRoom/>
                    : <Home/>
            }
        </div>
    );
}

export default App;
