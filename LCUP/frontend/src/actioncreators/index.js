import {
    CHANGE_ANSWER, CLOSE_LOG_IN_MODAL, CLOSE_SIGN_UP_MODAL,
    ERROR_LOG_IN_INST_USER,
    ERROR_LOG_IN_USER,
    LOG_IN_INST_USER,
    LOG_IN_USER,
    LOG_IN_WITH_INST_TOKEN,
    LOG_IN_WITH_TOKEN,
    LOG_OUT_USER,
    LOG_UP_END, OPEN_LOG_IN_MODAL,
    OPEN_QUESTIONS,
    OPEN_ROTE_LIST, OPEN_SIGN_UP_MODAL,
    OPEN_USER_PAGE, SET_CITY,
    SET_EXCURSIONS, SET_PAGINATION_INFO, SET_RUBRICS,
    SET_TOURS,
    CLOSE_PRIVACY_MODAL,
    OPEN_PRIVACY_MODAL
} from "../constants/actions";
import {MAIN_STATE, PRSNL_STATE} from "../constants/prefixes";

export function registrationUserAction(value) {
    return {
        prefix: MAIN_STATE,
        type: LOG_UP_END,
        payload: {
            ...value
        }
    }
}

export function loginUser(value) {
    return {
        prefix: MAIN_STATE,
        type: LOG_IN_USER,
        payload: {
            ...value
        }
    }
}

export function loginInstUser(value) {
    return {
        prefix: MAIN_STATE,
        type: LOG_IN_INST_USER,
        payload: {
            ...value
        }
    }
}

export function logOut() {
    return {
        prefix: MAIN_STATE,
        type: LOG_OUT_USER,
    }
}

export function errorLoginUser(value) {
    return {
        prefix: MAIN_STATE,
        type: ERROR_LOG_IN_USER,
        payload: value
    }
}

export function errorLoginInstUser(value) {
    return {
        prefix: MAIN_STATE,
        type: ERROR_LOG_IN_INST_USER,
        payload: value
    }
}

export function loginWithToken() {
    return {
        prefix: MAIN_STATE,
        type: LOG_IN_WITH_TOKEN,
    }
}

export function loginWithInstToken() {
    return {
        prefix: MAIN_STATE,
        type: LOG_IN_WITH_INST_TOKEN,
    }
}

export function changeAnswer(name, value) {
    return {
        prefix: MAIN_STATE,
        type: CHANGE_ANSWER,
        payload: {
            name: name,
            value: value
        }
    }
}

export function openRouteList() {
    return {
        prefix: MAIN_STATE,
        type: OPEN_ROTE_LIST,
    }
}

export function openQuestions() {
    return {
        prefix: MAIN_STATE,
        type: OPEN_QUESTIONS,
    }
}

export function openUserPage() {
    return {
        prefix: MAIN_STATE,
        type: OPEN_USER_PAGE,
    }
}

export function setExcursions(value) {
    return {
        prefix: PRSNL_STATE,
        type: SET_EXCURSIONS,
        payload: {
            value: value
        }
    }
}

export function setRubrics(rubrics) {
    return {
        prefix: PRSNL_STATE,
        type: SET_RUBRICS,
        payload: {
            rubrics: rubrics
        }
    }
}

export function setTours(tours) {
    return {
        prefix: PRSNL_STATE,
        type: SET_TOURS,
        payload: {
            tours: tours
        }
    }
}

export function setCity(value) {
    return {
        prefix: PRSNL_STATE,
        type: SET_CITY,
        payload: {
            value: value
        }
    }
}

export function setPaginationInfo(key, value) {
    return {
        prefix: PRSNL_STATE,
        type: SET_PAGINATION_INFO,
        payload: {
            key: key,
            value: value
        }
    }
}

export function openLogInModal() {
    return {
        prefix: MAIN_STATE,
        type: OPEN_LOG_IN_MODAL
    }
}

export function closeLogInModal() {
    return {
        prefix: MAIN_STATE,
        type: CLOSE_LOG_IN_MODAL
    }
}
export function closePrivacyModal() {
    return {
        prefix: MAIN_STATE,
        type: CLOSE_PRIVACY_MODAL
    }
}
export function openPrivacyModal() {
    return {
        prefix: MAIN_STATE,
        type: OPEN_PRIVACY_MODAL
    }
}

export function openSignUpModal() {
    return {
        prefix: MAIN_STATE,
        type: OPEN_SIGN_UP_MODAL
    }
}

export function closeSignUpModal() {
    return {
        prefix: MAIN_STATE,
        type: CLOSE_SIGN_UP_MODAL
    }
}
