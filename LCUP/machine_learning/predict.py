""" This module uses django server, (when running it is needn't to change '.train'
to 'train')."""
import os
import pickle
import numpy

from tensorflow.keras import models
from .train import path
from configs.logger import logger


LABEL_COLUMN = 15
ATTRIBUTE_COLUMNS_TO = 12
PROBABILITY_ROUND = 2
TO_PERCENTAGE = 100
TOP_VALUE = 3
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class Predict:
    """ Does the prediction."""

    def __init__(self, answer):
        self.source_dataset = path()[1]
        self.answer = numpy.array(answer)
        self.attr_for_bunch = []
        self.attr_pickle = path()[3]
        self.label_pickle = path()[4]

    @staticmethod
    def formatting(prediction) -> float:
        """ Format predicted class to final look

        :param prediction:  float probability of predicted class
        :return:            formatted predicted class
        """

        return round(prediction * TO_PERCENTAGE, PROBABILITY_ROUND)

    @property
    def attr_dict(self) -> list:
        """ Gets attribute dictionary from pickle file

        :return: list with dictionaries with 'key: answer' for each NN question
        """

        with open(self.attr_pickle, 'rb') as file:
            pickle_data = pickle.load(file)
        return pickle_data

    @property
    def label_dict(self) -> dict:
        """ Sets Labels to Bunch structure (to do predict based on that)

        :return: dictionary with 'key: city'
        """

        with open(self.label_pickle, 'rb') as label_pickle:
            pickle_data = pickle.load(label_pickle)
        return pickle_data

    @staticmethod
    def get_index_by_value(search_value: str, document: dict) -> int:
        """ Gets index from input dictionary by input value

        :param  search_value:   value to search in the input dictionary
        :param  document:       attribute dictionary
        :return:                index of search value
        """

        default_index = 0
        for index, value in enumerate(document.values()):
            if search_value == value:
                return index
        return default_index

    def set_attr(self) -> None:
        """ Sets User attributes to Bunch structure (to do predict based on it)."""

        attr_numb_arr = []
        for user_answers in self.answer:
            for index, answer in enumerate(user_answers):
                key_ans = self.get_index_by_value(answer, self.attr_dict[index])
                attr_numb_arr.append([key_ans])
        self.attr_for_bunch = numpy.array(attr_numb_arr)
        self.attr_for_bunch = self.attr_for_bunch[0:ATTRIBUTE_COLUMNS_TO]. \
            transpose()

    def predict_data(self) -> dict:
        """ Predicts Cities based on incoming answers to the questions

        :return: dictionary of top 3 predicted Cities.
                 Final look-> 'city: percentage'
        """

        self.set_attr()

        loaded_model = models.load_model(path()[2], compile=False)
        loaded_model.compile(loss='sparse_categorical_crossentropy',
                             optimizer='Adadelta', metrics=['accuracy'])
        predict = loaded_model.predict(self.attr_for_bunch)
        full_predict = predict[0].tolist()
        predict_top = sorted(predict[0].tolist(), reverse=True)[:TOP_VALUE]
        cities = {}
        for prediction in predict_top:
            city_cd = self.label_dict[full_predict.index(prediction)]
            cities[city_cd] = self.formatting(prediction)
        return cities


class GetPrediction:
    """ Main class called from tour_manager.py."""

    @staticmethod
    def get_data(user_data: list) -> dict:
        """ Calls method that do the prediction from 'Predict' class

        :param:  list of user answers to do the prediction based on it
        :return: dictionary of top 3 predicted Cities with probability
        """
        try:
            predict = Predict([user_data])
            return predict.predict_data()
        except Exception as err:
            logger.warning(err)
            default_data = {'Riga': 8.00, 'Moscow': 8.00, 'Vienna': 8.00}
            return default_data


if __name__ == '__main__':
    USER1 = ['активный', 'один', 'достопримечательности', 'необычные местные блюда',
             'несколько ( до 6) часов', 'командные, игровые', 'места с историей',
             'нет предпочтений', 'отдых от работы и людей', 'на неделю',
             'нет предпочтений', 'зима']
    USER2 = ['ленивый', 'один', 'всего понемногу',
             'необычные местные блюда',
             'не имеет значения', 'командные, игровые', 'места с историей',
             'жара', 'отдых от работы и людей', 'на неделю',
             'нет предпочтений', 'зима']

    ANSW = USER1
    ANSW2 = USER2

    print(GetPrediction.get_data(ANSW))
    print(GetPrediction.get_data(ANSW2))
