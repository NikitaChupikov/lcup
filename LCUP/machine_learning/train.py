"""Trains NN"""
import os
from collections import Counter
import pickle
import numpy
import pandas as pd

from sklearn.utils import Bunch
from sklearn.model_selection import train_test_split as split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.utils import plot_model
from configs.logger import logger

LABEL_COLUMN = 15
ATTR_COL_TO = 12
TEST_SIZE = 0.1
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
NN_DROP_OUT = 0.5
NN_DENSE2 = 600
NN_DENSE3 = 300


def path():
    """ Open additional files necessary for train module.

    :return: test_dataset, dataset, model, attribute_pickle, label_pickle
    """

    current_path = os.path.abspath(__file__)
    test_dataset = current_path.replace('train.py', 'test_dataset.xls')
    dataset = current_path.replace('train.py', 'dataset.xls')
    model = current_path.replace('train.py', 'model.h5')
    attribute_pickle = current_path.replace('train.py', 'attribute.pickle')
    label_pickle = current_path.replace('train.py', 'label.pickle')
    return test_dataset, dataset, model, attribute_pickle, label_pickle


class Train:
    """ Trains the NN."""

    def __init__(self, dataset=path()[1]):
        self.dataset = dataset
        self.model = Sequential()
        self.attr_dict = []
        self.attr_for_bunch = []
        self.label_dict = {}
        self.labels_for_bunch = []
        self.target_names = []
        self.final_dataset = Bunch()

    def read_excel(self) -> None:
        """ Reads data from excel file skipping headers."""

        self.dataset = pd.read_excel(path()[1], skiprows=0).values.tolist()

    def set_attr(self) -> None:
        """ Sets NN Attributes (independent variables)."""

        self.read_excel()
        dataset_data = numpy.asarray(self.dataset)
        columns = len(dataset_data[0])
        transposed_arr = dataset_data.transpose()

        for elem in transposed_arr:
            data_list = list(Counter(elem).keys())
            temp_dict = {i: data_list[i] for i in range(0, len(data_list))}
            self.attr_dict.append(temp_dict)

        attr_numb_arr = []
        for col in range(columns):
            temp_list = []
            for raw in range(len(self.dataset)):
                for attr_key, attr_value in self.attr_dict[col].items():
                    if transposed_arr[col][raw] == attr_value:
                        temp_list.append(attr_key)
                attr_numb_arr.append(temp_list)

        self.attr_for_bunch = numpy.asarray(attr_numb_arr)[0:ATTR_COL_TO]. \
            transpose()

    def set_labels(self) -> None:
        """ Sets NN Labels (dependent variables)"""

        self.read_excel()
        label_arr = numpy.asarray(self.dataset)
        label_list = label_arr[:, LABEL_COLUMN]
        unique_labels = numpy.unique(label_arr[:, LABEL_COLUMN])

        self.label_dict = {i: unique_labels[i] for i in range(0, len(unique_labels))
                           }
        dataset_labels = []

        for label_id, label_item in enumerate(label_list):
            for dict_key, dict_value in self.label_dict.items():
                if label_item == dict_value:
                    dataset_labels.append(dict_key)

        self.labels_for_bunch = numpy.asarray(dataset_labels)
        self.target_names = numpy.asarray(
            [value for key, value in self.label_dict.items()])

    def pickle_attributes(self) -> None:
        """ Creates pickle object with NN attributes inside"""
        with open('attribute.pickle', 'wb') as file_attr:
            pickle.dump(self.attr_dict, file_attr)

    def pickle_labels(self) -> None:
        """ Creates pickle object with NN labels inside"""
        with open('label.pickle', 'wb') as file_label:
            pickle.dump(self.label_dict, file_label)

    def to_bunch(self) -> None:
        """ Creates Bunch for NN model."""

        self.final_dataset = Bunch(data=self.attr_for_bunch,
                                   target=self.labels_for_bunch,
                                   target_names=self.target_names
                                   )

    def train(self) -> None:
        """ Trains NN model."""

        attributes = self.final_dataset.data
        labels = self.final_dataset.target
        attr_train, attr_test, lbl_train, lbl_test = split(attributes, labels,
                                                           test_size=TEST_SIZE)

        self.model.add(Dense(12, input_shape=(12,), activation='relu'))
        self.model.add(Dense(NN_DENSE2, activation='relu'))
        self.model.add(Dropout(NN_DROP_OUT))
        self.model.add(Dense(NN_DENSE3, activation='relu'))
        self.model.add(Dropout(NN_DROP_OUT))
        self.model.add(Dense(self.target_names.shape[0], activation='softmax'))

        plot_model(self.model, to_file='model.png',
                   show_shapes=True)  # visualization
        self.model.compile(loss='sparse_categorical_crossentropy',
                           optimizer='Adadelta', metrics=['accuracy', ])
        self.model.fit(attr_train, lbl_train, epochs=500, batch_size=32,
                       validation_data=(attr_test, lbl_test))

        logger.info("[INFO] Evaluating neural network...")
        score = self.model.evaluate(attr_test, lbl_test, verbose=0)
        _, accuracy = self.model.evaluate(attr_test, lbl_test)
        logger.info('Accuracy: {%.2f}', (accuracy * 100))
        logger.info('Test loss: %f', score[0])
        logger.info('Test accuracy: %f', score[1] * 100)

    def serialize_model(self) -> None:
        """ Serializes model into .h5 file."""

        self.model.save("model.h5")
        logger.info("Saved model to disk")


if __name__ == '__main__':
    train = Train()
    train.set_attr()
    train.set_labels()
    train.pickle_attributes()
    train.pickle_labels()
    train.to_bunch()
    train.train()
    train.serialize_model()
