SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = '77w034sqtr33oh'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = 'JunzE2gxvZh0j9iz'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = [
    'r_emailaddress',
    'r_liteprofile',
    'w_member_social',
]
SOCIAL_AUTH_LINKEDIN_OAUTH2_FIELD_SELECTORS = [
    'email-address',
    'formatted-name',
    'public-profile-url',
    'picture-url',
]
SOCIAL_AUTH_LINKEDIN_OAUTH2_EXTRA_DATA = [
    ('id', 'id'),
    ('formattedName', 'name'),
    ('emailAddress', 'email_address'),
    ('pictureUrl', 'picture_url'),
    ('publicProfileUrl', 'profile_url'),
]
