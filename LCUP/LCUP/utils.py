from way_out.serializers import UserSerializer


def my_jwt_response_handler(token, user, flag: str, request=None):
    if flag == 'insta':
        return {
            'token': token,
            'user': user
        }

    else:
        return {
            'token': token,
            'user': UserSerializer(user, context={'request': request}).data
        }