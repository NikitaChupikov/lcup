# !/usr/bin/python
# -*- coding: utf-8 -*-
""" TourManager module for combining ML and API work"""
import os
import json
import socket
import platform

from configs.api_arguments import IATA_CODES, INVALID_PARAMS, NO_PARAMS_ERROR
from configs.config import marker
from configs.logger import logger

from nested_lookup import nested_lookup

from api_helpers.travelpayouts.travelpay_manager import TravelPayManager
from tour_managers.excursion_manager import ExcursionManager
from api_helpers.base_parser import loop
from machine_learning.predict import GetPrediction

manager = TravelPayManager()
LIMIT = 5
FLIGHTS_LIMIT = 20
HOTEL_LIMIT = 10


class TourManager:
    """ Combining ML and API work."""

    @property
    def invalid_params(self) -> dict:
        return INVALID_PARAMS

    @property
    def no_param_error(self) -> dict:
        return NO_PARAMS_ERROR

    @property
    def api_root_error(self) -> dict:
        return {'status': 'error',
                'message': 'error with API ifo file reading.'}

    @property
    def no_city_error(self) -> dict:
        return {'status': 'error',
                'code': '500',
                'message': f'No city '
                           f'in IATA codes'}

    @property
    def plane_params(self) -> dict:
        return {
            'errorCode': 400,
            'status': 'error',
            'message': "query: Missing required positional arguments ",
            'positional_arguments': ['origin',
                                     'destination',
                                     'depart_date',
                                     'return_date', ]}

    @property
    def covid_status(self):
        """ Get COVID-19 status in API. """
        filename = r'api/media/entry_info.json'
        if platform.system() == 'Windows':
            filename = r'api\media\entry_info.json'
        path = os.path.abspath(filename)
        with open(path, 'r') as file:
            if not os.path.exists(filename):
                logger.info(f'No such file or directory:{path}')
                return {'status': 'error',
                        'message': 'error with COVID-19 ifo file reading.'}
            else:
                return json.load(file)

    @property
    def ip_address(self):
        """ Get host IP address. """
        hostname = socket.gethostname()
        return socket.gethostbyname(hostname)

    @staticmethod
    def short_title(post_data, post) -> dict:
        """ Add short text info to news response.

        :param post_data: dict with post data.
        :param post:      post instance.
        :return:          updating dict with post data.
        """
        short_text_len = 80
        if len(post.rus_text) > short_text_len:
            short_ru_text = post.rus_text[0:short_text_len]
            post_data['short_ru_text'] = f'{short_ru_text}...'
        if len(post.text) > short_text_len:
            short_en_text = post.text[0:short_text_len]
            post_data['short_en_text'] = f'{short_en_text}...'
        return post_data

    @staticmethod
    def form_row_response(result: tuple, cities: dict) -> list:
        """ Form json ROW response for frontend.

        :param result: tuple(flights, excursions, hotels).
        :param cities: dict{'city': probability} from ML.
        :return:       list with formed data.
        """
        response_data = []
        for index, city in enumerate(cities):
            data = {
                'city_name': city,
                'probability': cities.get(city, ''),
                'flights': result[0][index],
                'hotels': result[1][index],
            }
            response_data.append(data)
        return response_data

    @staticmethod
    def log_info(origin: str, user_ans: list,
                 cities: dict, codes: list) -> str:
        msg = f'Origin: {origin}\n' \
              f'Data from user: {user_ans}\n' \
              f'Cities from ML: {cities}\n' \
              f'Codes: {codes}'
        logger.info(msg)
        return msg

    @staticmethod
    def get_hotels(hotel_results: dict):
        """ Form hotels API response.

        :param hotel_results: API response with hotels.
        :return:              iter list with hotels.
        """
        try:
            if isinstance(hotel_results[0], dict):
                hotels = map(lambda data: {
                    'hotelName': data.get('hotelName', ''),
                    'hotelId': data.get('hotelId', ''),
                    'locationId': data.get('locationId', ''),
                    'priceAvg': data.get('priceAvg', ''),
                    'stars': data.get('stars', ''),
                    'geo': nested_lookup('geo', data),
                    'photo': f'https://photo.hotellook.com/'
                             f'image_v2/limit/h{data.get("hotelId", "")}'
                             f'/800/520.auto',
                    'url': f'https://search.hotellook.com/?'
                           f'hotelId={data.get("hotelId", "")}&'
                           f'locationId={data.get("locationId", "")}&'
                           f'marker={marker}'
                }, hotel_results)
                return hotels
            else:
                return hotel_results
        except Exception as err:
            logger.error(f'Invalid "hotel_result" parse: {err}')
            logger.info(f'Hotels results: {hotel_results}')
            return hotel_results

    @staticmethod
    def get_flights(flight_results: dict):
        """ Form flights API response.

        :param flight_results: API response with flights.
        :return:               iter list with flights.
        """
        passengers_count = 1
        if flight_results:
            flights = map(lambda data: {
                'value': data.get('value', ''),
                'return_date': data.get('return_date', ''),
                'depart_date': data.get('depart_date', ''),
                'gate': data.get('gate', ''),
                'number_of_changes': data.get('number_of_changes', ''),
                'url': f'https://www.aviasales.by/search/'
                       f'{data.get("origin", "")}'
                       f'{data.get("depart_date", "").split("-")[-1]}'
                       f'{data.get("depart_date", "").split("-")[1]}'
                       f'{data.get("destination", "")}'
                       f'{data.get("return_date", "").split("-")[-1]}'
                       f'{data.get("return_date", "").split("-")[1]}'
                       f'{passengers_count}?marker={marker}'
            }, flight_results)
            return flights
        else:
            return list()

    @staticmethod
    def get_codes_by_cities(cities: list) -> list:
        """ Get IATA codes by names of the cities.

        :param cities: list with cities.
        :return:       list of IATA code.
        """
        codes = []
        for city in cities:
            code = IATA_CODES.get(city, '')
            if code:
                codes.append(code)
            else:
                logger.warn(f'No code for city "{city}"')
        return codes

    @staticmethod
    def get_origin_code(user_data: dict) -> str:
        """ Get IATA code for origin city.

        :param user_data: user response.
        :return:          IATA code.
        """
        if user_data.get('origin'):
            city_name = user_data.get('origin')
            code = IATA_CODES.get(city_name)
            if code:
                return IATA_CODES.get(city_name, 'MSQ')
            else:
                logger.info(f'No city "{city_name}" '
                            f'in IATA codes:\n{IATA_CODES}')
        else:
            logger.info('No "origin" in user params')
            return 'MSQ'

    @staticmethod
    def get_year_season(user_data: dict) -> str:
        """ Get season of the year by depart_date.

        :param user_data: Data from user.
        :return:          The season of the year.
        """
        depart_date = user_data.get('depart_date', '')
        if depart_date:
            month = depart_date.split('-')[1]
            if month in ('01', '02', '12'):
                return 'зима'
            elif month in ('03', '04', '05'):
                return 'весна'
            elif month in ('06', '07', '08'):
                return 'лето'
            elif month in ('09', '10', '11'):
                return 'осень'
            else:
                logger.info(f'Invalid month {month} in {depart_date}')
        return 'лето'

    @staticmethod
    def invalid_origin_resp(origin: str, user_data: dict) -> list:
        """ Form response if origin not in dataset.

        :param origin:    city name from user.
        :param user_data: data from user.
        :return:          list with user answers and error.
        """
        response = [{'status': 'error',
                     'errorCode': 3,
                     'message': f'No origin {origin} in dataset'},
                    {'user_answers': user_data}]
        return response

    @staticmethod
    def get_codes(cities: dict, city_instance) -> dict:
        """ Get IATA codes for cities.

        :param cities:        dict with cities and probability from ML.
        :param city_instance: django model with city codes, names, and other...
        :return:              list with codes or dict with error.
        """
        codes = []
        for city in cities:
            city_info = city_instance.objects.filter(name=city)
            if city_info:
                code = city_info[0].code
                codes.append(code)
            else:
                logger.warn(f'No code for city "{city}"')
                return {'status': 'error', 'code': 400,
                        'message': 'Invalid city name'}
        return codes

    def get_origin(self, user_data: dict, city_instance) -> dict:
        """ Get IATA code for origin city.

        :param user_data:     dict with user answers.
        :param city_instance: django model with city codes, names, and other...
        :return:              str code of origin city or dict with error.
        """
        if user_data.get('origin'):
            city_name = user_data.get('origin')
            city_info = city_instance.objects.filter(name=city_name)
            code = city_info[0].code if city_info else None
            if code:
                return code
            else:
                logger.info(f'No city "{city_name}" '
                            f'in IATA codes')
                return self.no_city_error

        else:
            logger.info('No "origin" in user params')
            return {'status': 'error',
                    'code': 400,
                    'message': 'No "origin" in user params'}

    def form_response(self, result: tuple, cities: dict) -> list:
        """ Form json response for frontend.

        :param result: tuple(flights, excursions, hotels).
        :param cities: dict{'city': probability} from ML.
        :return:       list with formed data.
        """
        response_data = []
        for index, city in enumerate(cities):
            flight_data = result[0][index].get('data')
            hotel_data = result[1][index]
            excursion_data = result[2][index]
            excursions = ExcursionManager.parse_excursions(excursion_data)

            country_name = nested_lookup('country', hotel_data)
            if country_name:
                country_name = country_name[0]
            flights = self.get_flights(flight_data)
            hotels = self.get_hotels(hotel_data)

            # ToDo: Remove after COVID-19
            covid_countries = self.covid_status.get('countries')
            status = [country.get('status') for country in covid_countries if
                      country_name == country.get('name')]
            status = status[0] if status else 'No covid-19 info'

            result_data = {
                'city_name': city,
                'status': status,
                'country_name': country_name,
                'probability': cities.get(city, ''),
                'flights': flights,
                'hotels': hotels,
                'excursion': excursions,
            }
            response_data.append(result_data)
        return response_data

    def get_user_answer(self, user_data: dict) -> list:
        """ Get params from user response

        :param user_data: dict with user answers.
        :return:          list with dataset for ML.
        """
        activity_type = user_data.get('activity_type', 'активный')
        tour_group = user_data.get('tour_group', 'парой')
        interest = user_data.get('interest', 'всего понемногу')
        food_type = user_data.get('food_type', 'необычные местные блюда')
        travel_time = user_data.get('travel_time', 'несколько ( до 6) часов')
        sport_type = user_data.get('sport_type', 'командные, игровые')
        attractions = user_data.get('attractions', 'музеи искусств')
        climate = user_data.get('climate', 'нет предпочтений')
        housing = user_data.get('housing', 'нет предпочтений')
        expectations = user_data.get('expectations', 'отдых от работы и людей')
        season = self.get_year_season(user_data)

        user_ans = [activity_type, tour_group, interest, food_type,
                    travel_time, sport_type, attractions, climate,
                    expectations, 'на неделю', housing, season]
        return user_ans

    def get_response(self, user_data: dict, result: tuple,
                     cities: dict) -> list:
        """ Using user answer & ML return list with flights, excursions, hotels.

        :param user_data: dict with user answers.
        :param result:    tuple(flights, excursions, hotels).
        :param cities:    dict{'city': probability} from ML.
        :return:          if sort is True, return parse response
                          else return row response.
        """
        sort = user_data.get('sort', '')
        if sort == 'True':
            return self.form_response(result, cities)
        else:
            return self.form_row_response(result, cities)

    def get_ml_ans(self, user_data: dict) -> dict:
        """ Get only result of predict.

        :param user_data: dict with user answers.
        :return:          cities from ML.
        """
        user_ans = self.get_user_answer(user_data)
        cities = GetPrediction.get_data(user_ans)
        logger.info(f'Data from user: {user_ans}\n'
                    f'Cities from ML: {cities}\n')
        return cities

    def get_manager_trip(self, user_data: dict) -> list:
        """ Combining the work of ML and API's parsers.

        :param user_data: dict with user answers.
        :return:          [dict(flights, excursions, hotels), ...].
        """
        user_ans = self.get_user_answer(user_data)
        cities = GetPrediction.get_data(user_ans)
        codes = self.get_codes_by_cities(cities)
        origin = self.get_origin_code(user_data)
        self.log_info(origin, user_ans, cities, codes)
        return self.get_tour(user_data, origin, cities, codes)

    def get_trip_with_codes(self, user_data: dict, city_instance) -> list:
        """ Combining the work of ML and API's parsers.

        :param city_instance: django model with city codes, names, and other...
        :param user_data:     dict with user answers.
        :return:              [dict(flights, excursions, hotels), ...].
        """
        user_ans = self.get_user_answer(user_data)
        cities = GetPrediction.get_data(user_ans)
        codes = self.get_codes(cities, city_instance)
        origin = self.get_origin(user_data, city_instance)
        if isinstance(origin, dict):
            return origin
        self.log_info(origin, user_ans, cities, codes)
        return self.get_tour(user_data, origin, cities, codes)

    def get_tour(self, user_data: dict, origin: str,
                 cities: dict, codes: list) -> list:
        """ Get tour from travelpayout manager.

        :param user_data: dict with user answers.
        :param origin:    origin city - start point.
        :param cities:    list of cities.
        :param codes:     cities codes.
        :return:          [dict(flights, excursions, hotels), ...].
        """
        if origin:
            limit = user_data.get('limit', LIMIT)
            result = loop.run_until_complete(manager.get_trip
                                             (origin, cities, codes,
                                              user_data.get('depart_date', ''),
                                              user_data.get('return_date', ''),
                                              limit))
            return self.get_response(user_data, result, cities)
        else:
            return self.invalid_origin_resp(origin, user_data)

    def manage_tour(self, user_data: dict, city_instance) -> list:
        """ Get tour(flights and hotels) by cities from ML or user destination.

        :param user_data:      dict with user answers.
        :param city_instance:  django model with city codes, names, and other...
        :return:               [dict(flights, excursions, hotels), ...]
                               or dict with error.
        """
        destination = user_data.get('destination', '')
        if destination:
            cities = {destination: 100}
            origin = self.get_origin(user_data, city_instance)
            codes = self.get_codes(cities, city_instance)
            if isinstance(origin, dict):
                return origin
            if isinstance(codes, dict):
                return codes
            return self.get_tour(user_data, origin, cities, codes)
        else:
            return self.get_trip_with_codes(user_data, city_instance)

    def api_root(self, lang='rus') -> dict:
        """ Read API info file and add to API Root.

        :return: API Root information.
        """
        filename = r'configs/api_info.json'
        if lang == 'rus':
            filename = r'configs/ru_api_info.json'
        if platform.system() == 'Windows':
            filename = r'configs\ru_api_info.json'
        path = os.path.abspath(filename)
        with open(path, 'r') as file:
            if not os.path.exists(filename):
                logger.info(f'No such file or directory:{path}')
                return self.api_root_error
            else:
                resp = file.read().replace('localhost', self.ip_address)
                return json.loads(resp)

    def hotels(self, user_data) -> dict:
        """ Get hotels instances in API.

        :param user_data: data from user.
        :return:          json [{hotel info}, {hotel info}...]
        """
        city = user_data.get('location', '')
        depart_date = user_data.get('checkIn', '')
        return_date = user_data.get('checkOut', '')
        limit = user_data.get('limit', HOTEL_LIMIT)
        resp = loop.run_until_complete(manager.get_hotels([city], depart_date,
                                                          return_date, limit))
        return self.get_hotels(resp[0])

    def flights(self, user_data: dict, city_instance) -> dict:
        """ Get flights response in API.

        :param user_data:     data from user.
        :param city_instance: django City model instance.
        :return:              dict("city": city_name, "flights": list(flights)).
        """
        destination = user_data.get('destination', '')
        depart_date = user_data.get('depart_date', '')
        return_date = user_data.get('return_date', '')
        limit = user_data.get('limit', FLIGHTS_LIMIT)
        resp_keys = set(self.plane_params.get('positional_arguments'))
        if resp_keys.issubset(user_data.keys()):
            cities = {destination: 100}
            origin = self.get_origin(user_data, city_instance)
            codes = self.get_codes(cities, city_instance)
            resp = loop.run_until_complete(manager.get_flights(origin, codes,
                                                               depart_date,
                                                               return_date,
                                                               limit))

            return {'city': destination,
                    'flights': self.get_flights(resp[0].get('data'))}
        else:
            return self.plane_params

    def news(self, post) -> dict:
        """ Parse post news info.

        :param post: post instance.
        :return:     dict with post data.
        """
        post_data = {
            'id': post.id,
            'en_title': post.en_title,
            'ru_title': post.ru_title,
            'date': post.created.date(),
            'image': post.image_url,
            'en_text': post.text,
            'ru_text': post.rus_text,
        }
        post_data = self.short_title(post_data, post)
        return post_data
