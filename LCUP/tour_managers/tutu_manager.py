""" Module for provide tutu.ru transport in API. """
import asyncio

from api_helpers.travelpayouts.train_parser import TutuTrains
from api_helpers.travelpayouts.bus_parser import TutuBuses
from api_helpers.base_parser import loop


class TutuTransport:
    """ Tutu bus/train instance. """

    def __init__(self):
        self.bus = TutuBuses()
        self.train = TutuTrains()
        self.params = {'depart', 'arrival', 'date'}

    @property
    def error_param_msg(self) -> dict:
        """ Error message for invalids params. """
        return {
            'status': 'error',
            'code': 400,
            'message': 'Incorrect parameters',
            'params': {
                'date': 'DD.MM.YYYYY',
                'arrival': 'arrival point (Example: Minsk)',
                'depart': 'depart point (Example: Brest)'
            }
        }

    async def __get_train_endpoints(self, depart: str, arrival: str) -> tuple:
        """ Get depart/arrival ID's by endpoints names.

        :param depart:  depart point (Example: Minsk)
        :param arrival: arrival point (Example: Brest)
        :return:        tuple(depart_id, arrival_id)
        """
        tasks = [asyncio.ensure_future(self.train.get_endpoint_id(depart)),
                 asyncio.ensure_future(self.train.get_endpoint_id(arrival))
                 ]
        depart_id, arrival_id = await asyncio.gather(*tasks)
        return depart_id, arrival_id

    def __form_resp(self, trips: dict, date: str) -> list:
        """ From API response for buses.

        :param trips: resp with train tickets.
        :param date:  depart date.
        :return:      list of trains tours.
        """
        tours = []
        for trip in trips:
            tour = loop.run_until_complete(self.train.form_resp(trip, date))
            tours.append(tour)
        return tours

    def get_buses(self, depart: str, arrival: str, date: str) -> list:
        """ Get buses tours by endpoints names.

        :param depart:  depart point (Example: Minsk)
        :param arrival: arrival point (Example: Brest)
        :param date:    depart date.
        :return:        list of buses tours.
        """
        res = loop.run_until_complete(self.bus.bus_tours(depart, arrival, date))
        return res

    def get_trains(self, depart: str, arrival: str, date: str) -> list:
        """ Get train tours by endpoints names.

        :param depart:  depart point (Example: Minsk)
        :param arrival: arrival point (Example: Brest)
        :param date:    depart date.
        :return:        list of buses tours.
        """
        depart_id, arrival_id = loop.run_until_complete(
            self.__get_train_endpoints(depart, arrival))
        depart_id, arrival_id = depart_id[0].get('id'), arrival_id[0].get('id')
        trips = loop.run_until_complete(self.train.get_tours_by_ids(depart_id,
                                                                    arrival_id))
        return self.__form_resp(trips.get('trips'), date)
