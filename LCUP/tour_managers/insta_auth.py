""" Module to provide instagram authorisation for users"""
import os
import requests

from datetime import datetime, timedelta
from django.utils import timezone
from configs.api_arguments import USER_ID_DOESNT_EXIST, INSTA_USER_ALREADY_EXISTS
from way_out.models import AccountSocial
from configs.logger import logger

AUTH_URL = "https://api.instagram.com/oauth/access_token"
RESP_URL = "https://graph.instagram.com/"
REFRESH_TOKEN_URL = "https://graph.instagram.com/refresh_access_token"
APP_ID = os.environ.get('INST_APP_ID')
APP_SECRET = os.environ.get('INST_APP_SECRET')
REDIRECT_URI = os.environ.get('INSTA_REDIRECT_URL')
GRANT_TYPE = "authorization_code"
RESP_FIELDS = "id,username"

class Auth:
    """ Authorisation method."""

    def __init__(self):
        self.user_id = ''
        self.token = ''
        self.token_time_expiring = 0

    def get_user_by_code(self, token: str) -> str:
        """ Get user's info.

        :param token:   instagram access token
        :return:        instagram username
        """
        params = {"fields": RESP_FIELDS, "access_token": token}
        try:
            user = requests.get(RESP_URL + str(self.user_id), params=params).json()
            return user
        except Exception as err:
            logger.error(err)
            return f'ERROR: {err}'

    def auth_user(self, code: str):
        """ Authenticate user through instagram login page.

        :param code:    code received from front-end
        :return:        username received by code, or 'error' if the user_id
                        doesn't exist
        """
        params = {"client_id": APP_ID, "client_secret": APP_SECRET,
                  "grant_type": GRANT_TYPE,
                  "redirect_uri": REDIRECT_URI,
                  "code": code}
        user_data = requests.post(AUTH_URL, data=params).json()
        user_id = user_data.get('user_id', '')
        account_check = AccountSocial.objects.filter(social_id=user_id).exists()
        if account_check:
            self.check_if_needs_updates(user_id, user_data.get('access_token', ''))
            user_account = AccountSocial.objects.get(social_id=user_id)
            username = user_account.get_username()
            return username
        else:
            return USER_ID_DOESNT_EXIST

    def check_if_needs_updates(self, user_id: str, access_token: str) -> None:
        """ Method that checks if instagram token has expired,
        if it has - updates token, expire data in AccountSocial table.

        :param user_id:         instagramn user id
        :param access_token:    new instagram access token in case the old one
                                has expired
        """
        insta_db_manager = AccountSocial.objects.get(social_id=user_id)
        expire_date = insta_db_manager.get_expire_date()
        if datetime.now(tz=timezone.utc) > expire_date:
            self.update_longterm_marker(old_token=access_token,
                                        username=insta_db_manager.get_username())

    def get_token(self, code: str) -> str:
        """ Get access self.token for instagram auth.

        :param code:    auth code from instagram api
        :return:        instagram self.token
        """
        params = {"client_id": APP_ID, "client_secret": APP_SECRET,
                  "grant_type": GRANT_TYPE,
                  "redirect_uri": REDIRECT_URI,
                  "code": code}
        insta_data = requests.post(AUTH_URL, data=params).json()
        logger.info("Get instagram token request: {}".format(insta_data))
        access_token = insta_data.get('access_token', '')
        return self.update_token(insta_data, access_token)

    def update_token(self, insta_data: dict, permanent_token: str) -> str:
        """

        :param insta_data:          instagram data
        :param permanent_token:     permanent instagram marker (token)
        :return:                    long-term instagram marker (token)
        """
        url = 'https://graph.instagram.com/access_token?' \
              'grant_type=ig_exchange_token&' \
              'client_secret={}&access_token={}'.format(APP_SECRET, permanent_token)
        new_insta_data = requests.get(url).json()
        logger.info("Instagram longterm token request: {}".format(new_insta_data))
        if 'access_token' in new_insta_data:
            self.token = new_insta_data.get('access_token', '')
            self.user_id = insta_data.get('user_id', '')
            self.token_time_expiring = new_insta_data.get('expires_in', '')
            return {'token': self.token}
        else:

            response = {'error_code': new_insta_data.get('code', ''),
                        'error_message': new_insta_data.get('error_message', '')}
            return response

    def to_db(self, social_id: int, username: str) -> None:
        """ Insert Instagram User to AccountSocial table.

        :param social_id:   instagram id
        :param username:    instagram username
        """
        insta_db_manager = AccountSocial(
            social_id=social_id,
            username=username,
            token=self.token,
            insert_ts=datetime.now(tz=timezone.utc),
            expires_at=datetime.now(tz=timezone.utc) + timedelta(
                seconds=self.token_time_expiring),
            provider='instagram')
        insta_db_manager.save()

    @staticmethod
    def update_longterm_marker(old_token: str, username: str) -> str:
        """ Refreshes instagram access token (long-term marker), expiration date

        :param old_token:   old instagram token (marker)
        :param username:    instagram username
        :return:            new updated instagram access token (long-term marker)
        """
        params = {"grant_type": "ig_refresh_token", "access_token": old_token}
        try:
            new_data = requests.get(REFRESH_TOKEN_URL, params=params).json()
            new_token = new_data['access_token']
            insta_db_manager = AccountSocial.objects.get(username=username)
            insta_db_manager.token = new_token
            insta_db_manager.expires_at = datetime.now(tz=timezone.utc) + timedelta(
                seconds=new_data['expires_in'])
            insta_db_manager.save()
            return new_token
        except Exception as err:
            logger.error(err)
            return f'ERROR: {err}'


class InstaAuth:
    """ Authorise instagram user. """
    def __init__(self):
        self.auth = Auth()

    @property
    def user_already_exists(self):
        """ If Instaghram user has already been added to DB."""

        return INSTA_USER_ALREADY_EXISTS

    def register_user(self, code: str) -> str:
        """ Register user via instagram.

        :param code:    instagram access code
        :return:        instagram username
        """
        insta_resp = self.auth.get_token(code)
        if 'token' in insta_resp:
            return self.auth.get_user_by_code(insta_resp.get('token', ''))
        else:
            return insta_resp

    def user_to_db(self, social_id: int, username: str) -> None:
        """ Add Instagram User to DB

        :param social_id:   instagram id
        :param username:    instagram username
        """
        try:
            self.auth.to_db(social_id, username)
        except Exception as err:
            logger.error(err)

    def update_token(self, username: str) -> str:
        """ Refresh instagram token.

        :param username:    instagram username
        :return:            new token
        """
        user_account = AccountSocial.objects.get(username=username)
        old_token = user_account.get_token()
        username = user_account.get_username()
        new_token = self.auth.update_longterm_marker(old_token, username)
        return new_token

    def authenticate_user(self, code: str):
        """ Authenticate user against instagram access code he receives
            when logging in.

        :param code:    instagram code received from front-end when user logging in
        :return:        instagram username, or error
        """

        return self.auth.auth_user(code)
