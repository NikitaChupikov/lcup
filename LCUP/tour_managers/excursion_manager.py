""" Module for making excursions responses in Django """
from nested_lookup import nested_delete, nested_update

from configs.config import marker
from api_helpers.travelpayouts.travelpay_manager import TravelPayManager
from api_helpers.base_parser import loop

manager = TravelPayManager()

URL = f'https://c11.travelpayouts.com/click?shmarker={marker}.' \
      f'https://experience.tripster.ru/experience/23560/&' \
      f'promo_id=652&source_type=customlink&type=click&custom_url='


class ExcursionManager:
    """ Class for managing excursion parser work. """
    excursions_with_rubrics = 1
    rubrics_only = 2
    excursions_only = 3

    @property
    def no_params_error(self) -> dict:
        """ Get error message. """
        return {
            'code': '400',
            'status': 'Bad Request',
            'message': 'Use "city" and "tag" params'
        }

    @property
    def no_resp_type_error(self):
        """ Use for method 'get_excursions' calls"""
        return {
            'status': 'Excursion module error',
            'message': 'No type of resp param in function'
        }

    @staticmethod
    def excursions(city_name, city_tag=None, page_size=20, page=1) -> dict:
        """ Get excursions by city and tag.

        :param city_name: name of the city.
        :param city_tag:  id of city rubrics.
        :param page_size: number of excursions on page.
        :param page:      page number.
        :return:          dict with excursions.
        """
        return loop.run_until_complete(
            manager.city_excursions(city_name=city_name,
                                    citytag=city_tag,
                                    page_size=page_size,
                                    page=page))

    @staticmethod
    def rubrics(city_name) -> dict:
        """ Get rubrics by city.

        :param city_name: name of the city.
        :return:          dict with city rubrics.
        """
        return loop.run_until_complete(
            manager.city_excursions_rubrics(city_name=city_name))

    async def excursions_with_rubrics_resp(self, city_name: str, city_tag=None,
                                           page_size=20, page=1) -> dict:
        """ Get excursions with rubrics.

        :param city_name: name of the city
        :param city_tag:  id of city rubric.
        :param page_size: number of excursions on page.
        :param page:      page number.
        :return:          excursions with rubrics.
        """
        excursions = await manager.city_excursions(city_name=city_name,
                                                   citytag=city_tag,
                                                   page_size=page_size,
                                                   page=page)
        rubrics = await manager.city_excursions_rubrics(city_name=city_name)
        return {
            'rubrics': self.parse_excursions(rubrics),
            'excursions': self.parse_rubrics(excursions)
        }

    @staticmethod
    def excursions_and_rubrics(city_name) -> tuple:
        """ Get excursions and rubrics by city.

        :param city_name: name of the city.
        :return:          tuple(rubrics, excursions)
        """
        return loop.run_until_complete(
            manager.city_excursions_and_rubrics(city_name=city_name))

    @staticmethod
    def parse_rubrics(rubrics: dict) -> dict:
        """ Parse rubrics dict.

        :param rubrics: dict with city rubrics.
        :return:        parsed rubrics dict.
        """
        rubrics_res = rubrics.get('results')
        rubrics_res = nested_update(rubrics_res, key='is_hidden', value='false')
        rubrics_res = nested_delete(rubrics_res, 'is_hidden')
        return nested_delete(rubrics_res, 'tag')

    @staticmethod
    def parse_excursions(excursions: dict) -> dict:
        """ Parse excursions dict and add partner url.

        :param excursions: dict with city excursions.
        :return:           parsed excursions dict.
        """
        excursions_res = excursions.get('results')
        for excursion in excursions_res:
            url = excursion.get('url')
            partner_url = f'{URL}{url}'
            nested_update(excursion, 'url', partner_url, in_place=True)
        return excursions_res

    def parser_excursions_and_rubrics(self, city_name: str) -> dict:
        """ Create dict with rubrics and excursions.

        :param city_name: name of the city.
        :return:          dict with response tuple(rubrics, excursions)
        """
        rubrics = self.rubrics(city_name)
        excursions = self.excursions(city_name)
        return {
            'rubrics': self.parse_excursions(rubrics),
            'excursions': self.parse_rubrics(excursions)
        }

    def chose_resp_type(self, city_name: str, code: int,
                        city_tag=None, page_size=20, page=1) -> dict:
        """ Choosing type of response

        :param city_name: name of the city
        :param code:      code for change type of response.
        :param city_tag:  id of city rubric.
        :param page_size: number of excursions on page.
        :param page:      page number.
        :return:          excursions with rubrics or only rubrics or excursions.
        """
        if code == self.excursions_with_rubrics:
            return loop.run_until_complete(
                self.excursions_with_rubrics_resp(city_name, city_tag,
                                                  page_size, page))
        elif code == self.rubrics_only:
            rubrics = self.rubrics(city_name)
            return self.parse_rubrics(rubrics)

        elif code == self.excursions_only:
            excursions = self.excursions(city_name, city_tag, page_size, page)
            return self.parse_excursions(excursions)
        else:
            return self.no_resp_type_error

    def get_excursions(self, city_name: str, code: int,
                       city_tag=None, page_size=20, page=1) -> dict:
        """ Get excursions instances in dict.

        :param city_name: name of the city
        :param code:      code for change type of response.
        :param city_tag:  id of city rubric.
        :param page_size: number of excursions on page.
        :param page:      page number.
        :return:          excursions with rubrics or only rubrics or excursions.
        """
        if city_name:
            return self.chose_resp_type(city_name, code, city_tag,
                                        page_size, page)
        else:
            return self.no_params_error
