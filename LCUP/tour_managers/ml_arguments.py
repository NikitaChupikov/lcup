""" Module for getting Machine Learn questions response. """
import os
import json

from configs.logger import logger
from nested_lookup import nested_lookup

FILENAME = r'./configs/ml_questions.json'
FULL_Q = r'./api/media/full_questions.json'


class MachineLearnQuestions:
    """ ML questions file parser. """

    @staticmethod
    def read_config(filename: str) -> dict:
        """ Get ML params from .json config.

        :return: data from json file.
        """
        if not os.path.exists(FILENAME):
            logger.info(f'No such file or directory:{filename}')
            return {'status': 'error',
                    'message': 'Config file with ML params does not exist'}
        else:
            with open(filename, 'r', encoding="utf8") as file:
                config = json.loads(file.read())
                return config

    def get_questions(self, params: dict, auth) -> dict:
        """ Get dict with params for frontend part.

        :param params: params from user.
        :param auth:   marker for user authentication.
        :return:       dict with all ML params.
        """
        if auth:
            filename = FULL_Q
        else:
            filename = FILENAME

        if params:
            config = self.read_config(filename)
            search = params.get('search', '')
            search_result = nested_lookup(search, config)
            return {'search_value': search,
                    'search_result': search_result}
        else:
            return self.read_config(filename)
