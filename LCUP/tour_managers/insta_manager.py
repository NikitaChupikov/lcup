""" Module that provides instagram instance work in API. """
import instagram_explore as ie
import pandas as pd

from api_helpers.insta_helper import InstaAnalyzer
from configs.api_arguments import INSTAGRAM_PARAMS
from configs.logger import logger


class InstaManager:
    """ API manager for instagram instance. """

    @property
    def invalid_params(self) -> dict:
        return INSTAGRAM_PARAMS

    @staticmethod
    def instance_in_dict(username: str) -> dict:
        """ Form dict from instagram instance.

        :param username: instagram username.
        :return:         dict with data from instagram.
        """
        insta = InstaAnalyzer(username)
        user_data = {
            'full_name': insta.full_name,
            'id': insta.id,
            'biography': insta.biography,
            'edge_follow': insta.edge_follow,
            'edge_followed_by': insta.edge_followed_by,
            'locations': insta.locations,
            'profile_pic_url_hd': insta.profile_pic_url_hd,
            'pics': insta.pics,
        }
        return user_data

    def parse(self, request: dict) -> dict:
        """ Parse data from instagram by username.

        :param request: GET request from frontend.
        :return:        dict with insta data.
        """
        username = request.get('insta_user', '')
        if username:
            try:
                user_data = self.instance_in_dict(username)
                return user_data
            except Exception as err:
                logger.info(f'Problem with creating instagram '
                            f'instance with name="{username}\n{err}"')
                return {'status': 'error',
                        'message': f'Invalid username: {username}',
                        'errorCode': 5}
        else:
            return self.invalid_params

    @staticmethod
    def get_user_locations(username: str) -> list:
        """ Get locations from instagram by username

        :param username: instagram username
        :return:         locations list
        """
        locations = list()
        try:
            user_info = ie.user(username)
            for post in user_info.data['edge_owner_to_timeline_media']['edges']:
                if post['node']['location'] is not None:
                    locations.append(post['node']['location']['name'])
        except Exception as err:
            logger.info('Error getting instagram instance with name=%s\n%s',
                        username, err)
        return locations

    @staticmethod
    def get_cities_by_locations(locations: list) -> list:
        """ Get exact cities from location list

        :param locations: locations list from instagram
        :return:          cities list for locations list
        """
        cities_dataset = './machine_learning/cities_dataset.txt'
        cities = pd.read_csv(cities_dataset)
        cities = cities['city_1'].tolist()
        city_list = list()
        for location in locations:
            for city in cities:
                if location.find(city) >= 0:
                    city_list.append(city)
        return city_list

    @staticmethod
    def get_similars(city_user, user_id: int) -> pd.Series:
        """ Get similar cities for user_id with visited city_user

        :param city_user: dataframe of users and visited cities
        :param user_id:   current user id
        :return:          series of similar cities
        """
        preference_countries = city_user.pivot_table(index=['userId'], columns=['city'],
                                                     values=['loc_count'], aggfunc='first',
                                                     fill_value=0)

        corr_m = preference_countries.corr(method='pearson', min_periods=100)
        my_ratings = preference_countries.loc[user_id].dropna()
        sim_candidates = pd.Series()
        for i in range(0, len(my_ratings.index)):
            sims = corr_m[my_ratings.index[i]].dropna()
            sims = sims.map(lambda x: abs(x * my_ratings[i]))
            sim_candidates = sim_candidates.append(sims)
        sim_candidates.sort_values(inplace=True, ascending=False)
        return sim_candidates

    @staticmethod
    def update_city_user(user_id: int, locations: list) -> pd.DataFrame:
        """ Update dataset city_user

        :param user_id:   current user id
        :param locations: locations list for current user
        :return:          updated city_user dataframe
        """
        city_user_dataset = './machine_learning/city_user_dataset.txt'
        city_user = pd.read_csv(city_user_dataset)
        new_user_locs = pd.DataFrame({'userId': [user_id for i in range(0, len(locations))],
                                      'city': locations, 'loc_count':
                                          [1 for i in range(0, len(locations))]}).drop_duplicates()
        city_user = pd.concat([new_user_locs, city_user], ignore_index=True, sort=True).drop_duplicates()
        city_user.to_csv(city_user_dataset, index=None)
        return city_user

    @staticmethod
    def get_user_id(username: str) -> int:
        """ Get user_id for username

        :param username: instagram username
        :return:         user_id
        """
        users_dataset = './machine_learning/users_dataset.txt'
        users = pd.read_csv(users_dataset)
        if username in users['username'].tolist():
            user_id = users[users['username'] == username]['userId'].tolist()[0]
        else:
            user_id = users['userId'].max() + 1
            user_row = pd.DataFrame({'userId': [user_id], 'username': [username], 'id_name': ['']})
            users = pd.concat([users, user_row], ignore_index=True, sort=False)
            users.to_csv(users_dataset, index=None)
        return user_id

    @staticmethod
    def user_recommend(username: str, result_count: int) -> list:
        """ Get locations recommended to user

        :param username:     instagram username.
        :param result_count: cities number to return
        :return:             list of pairs city name and similarity index
        """
        locations = InstaManager.get_cities_by_locations(InstaManager.get_user_locations(username))
        if len(locations) > 0:
            user_id = InstaManager.get_user_id(username)
            city_user = InstaManager.update_city_user(user_id, locations)
            sim_candidates = InstaManager.get_similars(city_user, user_id)
            results = [[sim_candidates.index[i][1], sim_candidates[i]]
                       for i in range(0, min(len(sim_candidates), result_count+len(locations)))
                       if sim_candidates[i] < 1 and sim_candidates.index[i][1] not in locations]
            if len(results) > result_count:
                return results[0:result_count]
            return results
        return list()
