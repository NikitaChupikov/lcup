""" Manager module for manage user answers in DB"""
import ast

from configs.logger import logger
from configs.api_arguments import SAVE_ERROR, SAVE_USER
from way_out.models import UserAnswer


class UserManager:
    """ Class for manage user works. """

    @property
    def auth_error(self) -> dict:
        """ Error if user is not authenticated"""
        return {
            "status": "OK",
            "errorCode": 401,
            "message": "User is not authenticated"
        }

    @staticmethod
    def update_user_ans(questions: dict, username: str) -> dict:
        """ Find user in DataBase and update user answers.

        :param questions:  updating params for ML.
        :param username:   username from user account.
        :return:           ok/error status response.
        """
        try:
            answers = UserAnswer.objects.get(username=username)
            answers.data = {"questions": questions}
            answers.save()
            return SAVE_USER
        except Exception as error:
            logger.error(error)
            return SAVE_ERROR

    @staticmethod
    def add_new_user_ans(username: str, data: dict) -> None:
        """ Add default ML answers for new user.

        :param username: username from user account.
        :param data:     default ML answers.
        """
        new_user = UserAnswer(username=username, data=data)
        new_user.save()

    @staticmethod
    def get_user_answers(username: str) -> dict:
        """ Get user ML answers from DataBase.

        :param username: username from user account.
        :return:         user ML answers in .json format.
        """
        answers = UserAnswer.objects.get(username=username)
        json_data = ast.literal_eval(answers.data)
        return json_data

    def save_user_ans(self, user_ans: list, ml_questions: dict,
                      username: str) -> dict:
        """ Puts the user's answers in the first position
            in ML params and saves in the database.

        :param user_ans:     answers from user.
        :param username:     username from user account.
        :param ml_questions: questions and params for ML.
        :return:             ok/error status response.
        """
        last_id = 10
        pr_id = last_id - 1
        user_ans[pr_id], user_ans[last_id] = user_ans[last_id], user_ans[pr_id]
        questions = ml_questions.get('questions')
        for ans, question in zip(user_ans, questions[0:last_id]):
            answers = question.get('answers')
            for param in enumerate(answers):
                index = param[0]
                if ans == answers[index]:
                    answers[0], answers[index] = answers[index], answers[0]
                    question['answers'] = answers
        response = self.update_user_ans(questions, username)
        return response
